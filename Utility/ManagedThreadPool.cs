﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;

namespace Utility
{
    public class ManagedThreadPool
    {
        public delegate void ThreadFinishDelegate<Tout>(Tout output);
        public delegate void ThreadFinishVoid();
        private Queue<Tuple<string, ThreadStart>> queuedThreads = new Queue<Tuple<string, ThreadStart>>();
        private Thread[] threadPool;
        private int maxThreads = 0;
        public int currentActiveThreads = 0;

        public ManagedThreadPool(int maxThreads)
        {
            this.maxThreads = maxThreads;
            threadPool = new Thread[maxThreads];
        }

        public bool hasAliveOrUnstartedThreads()
        {
           return queuedThreads.Count() > 0 || threadPool.Where(t => null != t && t.IsAlive).Count() > 0;
        }

        /// <summary>
        /// Adds a Task to the thread queue with input and output
        /// </summary>
        /// <typeparam name="Tin">input type</typeparam>
        /// <typeparam name="Tout">output type</typeparam>
        /// <param name="taskName">name of task</param>
        /// <param name="taskFunc">task as a function</param>
        /// <param name="callerDelegate">method from the caller to execute upon completion</param>
        /// <param name="input">input to the function</param>
        public void EnqueueTask<Tin, Tout>(string taskName, 
                                            Func<Tin,Tout> taskFunc,
                                            ThreadFinishDelegate<Tout> callerDelegate,
                                            Tin input)
        {
            InOutFunc<Tin, Tout> threadFunc = new InOutFunc<Tin, Tout>(input, taskFunc, callerDelegate);
            threadFunc.FunctionFinished += ThreadFunc_DecrementCurrentThreadCount;
            queuedThreads.Enqueue(Tuple.Create(taskName, createThreadStart(threadFunc)));
        }
        /// <summary>
        /// Adds a task to the thread queue with no input or output
        /// </summary>
        /// <param name="taskName">name of the task</param>
        /// <param name="action">task to perform</param>
        /// <param name="callerDelegate">method from the caller to execute upon completion</param>
        public void EnqueueTask(string taskName,
                                Action action,
                                ThreadFinishVoid callerDelegate)
        {
            VoidFunc voidFunc = new VoidFunc(action, callerDelegate);
            voidFunc.FunctionFinished += ThreadFunc_DecrementCurrentThreadCount;
            queuedThreads.Enqueue(Tuple.Create(taskName, createThreadStart(voidFunc)));
        }


        public void RunAllAsynchronous()
        {
            VoidFunc voidFunc = new VoidFunc(RunAll, null);
            voidFunc.FunctionFinished += ThreadFunc_DecrementCurrentThreadCount;
            createAndStartThread("Asynchronous Thread Runner Thread", createThreadStart(voidFunc));
        }

        /// <summary>
        /// Runs all queued threads. Method exeutes on the main thread
        /// causing the application to wait until all threads have been
        /// started (not finished).
        /// </summary>
        public void RunAll()
        {
            while(true)
            {
                for (int i = 0;i < threadPool.Count(); i++)
                { 
                    if(threadPool[i] == null || !threadPool[i].IsAlive)
                    {
                        if(queuedThreads.Count > 0)
                        {
                            Tuple<string, ThreadStart> threadDef = queuedThreads.Dequeue();
                            createAndStartThread(threadDef.Item1, threadDef.Item2, ref threadPool[i]);
                        } else
                        {
                            return;
                        }
                            
                    }
                }
            }
        }

        /// <summary>
        /// Creates a thread and starts it
        /// </summary>
        /// <param name="threadName"></param>
        /// <param name="threadStart"></param>
        private void createAndStartThread(string threadName, ThreadStart threadStart, ref Thread thread)
        {
            //thread?.Abort();
            thread = new Thread(threadStart);
            thread.Name = threadName;
            thread.IsBackground = true;
            thread.Start();
            currentActiveThreads++;
        }

        private void  createAndStartThread(string threadName, ThreadStart threadStart)
        {
            Thread thread = new Thread(threadStart);
            thread.Name = threadName;
            thread.IsBackground = true;
            thread.Start();
            currentActiveThreads++;
        }

        private void ThreadFunc_DecrementCurrentThreadCount(object sender, EventArgs e)
        {
            currentActiveThreads--;
        }

        private ThreadStart createThreadStart(IThreadFunc threadFunc)
        {
            return new ThreadStart(threadFunc.InvokeFunc);
        }

        /// <summary>
        /// Interface for thread functions
        /// </summary>
        private interface IThreadFunc
        {
            void InvokeFunc();
            event EventHandler FunctionFinished;
        }
        /// <summary>
        /// Class that encapsulates a thread function that inputs
        /// object of type Tin and outputs and object of Tout
        /// </summary>
        /// <typeparam name="Tin"></typeparam>
        /// <typeparam name="Tout"></typeparam>
        private class InOutFunc<Tin, Tout> : IThreadFunc
        {
            
            private Tin input;
            private Func<Tin, Tout> func;
            private Tout output;
            private ThreadFinishDelegate<Tout> callerMethod;
            public event EventHandler FunctionFinished;

            public InOutFunc(Tin input, 
                Func<Tin,Tout> func,
                ThreadFinishDelegate<Tout> callerMethod)
            {
                this.input = input;
                this.func = func;
                this.callerMethod = callerMethod;
            }

            public void InvokeFunc()
            {
                output = func.Invoke(input);
                FunctionFinished?.Invoke(this, null);
                if (callerMethod != null)
                {
                    callerMethod(output);
                }
            }
        }
        /// <summary>
        /// Encapsulates a method that takes no input has no output
        /// </summary>
        private class VoidFunc : IThreadFunc
        {
            private Action action;
            private ThreadFinishVoid callerFunc;
            public event EventHandler FunctionFinished;
            public VoidFunc(Action action, ThreadFinishVoid callerFunc)
            {
                this.action = action;
                this.callerFunc = callerFunc;
            }

            public void InvokeFunc()
            {
                action.Invoke();
                FunctionFinished?.Invoke(this, null);
                if (callerFunc != null)
                {
                    callerFunc();
                }
            }
        }

    }
}
