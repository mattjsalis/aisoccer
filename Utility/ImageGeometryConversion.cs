﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    public class ImageGeometryConversion
    {
        public static Vect3 CanvasPositionToFieldPosition(Vect3 imagePosition,
            double topPadding,
            double leftPadding,
            double fieldWidth,
            double fieldHeight)
        {
            return imagePosition.AddByComponentValues(
                -(leftPadding + fieldWidth / 2),
                -(topPadding + fieldHeight / 2),
                0)
                .MultiplyByComponentFactors(1, -1, 1);
        }

        public static Vect3 FieldPositionToCanvasPosition(Vect3 fieldPosition,
            double topPadding,
            double leftPadding,
            double fieldWidth,
            double fieldHeight)
        {
            return fieldPosition
                .MultiplyByComponentFactors(1, -1, 1)
                .AddByComponentValues(
                leftPadding + fieldWidth/2,
                (topPadding + fieldHeight / 2),
                0)
                ;
        }

        public static Vect3 FieldPositionToCanvasPosition(Vect3 fieldPosition,
            double topPadding,
            double leftPadding,
            double fieldWidth,
            double fieldHeight,
            double componentHeight,
            double componentWidth)
        {
            return fieldPosition
                .MultiplyByComponentFactors(1, -1, 1)
                .AddByComponentValues(
                leftPadding + fieldWidth / 2,
                (topPadding + fieldHeight / 2),
                0)
                .AddByComponentValues(-componentWidth /2, - componentHeight/2, 0.0)
                ;
        }
    }
}
