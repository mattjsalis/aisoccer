﻿using System;
using System.Xml.Serialization;
using static System.Math;

namespace Utility
{
    [Serializable]
    /// <summary>
    /// Represents a three component mathematical vector
    /// </summary>
    public class Vect3
    {
        [XmlAttribute]
        public double X { get; set; }
        [XmlAttribute]
        public double Y { get; set; }
        [XmlAttribute]
        public double Z { get; set; }

        [XmlIgnore]
        public static Vect3 XUnit {get{return new Vect3(1,0,0); } }
        [XmlIgnore]
        public static Vect3 YUnit { get { return new Vect3(0, 1, 0); } }
        [XmlIgnore]
        public static Vect3 ZUnit { get { return new Vect3(0, 0, 1); } }
        [XmlIgnore]
        public double Magnitude
        {
            get
            {
                return Sqrt(Pow(X, 2.0) + Pow(Y, 2.0) + Pow(Z, 2.0));
            }
        }
        [XmlIgnore]
        public Vect3 UnitVector {
            get {
                double mag = Magnitude;
                if(mag == 0.0)
                {
                    return new Vect3(0,0,0);
                }
                return (new Vect3(X,Y,Z)).Times(1/Magnitude); 
            }
        }

        /// <summary>
        /// Vector from magnitude and azimuth
        /// </summary>
        /// <param name="magnitude"></param>
        /// <param name="azimuth">Measured in radians, CCW off of positive x-axis</param>
        public Vect3(double magnitude, double azimuth)
        {
            X = magnitude * Math.Cos(azimuth);
            Y = magnitude * Math.Sin(azimuth);
            Z = 0;
        }
        
        public Vect3(double magnitude, Angle angle)
        {
            X = angle.Cos() * magnitude;
            Y = angle.Sin() * magnitude;
            Z = 0;
        }

        public Vect3(Vect3 v)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
        }
        
        /// <summary>
        /// Initializes three dimensional vector with xyz components
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public Vect3(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        /// <summary>
        /// Initializes zero vector
        /// </summary>
        public Vect3()
        {
            X = Y = Z = 0;
        }


        /// <summary>
        /// Addes vectors to new instance of vector
        /// </summary>
        /// <param name="vect"></param>
        /// <returns></returns>
        public Vect3 Plus(Vect3 vect)
        {
            return new Vect3(X + vect.X, Y + vect.Y, Z + vect.Z);
        }

        public void AddTo(Vect3 vect)
        {
            X += vect.X;
            Y += vect.Y;
            Z += vect.Z;
        }

        public Vect3 Minus(Vect3 vect)
        {
            return new Vect3(X - vect.X, Y - vect.Y, Z - vect.Z);
        }

        /// <summary>
        /// Multiplies a scalar by the vect and returns a new vect
        /// </summary>
        /// <param name="aDub"></param>
        /// <returns></returns>
        public Vect3 Times(double aDub)
        {
            return new Vect3(X*aDub, Y * aDub, Z * aDub);
        }

        public Vect3 Copy()
        {
            return new Vect3(X, Y, Z);
        }

        public double DistanceToVect(Vect3 anotherVect)
        {
            return Sqrt(Pow(anotherVect.X - X, 2.0) 
                        + Pow(anotherVect.Y - Y, 2.0) 
                        + Pow(anotherVect.Z - Z, 2.0));
        }

        /// <summary>
        /// Returns the relative angle between two vectors. Bounded between 0 and 180 deg
        /// </summary>
        /// <param name="anotherVect"></param>
        /// <returns></returns>
        public Angle RelativeAngleBetweenVectors(Vect3 anotherVect)
        {
            return Angle.FromRadians(Acos(UnitVector.DotProduct(anotherVect.UnitVector)));
        }

        public Angle getAzimuthOfOther(Vect3 other)
        {
            Vect3 toOther = other.Minus(this);
            return Angle.FromRadians(Atan2(toOther.Y, toOther.X));
        }

        public double DotProduct(Vect3 anotherVect)
        {
            return X * anotherVect.X + Y * anotherVect.Y * Z * anotherVect.Z;
        }

        public Vect3 CrossProduct(Vect3 v)
        {
            return new Vect3(Y * v.Z - Z * v.Y,
                Z * v.X - X * v.Z,
                X * v.Y - Y * v.X);
        }

        public Vect3 Rotate2D(Angle angle)
        {
            return new Vect3(X * angle.Cos() - Y * angle.Sin(),
                            X * angle.Sin() + Y * angle.Cos(),
                            Z);
        }

        public bool EqualTo(Vect3 vect, double margin)
        {
            return Abs(vect.X - X) < margin
                && Abs(vect.Y - Y) < margin
                && Abs(vect.Z - Z) < margin;
        }

        public bool EqualTo(Vect3 vect)
        {
            return X == vect.X && Y == vect.Y && Z == vect.Z;
        }
        
        public Vect3 MultiplyByComponentFactors(double xFactor, double yFactor, double zFactor)
        {
            return new Vect3(X * xFactor, Y * yFactor, Z * zFactor);
        }

        public Vect3 AddByComponentValues(double xValue, double yValue, double zValue)
        {
            return new Vect3(X + xValue, Y + yValue, Z + zValue);
        }

        public override bool Equals(object obj)
        {
            return obj.GetType() == typeof(Vect3)
                && ((Vect3)obj).X == X
                && ((Vect3)obj).Y == Y
                && ((Vect3)obj).Z == Z;
        }



        public override string ToString()
        {
            return "[" + X.ToString() + ","
                + Y.ToString() + ","
                + Z.ToString() + "]";
        }
    }
}
