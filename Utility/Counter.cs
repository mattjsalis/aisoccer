﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Utility
{
    public class Counter
    {
        private int count = 0;

        public int Count { get => count; }

        public int GetThenIncrement()
        {
            int curr = count;
            count++;
            return curr;
        }

        public int GetThenIncrementBy(int val)
        {
            int curr = count;
            count += val;
            return curr;
        }

        public int[] GetAndIncrementIndexRange(int val)
        {
            if(val == 0)
            {
                return new int[] { count, count };
            }
            int lowerBound = GetThenIncrementBy(val);
            int upperBound = count - 1;
            int[] parameterIndexRange = new int[] { lowerBound, upperBound };
            return parameterIndexRange;
        }
    }
}
