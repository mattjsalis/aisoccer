﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    public class Probability
    {
        private static Random rand = new Random();
        public static int ChooseIntInRange(int max)
        {
            return rand.Next(max);
        }

        public static double RandomBetween(double lowerVal, double upperVal)
        {
            return RandomBetween( lowerVal,  upperVal,  rand);
        }

        public static double RandomBetween(double lowerVal, double upperVal, Random rand)
        {
            return rand.NextDouble() * (upperVal - lowerVal) + lowerVal;
        }
    }
}
