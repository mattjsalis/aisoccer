﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    public class EnumUtility
    {
        public static List<T> GetAsList<T>(Type anEnumType) where T : System.Enum
        {
            List<T> enums = new List<T>();
            foreach (T anEnum in Enum.GetValues(anEnumType))
            {
                enums.Add(anEnum);
            }
            return enums;
        }
    }
}
