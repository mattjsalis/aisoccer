﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;

namespace Utility
{
    public class XMLSerializer<T>
    {

        //Generating a serializer is slow. Add serializer to dictionary for each type as called
        private static Dictionary<Type, XmlSerializer> deserializerDict = new Dictionary<Type, XmlSerializer>();

        public static T Deserialize(string type)
        {
            AddDeserializerIfNecessary();
            return (T)deserializerDict[typeof(T)].Deserialize(new StringReader(type));

        }

        /// <summary>
        /// Adds a serializer for this type to the dictionary
        /// </summary>
        private static void AddDeserializerIfNecessary()
        {
            if (!deserializerDict.ContainsKey(typeof(T)))
            {
                deserializerDict.Add(typeof(T), new XmlSerializer(typeof(T)));
            }
        }

        public static string SerializeToDocumentText(T type, bool isIndented)
        {
            AddDeserializerIfNecessary();
            if (isIndented) { return XDocument.Parse(SerializeToDocument(type).OuterXml).ToString(); }
            return SerializeToDocument(type).OuterXml;
        }

        public static string SerializeToElementText(T type, bool isIndented)
        {
            AddDeserializerIfNecessary();
            if (isIndented) { return XElement.Parse(SerializeToElement(type).OuterXml).ToString();}
            return SerializeToElement(type).OuterXml;
        }

        public static XmlElement SerializeToElement(T type)
        {
            AddDeserializerIfNecessary();
            return SerializeToDocument(type).DocumentElement;
        }

        public static XmlDocument SerializeToDocument(T type)
        {
            AddDeserializerIfNecessary();
            var document = new XmlDocument();
            using (var ms = new MemoryStream())
            {
                deserializerDict[typeof(T)].Serialize(ms, type);
                ms.Position = 0;
                document.Load(ms);
            }
            
            return document;
        }
    }
}
