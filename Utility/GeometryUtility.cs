﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static System.Math;

namespace Utility
{
    public class GeometryUtility
    {

        /// <summary>
        /// Find the wedge from a set of discretized wedges from 0 to 360 deg
        /// with the fewest points in it.
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="wedgeWidth"></param>
        /// <returns> vector defining the middle of the wedge with the fewest points</returns>
        public static SortedDictionary<int,List<Vect3>> GetWedgesWithFewestPoints(double maxDistToConsider, 
            Vect3 origin, int wedgesToConsider, List<Vect3> allPoints)
        {
            SortedDictionary<int, List<Vect3>> wedgeDict = new SortedDictionary<int, List<Vect3>>();

            double wedgeWidth = GetEvenCutWedgeSliceAngleDeg(wedgesToConsider);
            double[] wedgeCenterAngles =  ArrayUtilities.GetLinearlySpacedValues(0.0, 360.0, wedgesToConsider);
            List<Vect3> filteredByDistance = allPoints.Where(p => origin.DistanceToVect(p) > maxDistToConsider).ToList();
            int minValueInWedge = allPoints.Count() + 1;
            Vect3 bestWedge = new Vect3();
            foreach (double wca in wedgeCenterAngles)
            {

                Vect3 relativeWedgeVect = new Vect3(1.0, Angle.FromDegrees(wca));
                Vect3 edge1 = relativeWedgeVect.Rotate2D(Angle.FromDegrees(-wedgeWidth / 2.0));
                Vect3 edge2 = relativeWedgeVect.Rotate2D(Angle.FromDegrees(wedgeWidth / 2.0));
                int pointsInWedge = filteredByDistance.Where(p => IsInWedge(origin, edge1, edge2, p)).Count();
                bool hasKey = wedgeDict.Keys.Contains(pointsInWedge);
                if (hasKey)
                {
                    wedgeDict[pointsInWedge].Add(relativeWedgeVect);
                } 
                else
                {
                    wedgeDict.Add(pointsInWedge, new List<Vect3>());
                    wedgeDict[pointsInWedge].Add(relativeWedgeVect);
                }

            }
            return wedgeDict;
        }

        public static double GetEvenCutWedgeSliceAngleDeg(int n)
        {
            return 360.0 / n;
        }




        /// <summary>
        /// Method to determine if a point is within a
        /// wedge defined by edge1 and edge2. The angle
        /// of the wedge is defined as going from edge1 to edge 2
        /// </summary>
        /// <param name="originPos"></param>
        /// <param name="edge1"></param>
        /// <param name="edge2"></param>
        /// <param name="itemPos"></param>
        /// <returns></returns>
        public static bool IsInWedge(Vect3 originPos, 
            Vect3 edge1, 
            Vect3 edge2,
            Vect3 itemPos)
        {
            if (itemPos.EqualTo(originPos) 
                || itemPos.EqualTo(edge1)
                || itemPos.EqualTo(edge2)
                ) { return true; }
            Vect3 vectOriginToItem = itemPos.Minus(originPos);
            Angle relAngleEdges = edge1.RelativeAngleBetweenVectors(edge2);
            Angle relAngleBetweenEdge1AndItem = edge1.RelativeAngleBetweenVectors(itemPos);
            return relAngleBetweenEdge1AndItem.Degrees <= relAngleEdges.Degrees
                && Sign(edge1.CrossProduct(vectOriginToItem).Z) == Sign(edge1.CrossProduct(edge2).Z);
        }

        /// <summary>
        /// Returns the minimum distance between an item and an
        /// vector starting from an origin
        /// </summary>
        /// <param name="originPos"></param>
        /// <param name="vectToMeasureTo"></param>
        /// <param name="itemPos"></param>
        /// <returns></returns>
        public static double MinDistanceToVectorIntercept(Vect3 originPos, Vect3 vectToMeasureTo, Vect3 itemPos)
        {
            Vect3 vectOriginToItem = itemPos.Minus(originPos);
            Angle relativeAngle = vectOriginToItem.RelativeAngleBetweenVectors(vectToMeasureTo);
            return vectOriginToItem.Magnitude * relativeAngle.Sin();
        }

        /// <summary>
        /// Returns the triangle angle corresponding to opposingSide
        /// by way of the Law of Cosines
        /// </summary>
        /// <param name="oppositeSide"></param>
        /// <param name="side1"></param>
        /// <param name="side2"></param>
        /// <param name="angBtwnOpposingSideANdSide1"></param>
        /// <returns></returns>
        public static Angle GetAngleFromTriangle(double oppositeSide, double side1, double side2,
                                                    Angle angBtwnOpposingSideANdSide1)
        {
            double angleInRad = Acos((Pow(oppositeSide, 2) - Pow(side1, 2) - Pow(side2, 2)) / (-2 * side1 * side2));
            return double.IsNaN(angleInRad) ? Angle.FromDegrees(0) : Angle.FromRadians(angleInRad);
        }

        public static Tuple<Angle, Angle> GetBoundingAzimuths(Vect3 originPos, Vect3 itemPos, Angle azSector)
        {
            Vect3 vectOriginToItem = itemPos.Minus(originPos);
            Angle relAngle = vectOriginToItem.RelativeAngleBetweenVectors(Vect3.XUnit);
            if (vectOriginToItem.Y < 0)
            {
                relAngle = Angle.FromDegrees(-relAngle.Degrees); 
            }

            return Tuple.Create(Angle.FromDegrees(relAngle.Degrees - azSector.Degrees / 2),
                Angle.FromDegrees(relAngle.Degrees + azSector.Degrees / 2));
        }
        public static Pair<double, Angle> getDistanceAndAzimuth(Vect3 pos1, Vect3 pos2)
        {
            Vect3 vectFrom1To2 = pos2.Minus(pos1);
            double distance = vectFrom1To2.Magnitude;
            if (distance == 0.0)
            {
                return new Pair<double, Angle>(distance, Angle.FromDegrees(0.0));
            }
            Angle az = pos1.getAzimuthOfOther(pos2);
            return new Pair<double, Angle>(distance, az);
        }
    }

    

    
}
