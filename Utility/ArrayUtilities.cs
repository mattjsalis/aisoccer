﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    public class ArrayUtilities
    {
        public static double[] GetLinearlySpacedValues(double lowerBound, 
            double upperBound,
            int numValues)
        {
            double[] vals = new double[numValues];
            double interval = (upperBound - lowerBound) / (numValues - 1);
            for(int i = 0; i < numValues; i++)
            {
                vals[i] = lowerBound + i * interval;
            }
            return vals;
        }
    }
}
