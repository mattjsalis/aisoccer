﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    
    public class StealPotential : IPotentialFunction
    {
        public List<StealPotential> allPotentials = null;
        public Vect3 position = null;

        public StealPotential(IEnumerable<Vect3> stealerPositions)
        {
            allPotentials = stealerPositions.Select(sp => new StealPotential(sp)).ToList();
        }
        public StealPotential(Vect3 position)
        {
            this.position = position;
        }

        public double getPotential(Vect3 otherPosition)
        {
            if(allPotentials != null)
            {
                return allPotentials.Select(p => p.getPotential(otherPosition)).Sum();
            }
            double distanceBetweenPlayers = position.DistanceToVect(otherPosition);
            return Math.Exp(-distanceBetweenPlayers);
        }

        public IPotentialFunction getCombinedPotentialFunction(IEnumerable<Vect3> allStealerPositions)
        {
            return new StealPotential(allStealerPositions);
        }

    }
}
