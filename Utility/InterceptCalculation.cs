﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using static System.Math;

namespace Utility
{
    public class InterceptCalculation
    {
        /// <summary>
        /// Time to intercept is calculated using an intercept triangle
        /// 
        /// 
        /// t = (-d*(Ve*cos(az) +- sqrt(Vi^2 - Ve^2*sin^2(az)))
        ///                     / (Vi^2 - Ve^2)
        /// 
        /// With the special case for Vi == Ve where,
        /// 
        /// t = d/(2*V*cos(az))
        /// 
        /// </summary>
        /// <param name="relativeAzimuth">az</param>
        /// <param name="velocityOfInterceptor">Vi</param>
        /// <param name="velocityOfInterceptee">Ve</param>
        /// <param name="startingDistance">d</param>
        /// <returns>null if the intercept isn't possible,
        /// array with one or two time values, 
        /// depending on solution</returns>
        public static double[] GetTimeToIntercepts(Angle relativeAzimuth,
            double velocityOfInterceptor,
            double velocityOfInterceptee,
            double startingDistance)
        {
            //For t to be real, Vi^2 - Ve^2*sin^2(az) must be positive => az <= arcsin(Vi/Ve)
            if (relativeAzimuth.Radians > Asin(velocityOfInterceptor/ velocityOfInterceptee))
            {
                return null;
            }
            //Special case where both speeds are the same
            if (velocityOfInterceptor == velocityOfInterceptee)
            {
                /// t = d/(2*V*cos(az))
                return new double[] { startingDistance / (2 * velocityOfInterceptor * relativeAzimuth.Cos()) };
            }

            double[] positiveTimeVals = null;
            if (velocityOfInterceptor > velocityOfInterceptee)
            {
                if(relativeAzimuth.Degrees <= 90.0)
                {
                    //Only addition in the quadratic solution can return a positive time value
                    positiveTimeVals = calculateInterceptTimes(relativeAzimuth,
                                            velocityOfInterceptor,
                                            velocityOfInterceptee,
                                            startingDistance, false, true);
                    
                } 
                else
                {
                    //Either addition or subtraction in the quadratic solution 
                    // can return a positive time value
                    positiveTimeVals = calculateInterceptTimes(relativeAzimuth,
                                            velocityOfInterceptor,
                                            velocityOfInterceptee,
                                            startingDistance, true, true);
                }
            }
            else if (velocityOfInterceptor < velocityOfInterceptee)
            {
                if (relativeAzimuth.Degrees < 90.0)
                {
                    //Either addition or subtraction in the quadratic solution 
                    // can return a positive time value

                    positiveTimeVals = calculateInterceptTimes(relativeAzimuth,
                                            velocityOfInterceptor,
                                            velocityOfInterceptee,
                                            startingDistance, true, true);
                }
                else
                {
                    //Only subtraction in the quadratic solution can return a positive time value
                    positiveTimeVals = calculateInterceptTimes(relativeAzimuth,
                                            velocityOfInterceptor,
                                            velocityOfInterceptee,
                                            startingDistance, true, false);
                }
            }

            return positiveTimeVals!= null && positiveTimeVals.Length > 0 ? positiveTimeVals : null;
        }

        /// <summary>
        /// Returns min time to intercept
        /// </summary>
        /// <param name="relativeAzimuth"></param>
        /// <param name="velocityOfInterceptor"></param>
        /// <param name="velocityOfInterceptee"></param>
        /// <param name="startingDistance"></param>
        /// <returns>minimum time to intercept or null if intercept isn't possible</returns>
        public static double? GetMinTimeToIntercept(Angle relativeAzimuth,
            double velocityOfInterceptor,
            double velocityOfInterceptee,
            double startingDistance)
        {
            double[] times = GetTimeToIntercepts(relativeAzimuth, velocityOfInterceptor, velocityOfInterceptee, startingDistance);
            return times != null ? times?.Min() : null;
        }



        /// <summary>
        /// Returns max time to intercept
        /// </summary>
        /// <param name="relativeAzimuth"></param>
        /// <param name="velocityOfInterceptor"></param>
        /// <param name="velocityOfInterceptee"></param>
        /// <param name="startingDistance"></param>
        /// <returns>maximum time to intercept or null if intercept isn't possible</returns>
        public static double? GetMaxTimeToIntercept(Angle relativeAzimuth,
            double velocityOfInterceptor,
            double velocityOfInterceptee,
            double startingDistance)
        {
            double[] times = GetTimeToIntercepts(relativeAzimuth, velocityOfInterceptor, velocityOfInterceptee, startingDistance);
            return times != null ? times?.Max() : null;
        }

        public static Angle GetMaxRelativeAngle(double velocityOfInterceptor,
            double velocityOfInterceptee)
        {
            double velRatio = velocityOfInterceptor / velocityOfInterceptee;
            if(velRatio > 1.0) { return Angle.FromDegrees(180.0); }
            double angleInRad = Asin(velocityOfInterceptor / velocityOfInterceptee);
            angleInRad = Min(Asin(velocityOfInterceptor / velocityOfInterceptee), PI);
            return Angle.FromRadians(angleInRad);
        }

        /// <summary>
        /// Calculates the quadratic equation. Returns either an empty array or all positive
        /// time solutions to the quadratic
        /// </summary>
        /// <param name="relativeAzimuth"></param>
        /// <param name="velocityOfInterceptor"></param>
        /// <param name="velocityOfInterceptee"></param>
        /// <param name="startingDistance"></param>
        /// <param name="isPlus"></param>
        /// <param name="isMinus"></param>
        /// <returns></returns>
        private static double[] calculateInterceptTimes(Angle relativeAzimuth,
            double velocityOfInterceptor,
            double velocityOfInterceptee,
            double startingDistance,
            bool isPlus, bool isMinus)
        {
            double num1 = velocityOfInterceptee * relativeAzimuth.Cos();
            double num2 = Sqrt(Pow(velocityOfInterceptor, 2.0) - Pow(velocityOfInterceptee, 2.0) * Pow(relativeAzimuth.Sin(), 2.0));
            double denom = Pow(velocityOfInterceptor, 2.0) - Pow(velocityOfInterceptee, 2.0);

            if (isPlus && isMinus)
            {
                return new double[]
                {
                    -startingDistance*(num1 + num2)/denom,
                    -startingDistance*(num1 - num2)/denom
                }
                .Where(time => time > 0.0)
                .ToArray();
            } 
            else if (isPlus)
            {
                return new double[]
                {
                    -startingDistance*(num1 + num2)/denom
                }.Where(time => time > 0.0)
                .ToArray(); ;
            }
            else if (isMinus)
            {
                return new double[]
                {
                    -startingDistance*(num1 - num2)/denom
                }
                .Where(time => time > 0.0)
                .ToArray(); ;
            }

            return null;
        }
    
        

        public static Vect3 GetMinTimeInterceptVelocityVector(
            double maxSpeedInterceptor,
            Vect3 interceptorPos, Vect3 interceptorVel,
            Vect3 intercepteePos, Vect3 intercepteeVel)
        {
            Vect3 vectBallToPlayer = interceptorPos.Minus(intercepteePos);
            Angle relativeAngleBallVelToBallToPlayer = intercepteeVel.RelativeAngleBetweenVectors(vectBallToPlayer);
            double distanceToBall = interceptorPos.Minus(intercepteePos).Magnitude;
            //Determine the min time to intercept
            double? minTimeToIntercept = InterceptCalculation.GetMinTimeToIntercept(
                                                relativeAngleBallVelToBallToPlayer,
                                                interceptorVel.Magnitude,
                                                intercepteeVel.Magnitude,
                                                distanceToBall
                                            );
            //If intercept isn't possible, try to run blindly towards the ball at full speed
            if (minTimeToIntercept == null)
            {
                return intercepteePos.Minus(interceptorPos).UnitVector.Times(maxSpeedInterceptor);
            }
            //Calculate the distance traveled in order to calculate the whole intercept triangle
            double distancePlayerToTravel = minTimeToIntercept.Value * maxSpeedInterceptor;
            double distanceBallToTravel = minTimeToIntercept.Value * intercepteeVel.Magnitude;
            //Get the angle to run
            Angle relativeInterceptAngle = GeometryUtility.GetAngleFromTriangle(distanceBallToTravel,
                                                    distanceToBall,
                                                    distancePlayerToTravel,
                                                    relativeAngleBallVelToBallToPlayer);

            Vect3 directionForIntercept =
                vectBallToPlayer.UnitVector.Rotate2D(
                                            Angle.FromDegrees(
                                                Sign(vectBallToPlayer.CrossProduct(intercepteeVel).Z)     //Sign determines the direction of rotation
                                                    * (180 - relativeInterceptAngle.Degrees)              //Angle of roation is 180 deg - relative angle
                                                    ));
            return directionForIntercept.Times(maxSpeedInterceptor);
        }
    }
}
