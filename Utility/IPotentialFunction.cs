﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    public interface IPotentialFunction
    {
        double getPotential(Vect3 position);
        IPotentialFunction getCombinedPotentialFunction(IEnumerable<Vect3> positions);
    }
}
