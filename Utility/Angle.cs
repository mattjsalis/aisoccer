﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    /// <summary>
    /// Represents an angle with trig functions that work regardless of
    /// whether it was instantiated in radians or degrees. The angle is
    /// stored in both units and is bounded between 0 to 360 (degrees) and
    /// 0 to 2*pi (radians).
    /// </summary>
    public class Angle
    {
        public double Degrees { get; private set; }
        public double Radians { get; private set; }

        private Angle(double angle, bool isDegrees)
        {
            if (isDegrees)
            {
                Degrees = resolveDegreesTo0To360(angle);
                Radians = resolveRadiansTo0To2Pi(Math.PI / 180 * angle);
            } else
            {
                Degrees = resolveDegreesTo0To360(180 / Math.PI * angle);
                Radians = resolveRadiansTo0To2Pi(angle);
            }
                
        }

        private Angle(double rad)
        {
            Degrees = 180 / Math.PI;
            Radians = rad;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        private double resolveDegreesTo0To360(double angleInDeg)
        {
            return resolveBetweenLowerAndUpperLimit(angleInDeg, 0.0, 360.0);
        }

        private double resolveRadiansTo0To2Pi(double angleInRadians)
        {
            return resolveBetweenLowerAndUpperLimit(angleInRadians, 0.0, 2 * Math.PI);
        }

        private double resolveBetweenLowerAndUpperLimit(double angle, double lowerLimit, double upperLimit)
        {
            while (angle > upperLimit)
            {
                angle -= upperLimit;
                if (angle <= upperLimit) { return angle; }
            }

            while (angle < lowerLimit)
            {
                angle += upperLimit;
                if (angle >= lowerLimit) { return angle; }
            }

            return angle;
        }

        public static Angle FromDegrees(double angleInDegrees)
        {
            return new Angle(angleInDegrees, true);
        }
        public static Angle FromRadians(double angleInRadians)
        {
            return new Angle(angleInRadians, false);
        }

        public double Cos()
        {
            return Math.Cos(Radians);
        }

        public double Sin()
        {
            return Math.Sin(Radians);
        }

        public double Tan()
        {
            return Math.Tan(Radians);
        }


    }
}
