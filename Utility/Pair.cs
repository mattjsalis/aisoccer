﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    public class Pair<T1, T2>
    {
        public T1 First;
        public T2 Second;

        public Pair(T1 first, T2 second)
        {
            First = first;
            Second = second;
        }

        public static Pair<T1, T2> create(T1 first, T2 second) => new Pair<T1, T2>(first, second);
    }
}
