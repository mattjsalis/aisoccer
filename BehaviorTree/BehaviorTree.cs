﻿using ActionPotential;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using System.Xml.XPath;
using Utility;

namespace BehaviorTree
{

    public class BehaviorTree : BoundedParameterizable
    {
        public Counter counter;
        private BehaviorTree[] children = null;
        private Dictionary<PlayTypeEnum, LeafNode> playNodes = new Dictionary<PlayTypeEnum, LeafNode>();
        private Dictionary<PlayTypeEnum, ISamplingStrategy> samplingStrategy = new Dictionary<PlayTypeEnum, ISamplingStrategy>();
        private IMovingEntity User { get; set; }
        public IMovingEntity Ball;

        public BehaviorTree shallowCopyWith(IMovingEntity user)
        {
            BehaviorTree shallowCopy = new BehaviorTree();
            shallowCopy.children = this.children;
            shallowCopy.playNodes = this.playNodes;
            shallowCopy.samplingStrategy = this.samplingStrategy;
            shallowCopy.User = user;
            shallowCopy.Ball = Ball;
            return shallowCopy;
        }


        public override IParameterizable DeepCopy()
        {
            BehaviorTree copy = new BehaviorTree();
            if (children == null)
            {
                foreach(PlayTypeEnum playType in playNodes.Keys)
                {
                    copy.playNodes[playType] = playNodes[playType].DeepCopy() as LeafNode;
                }
                foreach (PlayTypeEnum playType in samplingStrategy.Keys)
                {
                    copy.samplingStrategy[playType] = samplingStrategy[playType].DeepCopy() as ISamplingStrategy;
                }
                return copy;
            }
            copy.children = new BehaviorTree[this.children.Length];
            for(int i = 0; i < children.Length; i++)
            {
                copy.children[i] = children[i].DeepCopy() as BehaviorTree;
            }
            copy.counter = counter;
            copy.Ball = Ball.DeepCopy() as IMovingEntity;
            return copy;
        }

        private BehaviorTree()
        {

        }

        private void SetUserOnLeafNodes(IMovingEntity user)
        {
            if(playNodes.Count > 0)
            {
                foreach (LeafNode leaf in playNodes.Values)
                {
                    leaf.User = user;
                }
                return;
            }
            foreach(BehaviorTree child in children)
            {
                child.SetUserOnLeafNodes(user);
            }
        }

        public static BehaviorTree GetRandomlyGenerated(ref IMovingEntity ball,
            IMovingEntity[] otherTeam, IMovingEntity[] ownTeam, ref IMovingEntity ownGoal, ref IMovingEntity otherGoal,
            ISamplingStrategy sampling)
        {
            Counter counter = new Counter();
            BehaviorTree behaviorTree = new BehaviorTree(ref counter, ref ball, otherTeam, ownTeam, ref ownGoal, ref otherGoal,
                null, null, null, sampling);

            List<double> paramValues = new List<double>();
            for (int i = 0; i < counter.Count; i++)
            {
                paramValues.Add(double.NaN);
            }
            behaviorTree.AssignRandomBoundedValuesToList(paramValues);
            behaviorTree.assignFromParameters(paramValues);
            
            return behaviorTree;
        }

        public BehaviorTree(ref Counter counter, ref IMovingEntity ball,
            IMovingEntity[] otherTeam, IMovingEntity[] ownTeam, ref IMovingEntity ownGoal, ref IMovingEntity otherGoal,
            ISamplingStrategy sampling)
            : this( ref counter, ref ball, otherTeam, ownTeam, ref ownGoal, ref otherGoal,
                null, null, null, sampling)
        {
            Ball = ball;
        }

        private BehaviorTree(ref Counter counter, ref IMovingEntity ball,
            IMovingEntity[] otherTeam, IMovingEntity[] ownTeam, 
            ref IMovingEntity ownGoal, ref IMovingEntity otherGoal,
            PlayerPositionEnum? playerPositionEnum, 
            TeamPossessionEnum? teamPossessionEnum,
            PlayerPosessionEnum? playerPosessionEnum,
            ISamplingStrategy sampling)
        {
            BehaviorTreeNode treeNode = new BehaviorTreeNode(playerPositionEnum,
                teamPossessionEnum, playerPosessionEnum);

            if(treeNode.isTerminal)
            {
                //Get the plays and define the leaf node array
                foreach(PlayTypeEnum allowablePlay in treeNode.allowablePlays)
                {
                    LeafNode leafNode = LeafNode.GetDefaultParameterizablLeafNode(ref counter, ref ball,
                        otherTeam, ownTeam,
                        ref ownGoal, ref otherGoal);
                    playNodes[allowablePlay] = leafNode;
                    samplingStrategy[allowablePlay] = sampling.copyWithNewParamIndexes(ref counter);
                }
                return;
            }

            int numChildren = 0;
            List<PlayerPositionEnum> playerPositionEnums =
                EnumUtility.GetAsList<PlayerPositionEnum>(typeof(PlayerPositionEnum));
            List<TeamPossessionEnum> teamPossEnums =
                EnumUtility.GetAsList<TeamPossessionEnum>(typeof(TeamPossessionEnum));
            List<PlayerPosessionEnum> playerPossEnums =
                EnumUtility.GetAsList<PlayerPosessionEnum>(typeof(PlayerPosessionEnum));
            bool isPlayerPositionLayer = false;
            bool isTeamPossLayer = false;
            bool isPlayerPossLayer = false;
            if (playerPositionEnum == null)
            {
                numChildren = playerPositionEnums.Count;
                isPlayerPositionLayer = true;
            }
            else if(teamPossessionEnum == null)
            {
                numChildren = teamPossEnums.Count;
                isTeamPossLayer = true;
            }
            else
            {
                numChildren = playerPossEnums.Count;
                isPlayerPossLayer = true;
            }
            
            children = new BehaviorTree[numChildren];
            for(int i=0; i < numChildren;i++)
            {
                if(isPlayerPositionLayer)
                {
                    playerPositionEnum = playerPositionEnums[i];
                }
                else if(isTeamPossLayer)
                {
                    teamPossessionEnum = teamPossEnums[i];
                }
                else if (isPlayerPossLayer)
                {
                    playerPosessionEnum = playerPossEnums[i];
                }
                children[i] = new BehaviorTree( ref counter, ref ball,
                     otherTeam, ownTeam, ref ownGoal, ref otherGoal,
                     playerPositionEnum, teamPossessionEnum, playerPosessionEnum,
                     sampling);
            }
            this.counter = counter;
            this.Ball = ball;
        }

        public LeafNode GetLeafNode(PlayerPositionEnum pos,
           TeamPossessionEnum team, PlayerPosessionEnum self,
           PlayTypeEnum type)
        {
            BehaviorTree parent = GetTerminalNode( pos,team, self, type);
            if(parent == null)
            {
                return null;
            }
            if (!parent.playNodes.ContainsKey(type))
            {
                return null;
            }
            return parent.playNodes[type];
        }

        public Tuple<List<double[]>, double[]> ComputeBestAndSuggestedLocs(PlayerPositionEnum pos,
           TeamPossessionEnum team, PlayerPosessionEnum self,
           PlayTypeEnum type, double[] xFieldBounds, double[] yFieldBounds,
           IMovingEntity player)
        {
            BehaviorTree parent = GetTerminalNode(pos, team, self, type);
            if (parent == null)
            {
                return null;
            }
            if (!parent.samplingStrategy.ContainsKey(type))
            {
                return null;
            }
            LeafNode playNode = parent.playNodes[type];
            List<double[]> suggestions = new List<double[]>();
            double[] best = parent.samplingStrategy[type].GetLocationSuggestion(player, Ball, playNode,
                xFieldBounds, yFieldBounds, suggestions);
            return new Tuple<List<double[]>, double[]>(suggestions, best);
        }

        private BehaviorTree GetTerminalNode(PlayerPositionEnum pos,
           TeamPossessionEnum team, PlayerPosessionEnum self,
           PlayTypeEnum type)
        {
            if (team != TeamPossessionEnum.TEAM && self == PlayerPosessionEnum.YES)
            {
                return null;
            }
            Queue<int> addresses = new Queue<int>();
            addresses.Enqueue(((int)pos));
            addresses.Enqueue(((int)team));
            addresses.Enqueue(((int)self));
            BehaviorTree parent = this;
            while (parent.children != null)
            {
                parent = parent.children[addresses.Dequeue()];
            }
            return parent;
        }

        public override void assignFromParameters(List<double> parameters)
        {
            if (playNodes.Count != 0)
            {
                foreach(LeafNode node in playNodes.Values)
                {
                    node.assignFromParameters(parameters);
                }
                foreach (ISamplingStrategy node in samplingStrategy.Values)
                {
                    node.assignFromParameters(parameters);
                }
                return;
            }
            foreach (BehaviorTree child in children)
            {
                child.assignFromParameters(parameters);
            }
        }

        public override void AssignBoundsToList(List<double[]> paramBoundsList)
        {
            if (playNodes.Count != 0)
            {
                foreach (LeafNode node in playNodes.Values)
                {
                    node.AssignBoundsToList(paramBoundsList);
                }
                foreach (ISamplingStrategy node in samplingStrategy.Values)
                {
                    node.AssignBoundsToList(paramBoundsList);
                }
                return;
            }
            foreach (BehaviorTree child in children)
            {
                child.AssignBoundsToList(paramBoundsList);
            }
        }

        public override void AssignRandomBoundedValuesToList(List<double> paramList)
        {
            if (playNodes.Count != 0)
            {
                foreach (LeafNode node in playNodes.Values)
                {
                    node.AssignRandomBoundedValuesToList(paramList);
                }
                foreach (ISamplingStrategy node in samplingStrategy.Values)
                {
                    node.AssignRandomBoundedValuesToList(paramList);
                }
                return;
            }
            foreach (BehaviorTree child in children)
            {
                child.AssignRandomBoundedValuesToList(paramList);
            }
        }

        public override int numParams()
        {
            return 0;
        }

        public List<int> GetRelevantParametersForPlayer(PlayerPositionEnum position)
        {
            List<int> relParams = new List<int>();
            List<PlayerPositionEnum> playerPositionEnums =
                EnumUtility.GetAsList<PlayerPositionEnum>(typeof(PlayerPositionEnum));
            int childIndex = playerPositionEnums.IndexOf(position);
            //Want to get all params indexes for all leafnodes of this player position
            BehaviorTree child = children[childIndex];
            child.AddToRelevantParametersForPlayer(relParams);
            return relParams;
        }

        private void AddToRelevantParametersForPlayer(List<int> indexes)
        {
            if(null == children)
            {
                foreach(LeafNode node in playNodes.Values)
                {
                    indexes.AddRange(node.getAllRelevantParameterIndices());
                }
                return;
            }
            foreach(BehaviorTree child in children)
            {
                child.AddToRelevantParametersForPlayer( indexes);
            }

        }

    }
}
