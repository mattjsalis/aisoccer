﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BehaviorTree
{
    public class BehaviorTreeNode
    {
        public string nodeName { get; }

        public bool isTerminal { get; }
        public PlayTypeEnum[] allowablePlays;

        public BehaviorTreeNode(
            PlayerPositionEnum? playerPositionEnum,
            TeamPossessionEnum? teamPossEnum,
            PlayerPosessionEnum? playPossEnum)
        {
            if( null == playerPositionEnum)
            {
                isTerminal = false;
                return;
            }
            isTerminal = true;
            if (teamPossEnum == null)
            {
                isTerminal = false;
                return;
            }
            else if (teamPossEnum != TeamPossessionEnum.TEAM)
            {
                allowablePlays = new PlayTypeEnum[] { PlayTypeEnum.MOVE_TO };
                return;
            }
            if (playPossEnum == null)
            {
                isTerminal = false;
                return;
            }
            else if (playPossEnum != PlayerPosessionEnum.YES)
            {
                allowablePlays = new PlayTypeEnum[] { PlayTypeEnum.MOVE_TO };
                return;
            }
            allowablePlays = new PlayTypeEnum[] { PlayTypeEnum.MOVE_TO,
                PlayTypeEnum.KICK_BALL_TO };
        }
    }
}
