﻿using ActionPotential;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace BehaviorTree
{
    public class LeafNode : ActionPotential.ActionPotential
    {
        public IMovingEntity User { get; set; }
        private IMovingEntity[] otherTeam;
        private IMovingEntity[] ownTeam;

        //Leaf node member variables
        public ActionPotential.ActionPotential fieldPerception;

        public ActionPotential.ActionPotential otherGoalPerception;
        public ActionPotential.ActionPotential ownGoalPerception;
        public ActionPotential.ActionPotential ballPerception;

        public ActionPotential.ActionPotential otherTeamPlayerPerception;
        public ActionPotential.ActionPotential ownTeamPlayerPerception;

        public LeafNode(ActionPotential.ActionPotential fieldPerception,
            ActionPotential.ActionPotential otherTeamPlayerPerception,
            ActionPotential.ActionPotential ownTeamPlayerPerception,
            ActionPotential.ActionPotential otherGoalPerception,
            ActionPotential.ActionPotential ownGoalPerception,
            ActionPotential.ActionPotential ballPerception,
            IMovingEntity[] otherTeam,
            IMovingEntity[] ownTeam,
            IMovingEntity user=null
            )
        {
            this.fieldPerception = fieldPerception;
            this.otherTeamPlayerPerception = otherTeamPlayerPerception;
            this.ownTeamPlayerPerception = ownTeamPlayerPerception;
            this.otherGoalPerception = otherGoalPerception;
            this.ownGoalPerception = ownGoalPerception;
            this.ballPerception = ballPerception;
            this.otherTeam = otherTeam;
            this.ownTeam = ownTeam;
            this.User = user;
        }

        /**
         * Does not deep copy players
         */
        public override IParameterizable DeepCopy()
        {
            LeafNode copy = new LeafNode(fieldPerception.DeepCopy() as ActionPotential.ActionPotential,
                otherTeamPlayerPerception.DeepCopy() as ActionPotential.ActionPotential,
                ownTeamPlayerPerception.DeepCopy() as ActionPotential.ActionPotential,
                otherGoalPerception.DeepCopy() as ActionPotential.ActionPotential,
                ownGoalPerception.DeepCopy() as ActionPotential.ActionPotential,
                ballPerception.DeepCopy() as ActionPotential.ActionPotential,
                otherTeam, ownTeam, User);
            copy.DeepCopyDictionaries(this);
            return copy;
        }

        public override void assignFromParameters(List<double> parameters)
        {
            fieldPerception.assignFromParameters(parameters);
            otherTeamPlayerPerception.assignFromParameters(parameters);
            ownTeamPlayerPerception.assignFromParameters(parameters);
            ownGoalPerception.assignFromParameters(parameters);
            otherGoalPerception.assignFromParameters(parameters);
            ballPerception.assignFromParameters(parameters);
        }

        public override void AssignBoundsToList(List<double[]> paramBoundsList)
        {
            fieldPerception.AssignBoundsToList(paramBoundsList);
            otherTeamPlayerPerception.AssignBoundsToList(paramBoundsList);
            ownTeamPlayerPerception.AssignBoundsToList(paramBoundsList);
            ownGoalPerception.AssignBoundsToList(paramBoundsList);
            otherGoalPerception.AssignBoundsToList(paramBoundsList);
            ballPerception.AssignBoundsToList(paramBoundsList);
        }

        public override void AssignRandomBoundedValuesToList(List<double> paramList)
        {
            fieldPerception.AssignRandomBoundedValuesToList(paramList);
            otherTeamPlayerPerception.AssignRandomBoundedValuesToList(paramList);
            ownTeamPlayerPerception.AssignRandomBoundedValuesToList(paramList);
            ownGoalPerception.AssignRandomBoundedValuesToList(paramList);
            otherGoalPerception.AssignRandomBoundedValuesToList(paramList);
            ballPerception.AssignRandomBoundedValuesToList(paramList);
        }

        public override List<int> getAllRelevantParameterIndices()
        {
            List<int> result = new List<int>();
            result.AddRange(fieldPerception.getAllRelevantParameterIndices());
            result.AddRange(otherTeamPlayerPerception.getAllRelevantParameterIndices());
            result.AddRange(ownTeamPlayerPerception.getAllRelevantParameterIndices());
            result.AddRange(ownGoalPerception.getAllRelevantParameterIndices());
            result.AddRange(otherGoalPerception.getAllRelevantParameterIndices());
            result.AddRange(ballPerception.getAllRelevantParameterIndices());
            return result;
        }

        public override int numParams()
        {
            return 0;
        }

        bool isMultiThread = false;

        public override double potentialAt(double x, double y, IMovingEntity user)
        {
            User = user;
            return potentialAt(x, y);
        }

        public override double potentialAt(double x, double y)
        {
            if(isMultiThread)
            {
                return potentialAtMultiThreaded( x,  y);
            }
            double pot = 0.0;
            double fieldPot = fieldPerception.potentialAt(x, y);
            double otherGoalPot = otherGoalPerception.potentialAt(x, y);
            double ownGoalPot = ownGoalPerception.potentialAt(x, y);
            double ballGoalPot = 0.0;
            if (User == null ||(User != null &&  !ballPerception.GetMovingEntity().Location.Equals(User.Location)))
            {
                ballGoalPot = ballPerception.potentialAt(x, y);
            }
            
            pot += (fieldPot + otherGoalPot + ownGoalPot + ballGoalPot);

            foreach(IMovingEntity p in ownTeam)
            {
                if(null != User && p.Equals(User))
                {
                    continue;
                }
                ActionPotential.ActionPotential copy = ownTeamPlayerPerception.GetCopyCenteredOn(p);
                pot += copy.potentialAt(x, y);
            }
            foreach (IMovingEntity p in otherTeam)
            {
                ActionPotential.ActionPotential copy = otherTeamPlayerPerception.GetCopyCenteredOn(p);
                pot += copy.potentialAt(x, y);
            }
            return pot;
        }

        private double potentialAtMultiThreaded(double x, double y)
        {
            List<Task<double>> tasks = new List<Task<double>>();

            tasks.Add(Task.Run(() => fieldPerception.potentialAt(x, y)));
            tasks.Add(Task.Run(() => otherGoalPerception.potentialAt(x, y)));
            tasks.Add(Task.Run(() => ownGoalPerception.potentialAt(x, y)));
            tasks.Add(Task.Run(() => ballPerception.potentialAt(x, y)));
            foreach (IMovingEntity p in ownTeam)
            {
                if (null != User && p.Equals(User))
                {
                    continue;
                }
                ActionPotential.ActionPotential copy = ownTeamPlayerPerception.GetCopyCenteredOn(p);
                tasks.Add(Task.Run(() => copy.potentialAt(x, y)));
            }
            foreach (IMovingEntity p in otherTeam)
            {
                ActionPotential.ActionPotential copy = otherTeamPlayerPerception.GetCopyCenteredOn(p);
                tasks.Add(Task.Run(() => copy.potentialAt(x, y)));
            }
            return tasks.Select(t=>t.Result).Sum();
        }

        public override Vect3 gradientAt(double x, double y)
        {
            Vect3 pot = fieldPerception.gradientAt(x, y);
            pot.AddTo(otherGoalPerception.gradientAt(x, y));
            pot.AddTo(ownGoalPerception.gradientAt(x, y));
            pot.AddTo(ballPerception.gradientAt(x, y));
            foreach (IMovingEntity p in ownTeam)
            {
                if (null != User && p.Equals(User))
                {
                    continue;
                }
                ActionPotential.ActionPotential copy = ownTeamPlayerPerception.GetCopyCenteredOn(p);
                pot.AddTo(copy.gradientAt(x, y));
            }
            foreach (IMovingEntity p in otherTeam)
            {
                ActionPotential.ActionPotential copy = otherTeamPlayerPerception.GetCopyCenteredOn(p);
                pot.AddTo(copy.gradientAt(x, y));
            }
            return pot;
        }

        public static LeafNode GetDefaultParameterizablLeafNode(ref Counter counter, ref IMovingEntity ball,
            IMovingEntity[] otherTeam, IMovingEntity[] ownTeam, ref IMovingEntity ownGoal, ref IMovingEntity otherGoal)
        {
            BivariateFunction fieldPerception = GetDefaultBivariateFunction(ref counter);
            ActionPotential.ActionPotential ballPerception = GetDefaultBivariateNormalDistribution(ref ball, ref counter);
            ActionPotential.ActionPotential ownGoalPerception = GetDefaultBivariateNormalDistribution(ref ownGoal, ref counter);
            ActionPotential.ActionPotential otherGoalPerception = GetDefaultBivariateNormalDistribution(ref otherGoal, ref counter);
            IMovingEntity player1Own = ownTeam[0];
            IMovingEntity player1Other = otherTeam[0];
            MacroActionPotential ownTeamPlayerPerception = GetMacroWithTwoBivariateNormalDist(ref player1Own, ref counter);
            MacroActionPotential otherTeamPlayerPerception = GetMacroWithTwoBivariateNormalDist(ref player1Other, ref counter);
            LeafNode leafNode = new LeafNode(fieldPerception, otherTeamPlayerPerception, ownTeamPlayerPerception,
                otherGoalPerception, ownGoalPerception, ballPerception, otherTeam, ownTeam);
            return leafNode;
        }

        public static MacroActionPotential GetMacroWithTwoBivariateNormalDist(ref IMovingEntity entity, ref Counter counter)
        {
            BivariateNormalDistribution bvnd1 = GetDefaultBivariateNormalDistribution(ref entity, ref counter);
            BivariateNormalDistribution bvnd2 = GetDefaultBivariateNormalDistribution(ref entity, ref counter);
            MacroActionPotential twoBivariateNormals = new MacroActionPotential(new List<ActionPotential.ActionPotential>() { bvnd1, bvnd2 });
            return twoBivariateNormals;
        }

        public static MacroActionPotential GetMacroWithTwoGravityPotential(ref IMovingEntity entity, ref Counter counter)
        {
            GravityPotential grav1 = GetDefaultGravityPotential(ref entity, ref counter);
            GravityPotential grav2 = GetDefaultGravityPotential(ref entity, ref counter);
            MacroActionPotential twoGrav = new MacroActionPotential(new List<ActionPotential.ActionPotential>() { grav1, grav2 });
            return twoGrav;
        }

        private static GravityPotential GetDefaultGravityPotential(ref IMovingEntity entity, ref Counter counter)
        {
            BivariateFunction mass = GetDefaultBivariateFunction(ref counter);
            BivariateFunction maxValue = GetDefaultBivariateFunction(ref counter);
            BivariateFunction expX = GetDefaultBivariateFunction(ref counter);
            BivariateFunction expY = GetDefaultBivariateFunction(ref counter);

            GravityPotential gravPotential = new GravityPotential(ref entity);

            gravPotential.RegisterMassFuncs(new IScalingFunction[] { mass }, ref counter);
            gravPotential.RegisterMaxValueFuncs(new IScalingFunction[] { maxValue }, ref counter);
            gravPotential.RegisterExponentXFuncs(new IScalingFunction[] { expX }, ref counter);
            gravPotential.RegisterExponentYFuncs(new IScalingFunction[] { expY }, ref counter);

            return gravPotential;
        }

        private static BivariateNormalDistribution GetDefaultBivariateNormalDistribution(ref IMovingEntity entity, ref Counter counter)
        {
            BivariateNormalDistribution bivariateNormalDistribution = 
                new BivariateNormalDistribution(ref entity);

            BivariateFunction biStdevX = GetDefaultBivariateFunction(ref counter);
            BivariateFunction biStdevY = GetDefaultBivariateFunction(ref counter);
            BivariateFunction biScale = GetDefaultBivariateFunction(ref counter);

            bivariateNormalDistribution.RegisterStdevXFuncs(new IScalingFunction[] { biStdevX }, ref counter);
            bivariateNormalDistribution.RegisterStdevYFuncs(new IScalingFunction[] { biStdevY }, ref counter);
            bivariateNormalDistribution.RegisterScalingValueFuncs(new IScalingFunction[] { biScale }, ref counter);

            //There may be value in learning a correlation but needs to be bounded between -1 and 1.0. TODO for revsit later
            Scalar correlationScalar = new Scalar(0.0);
            correlationScalar.SetBounds(new double[] { 0.0, 0.0 });
            bivariateNormalDistribution.RegisterCorrelationValueFuncs(new IScalingFunction[] { correlationScalar }, ref counter);

            return bivariateNormalDistribution;
        }

        private static BivariateFunction GetDefaultBivariateFunction(ref Counter counter)
        {
            BivariateFunction bivariate = new BivariateFunction(null, null);
            UnivariateFunction[] xFuncs = GetDefaultUnivariateFunctions();
            UnivariateFunction[] yFuncs = GetDefaultUnivariateFunctions();
            bivariate.RegisterXFunction(xFuncs, ref counter);
            bivariate.RegisterYFunction(yFuncs, ref counter);
            return bivariate;
        }

        private static UnivariateFunction[] GetDefaultUnivariateFunctions()
        {

            LogisticFunction logFunc = new LogisticFunction(0.0, 0.0, 0.0);
            logFunc.SetBounds(new double[] {-1000.0, 1000.0,
            0.1, 5.0,
            -1000.0, 1000.0,
            -1000.0, 1000.0});

            Relu relu = new Relu(0.0, 0.0, 0.0, 0.0);
            relu.SetBounds(new double[] {
                -100.0, 100.0,
                -100.0, 100.0,
                1e-6, 1000.0,
                1e-6, 1000.0,
                });

            SinFunction sinFunc = new SinFunction(0.0, 0.0, 0.0);
            sinFunc.SetBounds(new double[] {-1000.0, 1000.0,
            0.1, 10.0,
            -1000.0, 1000.0,
            -1000.0, 1000.0});
            Scalar scalar = new Scalar(0.0);
            scalar.SetBounds(new double[] { -1000.0, 1000.0 });

            NormalDistribution normalDistribution = new NormalDistribution();
            normalDistribution.SetBounds(new double[] { -1000.0, 1000.0,
                0.1, 100.0,
            -1000.0,1000.0});

            UnivariateFunction[] funcs = new UnivariateFunction[]
            {
                logFunc,
                sinFunc,
                scalar,
                normalDistribution,
                relu
            };
            return funcs;
        }

        public override ActionPotential.ActionPotential GetCopyCenteredOn(IMovingEntity entity)
        {
            throw new NotImplementedException();
        }

        public override IMovingEntity GetMovingEntity()
        {
            return User;
        }
    }
}
