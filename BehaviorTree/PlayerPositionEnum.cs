﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BehaviorTree
{
    public enum PlayerPositionEnum
    {
        LEFT_DEFENSE, CENTER_DEFENSE, RIGHT_DEFENSE,
        LEFT_MIDFIELD, CENTER_MIDFIELD, RIGHT_MIDFIELD,
        LEFT_ATTACK, CENTER_ATTACK, RIGHT_ATTACK
    }

    public enum TeamPossessionEnum
    {
        TEAM, OTHER, NONE
    }

    public enum PlayerPosessionEnum
    {
        YES, NO
    }

    public enum PlayTypeEnum
    {
        KICK_BALL_TO, MOVE_TO
    }

    
}
