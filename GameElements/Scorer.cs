﻿using BehaviorTree;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace GameElements
{

    public interface IScoreable
    {
        double score(GoalSide team, PlayerPositionEnum playerPos, Walls walls);
    }

    public class ScoredGoal : IScoreable
    {

        public double score(GoalSide team, PlayerPositionEnum playerPos, Walls walls)
        {
            return 1000.0;
        }
    }

    public class MadePass : IScoreable
    {
        double durationToReceive;
        Vect3 diffFromPassToReceive;

        public MadePass(double durationToReceive, Vect3 diffFromPassToReceive)
        {
            this.durationToReceive = durationToReceive;
            this.diffFromPassToReceive = diffFromPassToReceive;
        }

        public double score(GoalSide team, PlayerPositionEnum playerPos, Walls walls)
        {
            bool isMadeForwardProgress = team == GoalSide.LEFT && diffFromPassToReceive.X > 0
                 || team == GoalSide.RIGHT && diffFromPassToReceive.X < 0;
            double lengthPass = diffFromPassToReceive.Magnitude;
            double speedPass = lengthPass / durationToReceive;
            return isMadeForwardProgress ? speedPass + 1.0 : speedPass;
        }
    }

    public class GainedPossession : IScoreable
    {
        Vect3 locationGainedPoessession;

        public GainedPossession(Vect3 locationGainedPoessession)
        {
            this.locationGainedPoessession = locationGainedPoessession;
        }


        public double score(GoalSide team, PlayerPositionEnum playerPos, Walls walls)
        {

            double highReward = 10.0;

            double[] negXBounds = new double[] { walls.LeftWall, 0.0 };
            double[] posXBounds = new double[] { 0.0, walls.RightWall };
            double[] negYBounds = new double[] { walls.BottomWall, 0.0 };
            double[] posYBounds = new double[] { 0.0, walls.TopWall };

            double[] centerXBounds = new double[] { walls.LeftWall * 2.0/3.0, walls.RightWall * 2.0 / 3.0 };
            double[] centerYBounds = new double[] { walls.BottomWall * 2.0 / 3.0, walls.TopWall * 2.0 / 3.0 };

            bool isInHighRewardArea = false;
            if (team == GoalSide.LEFT)
            {

                if (playerPos.ToString().Contains("LEFT"))
                {
                    //Bias towards +y
                    isInHighRewardArea = locationGainedPoessession.Y > posYBounds[0] && locationGainedPoessession.Y < posYBounds[1];
                }
                else if (playerPos.ToString().Contains("RIGHT"))
                {
                    //Bias towards negative y
                    isInHighRewardArea = locationGainedPoessession.Y > negYBounds[0] && locationGainedPoessession.Y < negYBounds[1];
                }
                else
                {
                    //Bias towards center
                    isInHighRewardArea = locationGainedPoessession.Y > centerYBounds[0] && locationGainedPoessession.Y < centerYBounds[1];
                }

                if (isInHighRewardArea)
                {
                    if (playerPos.ToString().Contains("DEFENSE"))
                    {
                        //Bias towards -x
                        isInHighRewardArea = locationGainedPoessession.X > negXBounds[0] && locationGainedPoessession.X < negXBounds[1];
                    }
                    else if (playerPos.ToString().Contains("MIDFIELD"))
                    {
                        //Bias towards middle x
                        isInHighRewardArea = locationGainedPoessession.X > centerXBounds[0] && locationGainedPoessession.X < centerXBounds[1];
                    }
                    else
                    {
                        //Bias towards +x
                        isInHighRewardArea = locationGainedPoessession.X > posXBounds[0] && locationGainedPoessession.X < posXBounds[1];
                    }
                }
            }
            else
            {
                if (playerPos.ToString().Contains("LEFT"))
                {
                    //Bias towards -y
                }
                else if (playerPos.ToString().Contains("RIGHT"))
                {
                    //Bias towards +y
                    isInHighRewardArea = locationGainedPoessession.Y > posYBounds[0] && locationGainedPoessession.Y < posYBounds[1];
                }
                else
                {
                    //Bias towards center
                    isInHighRewardArea = locationGainedPoessession.Y > centerYBounds[0] && locationGainedPoessession.Y < centerYBounds[1];
                }
                if (isInHighRewardArea)
                {
                    if (playerPos.ToString().Contains("DEFENSE"))
                    {
                        //Bias towards +x
                        isInHighRewardArea = locationGainedPoessession.X > posXBounds[0] && locationGainedPoessession.X < posXBounds[1];
                    }
                    else if (playerPos.ToString().Contains("MIDFIELD"))
                    {
                        //Bias towards middle x
                        isInHighRewardArea = locationGainedPoessession.X > centerXBounds[0] && locationGainedPoessession.X < centerXBounds[1];
                    }
                    else
                    {
                        //Bias towards -x
                        isInHighRewardArea = locationGainedPoessession.X > negXBounds[0] && locationGainedPoessession.X < negXBounds[1];
                    }
                }
            }

            double reward = isInHighRewardArea ? highReward : 1.0;

            return reward;
        }
    }

    public class KeptPossession : IScoreable
    {
        double rewardConstant = 0.05;
        double dur;
        Vect3 diffPosition;

        public KeptPossession(double dur, Vect3 diffPosition)
        {
            this.dur = dur;
            this.diffPosition = diffPosition;
        }

        public double score(GoalSide team, PlayerPositionEnum playerPos, Walls walls)
        {
            bool isMadeForwardProgress = team == GoalSide.LEFT && diffPosition.X > 0
                || team == GoalSide.RIGHT && diffPosition.X < 0;
            double reward = Math.Min(dur * rewardConstant, 1.0);
            //Slight additional reward for making forward progress
            return isMadeForwardProgress ? 1.05 * reward : reward;
        }
    }


    public class Scorer
    {
        private GoalSide team;
        private Walls walls;
        private Dictionary<PlayerPositionEnum, List<IScoreable>> scorables =
            new Dictionary<PlayerPositionEnum, List<IScoreable>>();


        public Scorer(GoalSide team, Walls walls)
        {
            this.team = team;
            this.walls = walls;
        }


        public void AddScoreable(PlayerPositionEnum playerPos, IScoreable scoreable)
        {
            if(!scorables.ContainsKey(playerPos))
            {
                scorables[playerPos] = new List<IScoreable>();
            }
            scorables[playerPos].Add(scoreable);
        }

        /**
         * returns {most valuable player, their score, total team score}
         */
        public Tuple<PlayerPositionEnum, double, double> computeScore()
        {

            Dictionary< PlayerPositionEnum, double> playerScoreDict = GetPlayerScores();
            double totalScore = 0.0;
            double maxPlayerScore = 0.0;
            PlayerPositionEnum positionMvp = PlayerPositionEnum.LEFT_DEFENSE;
            foreach (var scoreableKey in playerScoreDict)
            {
                double playerScore = playerScoreDict[scoreableKey.Key];
                totalScore += playerScore;
                if(maxPlayerScore < playerScore)
                {
                    maxPlayerScore = playerScore;
                    positionMvp = scoreableKey.Key;
                }
            }
            return Tuple.Create(positionMvp, maxPlayerScore, totalScore);
        }

        public Dictionary<PlayerPositionEnum, double> GetPlayerScores()
        {
            Dictionary<PlayerPositionEnum, double> playerScores = new Dictionary<PlayerPositionEnum, double>();
            foreach (var scoreableKey in scorables.Keys)
            {
                var scoreableList = scorables[scoreableKey];
                double playerScore = 0.0;
                foreach (IScoreable scoreable in scoreableList)
                {
                    double scoreForPlay = scoreable.score(team, scoreableKey, walls);
                    // Console.WriteLine(team + "," + scoreableKey + "," + scoreable.GetType().Name + ": " + scoreForPlay);
                    playerScore += scoreForPlay;
                }
                playerScores[scoreableKey] = playerScore;
            }
            return playerScores;
        }

    }
}
