﻿using BehaviorTree;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace GameElements
{
    public class MoveTo : BehaviorTreeAction
    {
        public MoveTo(BehaviorTree.BehaviorTree behaviorTree, double[] xWallBounds, double[] yWallBounds) 
            : base(behaviorTree, xWallBounds, yWallBounds)
        {
        }

        public override IPlay ShallowCopyWithUser(IMovingEntity entity)
        {
            BehaviorTree.BehaviorTree shallowCopy = behaviors.shallowCopyWith(entity);
            MoveTo moveTo = new MoveTo(shallowCopy, xWallBounds, yWallBounds);
            if (locOfBest != null)
            {
                moveTo.locOfBest = new double[2];
                moveTo.locOfBest[0] = locOfBest[0];
                moveTo.locOfBest[1] = locOfBest[1];
            }
            return moveTo;
        }

        public override IPlay DeepCopy()
        {
            return ShallowCopyWithUser(null);
        }

        public override double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            return estimatePlayScore(ownTeam, otherTeam, ball, player, BehaviorTree.PlayTypeEnum.MOVE_TO);
        }

        public override void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            double xDirVel = locOfBest[0] - player.Location.X;
            double yDirVel = locOfBest[1] - player.Location.Y;
            Vect3 newVelPlayer = new Vect3(xDirVel, yDirVel, 0.0).UnitVector.Times(player.MaxRunSpeed);
            player.Velocity = newVelPlayer;
        }

        public override bool isViablePlay(Player player, Ball ball, Team ownTeam, Team otherTeam)
        {
            return true;
        }
    }
}
