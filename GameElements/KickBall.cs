﻿using BehaviorTree;
using GameElements;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Utility;

namespace GameElements
{
    public class KickBall : BehaviorTreeAction
    {

        public KickBall(BehaviorTree.BehaviorTree behaviorTree,
            double[] xWallBounds, double[] yWallBounds) 
            : base(behaviorTree,xWallBounds,yWallBounds)
        {
            
        }

        public override IPlay ShallowCopyWithUser(IMovingEntity entity)
        {
            BehaviorTree.BehaviorTree shallowCopy = behaviors.shallowCopyWith(entity);
            KickBall kickBall = new KickBall(shallowCopy, xWallBounds, yWallBounds);
            if (locOfBest != null)
            {
                kickBall.locOfBest = new double[2];
                kickBall.locOfBest[0] = locOfBest[0];
                kickBall.locOfBest[1] = locOfBest[1];
            }
            return kickBall;
        }

        public override IPlay DeepCopy()
        {
            return ShallowCopyWithUser(null);
        }

        public override double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            return estimatePlayScore( ownTeam,  otherTeam,  ball,  player, PlayTypeEnum.KICK_BALL_TO);
        }

        public override void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            double x = locOfBest[0] - player.Location.X;
            double y = locOfBest[1] - player.Location.Y;
            Vect3 newVelBall = new Vect3(x, y, 0).UnitVector.Times(player.MaxKickSpeed);

            ball.Velocity = newVelBall;
            ownTeam.HasPosession = false;
            player.HasPosession = false;
            player.PositionAtKick = player.Location;
        }

        public override bool isViablePlay(Player player, Ball ball, Team ownTeam, Team otherTeam)
        {
            return player.HasPosession;
        }
    }
}
