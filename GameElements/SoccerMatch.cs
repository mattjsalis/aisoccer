﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Utility;
using NeuralNet;
using SoccerComponents;
using Actions;
using System.Data;
using BehaviorTree;

namespace GameElements
{
    /// <summary>
    /// 
    /// </summary>
    public class SoccerMatch

    {
        public static readonly double TIME_STEP = 0.1;
        public double TIME_TO_CONSIDER_ACTION = 0.4;
        public double TIME_LIMIT { get; set; } = 30;
        private List<GameFrame> gameFrames = new List<GameFrame>();
        private int scoreToWin = int.MaxValue;
        private Team team1;
        private Team team2;
        private Ball ball;
        private DateTime timeOfLastFrame = DateTime.Now;
        private Walls walls;
 
        public static bool IsRandomPlay = true;
        public double time = 0.0;

        public enum EventType
        {
            GAINED_POSSESSION,
            LOST_POSSESSION,
            SCORED_GOAL,
            SCORED_OWN_GOAL
        }

        private Player prevPlayerWithPoss;
        private SortedDictionary<double, Tuple<GoalSide, PlayerPositionEnum, EventType, Vect3>> events = new SortedDictionary<double, Tuple<GoalSide, PlayerPositionEnum, EventType, Vect3>>();

        public static SoccerMatch GetDeepCopy(Walls walls, Team team1, Team team2, Ball ball)
        {
            return new SoccerMatch(walls, ref team1, ref  team2, ref ball,  true);
        }

        public static SoccerMatch GetShallowCopy(Walls walls, ref Team team1, ref Team team2, ref Ball ball)
        {
            return new SoccerMatch(walls, ref team1, ref team2, ref ball,  false);
        }

        private SoccerMatch(Walls walls, ref Team team1, ref Team team2, ref Ball ball, bool isDeepCopy) 
        {
            if (team1.TeamGoalSide.Equals(team2.TeamGoalSide))
            {
                throw new InvalidOperationException("Teams must not be assigned to the same goal side.");
            }
            if (isDeepCopy)
            {
                this.team1 = (Team)team1.DeepCopy();
                this.team2 = (Team)team2.DeepCopy();
                this.ball = (Ball)ball.DeepCopy();
            } else
            {
                this.team1 = team1;
                this.team2 = team2;
                this.ball = ball;
            }

            team1.SetPlayerPositionsToDefault(walls);
            team2.SetPlayerPositionsToDefault(walls);
            this.walls = walls;
        }

        public SoccerMatch DeepCopy()
        {
            return new SoccerMatch(walls, ref team1, ref team2, ref ball, true);
        }

        private void addNewScore(Projection projection)
        {
            Tuple<GoalSide, PlayerPositionEnum, EventType, Vect3> lastPositionWithBall = events.Values.Last();
            if(events.ContainsKey(time))
            {
                events.Remove(time);
            }
            if (projection.Equals(Projection.BALL_SCORED_IN_LEFT_GOAL))
            {
                EventType goalType = lastPositionWithBall.Item1 == GoalSide.LEFT ? EventType.SCORED_OWN_GOAL : EventType.SCORED_GOAL;
                events.Add(time, Tuple.Create<GoalSide, PlayerPositionEnum, EventType, Vect3>(GoalSide.RIGHT,
                        lastPositionWithBall.Item2, goalType, null));
                if (team1.TeamGoalSide.Equals(GoalSide.RIGHT))
                {
                    team1.Score += 1;
                    
                } else
                {
                    team2.Score += 1;
                }
            }
            if (projection.Equals(Projection.BALL_SCORED_IN_RIGHT_GOAL))
            {
                EventType goalType = lastPositionWithBall.Item1 == GoalSide.RIGHT ? EventType.SCORED_OWN_GOAL : EventType.SCORED_GOAL;
                events.Add(time, Tuple.Create<GoalSide, PlayerPositionEnum, EventType, Vect3>(GoalSide.LEFT,
                        lastPositionWithBall.Item2, goalType, null));
                if (team1.TeamGoalSide.Equals(GoalSide.LEFT))
                {
                    team1.Score += 1;
                    
                } else
                {
                    team2.Score += 1;
                }
            }
        }

        private void resetTeamsAndAssignPossession(Projection projection)
        {
            //Reset positions
            team1.SetPlayerPositionsToDefault(walls);
            team2.SetPlayerPositionsToDefault(walls);
            
            if (projection.Equals(Projection.BALL_SCORED_IN_LEFT_GOAL))
            {
                if (team1.TeamGoalSide.Equals(GoalSide.LEFT))
                {
                    team2.HasPosession = false;
                    team1.HasPosession = true;
                    team1.Players[team1.IndexOfPlayerWithBall].HasPosession = true;
                    ball.Location = team1.Players[team1.IndexOfPlayerWithBall].Location;
                    ball.Velocity = team1.Players[team1.IndexOfPlayerWithBall].Velocity;
                } else
                {
                    team1.HasPosession = false;
                    team2.HasPosession = true;
                    team2.Players[team2.IndexOfPlayerWithBall].HasPosession = true;
                    ball.Location = team2.Players[team2.IndexOfPlayerWithBall].Location;
                    ball.Velocity = team2.Players[team2.IndexOfPlayerWithBall].Velocity;
                }
            }

            if (projection.Equals(Projection.BALL_SCORED_IN_RIGHT_GOAL))
            {
                if (team1.TeamGoalSide.Equals(GoalSide.RIGHT))
                {
                    team2.HasPosession = false;
                    team1.HasPosession = true;
                    team1.Players[team1.IndexOfPlayerWithBall].HasPosession = true;
                    ball.Location = team1.Players[team1.IndexOfPlayerWithBall].Location;
                    ball.Velocity = team1.Players[team1.IndexOfPlayerWithBall].Velocity;
                }
                else
                {
                    team1.HasPosession = false;
                    team2.HasPosession = true;
                    team2.Players[team2.IndexOfPlayerWithBall].HasPosession = true;
                    ball.Location = team2.Players[team2.IndexOfPlayerWithBall].Location;
                    ball.Velocity = team2.Players[team2.IndexOfPlayerWithBall].Velocity;
                }
            }
        }

        /// <summary>
        /// Initializes and starts a match
        /// </summary>
        /// <param name="walls">Walls object that defines the boundaries for this game</param>
        /// <param name="allowablePlays">the plays that players may execute</param>
        /// <param name="goalSideWithInitialPossession">the side of the team with initial possession</param>
        /// <param name="runRealTime">boolean indicating if the game should be run in realtime</param>
        /// <returns></returns>
        public Tuple<Scorer, Scorer> InitializeAndStartMatch(Walls walls, 
                                                        List<IPlay> allowablePlaysTeam1,
                                                        List<IPlay> allowablePlaysTeam2,
                                                        GoalSide goalSideWithInitialPossession,
                                                        bool runRealTime, double MULTIPLE_OF_REALTIME=1.0)
        {
            Console.WriteLine("Starting game...");
            //Assign the allowable plays to each player
            foreach (Player player in team1.Players) 
            {
                player.AvailablePlays = allowablePlaysTeam1.Select(play => play.ShallowCopyWithUser(player)).ToList(); 
            }
            foreach (Player player in team2.Players) 
            {
                player.AvailablePlays = allowablePlaysTeam2.Select(play => play.ShallowCopyWithUser(player)).ToList(); 
            }
            //Get the team with initial possession
            Team teamWithPossession = GetTeamWithGoalSide(goalSideWithInitialPossession);
            if (teamWithPossession != null) 
            {
                //If possesion has been assigned to a particular team, then update the 
                // possession booleans for the team
                teamWithPossession.HasPosession = true;
                ball.Location = teamWithPossession.Players[teamWithPossession.IndexOfPlayerWithBall].Location;
                teamWithPossession.Players[teamWithPossession.IndexOfPlayerWithBall].HasPosession = true;
            }

            //Start the clock at zero
            double timeBeforeAction = 0.0;
            DateTime then = DateTime.Now;
            while (time < TIME_LIMIT &&  (team1.Score <= scoreToWin || team2.Score <= scoreToWin))
            {
                if (runRealTime)
                {
                    PauseForRealTime(MULTIPLE_OF_REALTIME);
                }
                bool isPlayersDoAction = false;
                if(timeBeforeAction >= TIME_TO_CONSIDER_ACTION)
                {
                    isPlayersDoAction = true;
                    timeBeforeAction = 0.0;
                }
                playGameFrame(time, isPlayersDoAction);
                time += TIME_STEP;
                timeBeforeAction += TIME_STEP;
                //Console.WriteLine(time);
            }
            return processEvents(walls);
        }

        public Tuple<Scorer, Scorer> processEvents(Walls walls)
        {
            int i = 0;
            double prevTime = 0.0;
            Scorer leftSideScorer = new Scorer(GoalSide.LEFT, walls);
            Scorer rightSideScorer = new Scorer(GoalSide.RIGHT, walls);

            foreach (double time in events.Keys.Skip(1))
            {
                if(i == 0)
                {
                    prevTime = time;
                    i++;
                    continue;
                }
                double dur = time - prevTime;
                var prevData = events[prevTime];
                var currData = events[time];
                prevTime = time;

                
                Scorer scorerToUse = currData.Item1 == GoalSide.LEFT ? leftSideScorer : rightSideScorer;
                if(currData.Item3 == EventType.SCORED_OWN_GOAL)
                {
                   
                    i = 0;
                    if(currData.Item1 == GoalSide.LEFT)
                    {
                        scorerToUse = rightSideScorer;
                    }
                    else
                    {
                        scorerToUse = leftSideScorer;
                    }
                    scorerToUse.AddScoreable(currData.Item2, new ScoredGoal());
                    continue;
                }
                //Goal
                bool isGoal = currData.Item3 == EventType.SCORED_GOAL;
                if(isGoal)
                {
                    scorerToUse.AddScoreable(currData.Item2, new ScoredGoal());
                    i = 0; //Reset i because play will reset
                    continue;
                }
                Vect3 changeInLoc = new Vect3();
                if (prevData.Item4 != null)
                {
                    changeInLoc = currData.Item4.Minus(prevData.Item4);
                }
                
                Vect3 locationOfReceival = currData.Item4;
                //Kept Posession
                bool isKeptPossession = currData.Item1 == prevData.Item1
                    && currData.Item2 == prevData.Item2
                    && currData.Item3 == EventType.LOST_POSSESSION;

                if (isKeptPossession)
                {
                    KeptPossession keptPossession = new KeptPossession(dur, changeInLoc);
                    scorerToUse.AddScoreable(currData.Item2, keptPossession);
                    continue;
                }

                //Pass
                bool isPass = currData.Item1 == prevData.Item1 
                    && currData.Item2 != prevData.Item2
                    && currData.Item3 == EventType.GAINED_POSSESSION;

                if(isPass)
                {
                    MadePass pass = new MadePass(dur, changeInLoc);
                    scorerToUse.AddScoreable(currData.Item2, pass);
                    continue;
                }

                //Gained Possession
                if(currData.Item3 == EventType.GAINED_POSSESSION)
                {
                    GainedPossession gainedPossession = new GainedPossession(locationOfReceival);
                    scorerToUse.AddScoreable(currData.Item2, gainedPossession);
                    continue;
                }

            }
            return Tuple.Create(leftSideScorer, rightSideScorer);
        }

        /// <summary>
        /// Pauses until the clock time matches the game time
        /// </summary>
        private void PauseForRealTime(double multipleOfRealTime)
        {
            while((DateTime.Now - timeOfLastFrame).TotalSeconds < TIME_STEP / multipleOfRealTime) {}
            timeOfLastFrame = DateTime.Now;
        }


        private void Walls_RaiseBallScoredEvent(object sender, Projection projection)
        {
            addNewScore(projection);
            resetTeamsAndAssignPossession(projection);
        }

        private void playGameFrame(double time, bool isMakePlayersDoAction)
        {
            //Add the current state of the game to the queue
            //gameFrames.Add(GameFrame.GetDeepCopy(time, team1, team2, ball));
            //Ensure only one player has possesion of the ball at a time
            Player playerWithBall = vieForPossession();
            if(playerWithBall != null || prevPlayerWithPoss != null)
            {
                
                if(null == playerWithBall)
                {
                    events.Add(time, new Tuple<GoalSide, PlayerPositionEnum, EventType, Vect3>
                        (prevPlayerWithPoss.GoalSide, prevPlayerWithPoss.PositionEnum, EventType.LOST_POSSESSION, prevPlayerWithPoss.Location.Copy()));
                }
                else if(!playerWithBall.Equals(prevPlayerWithPoss))
                {
                    events.Add(time, new Tuple<GoalSide, PlayerPositionEnum, EventType, Vect3>
                        (playerWithBall.GoalSide, playerWithBall.PositionEnum, EventType.GAINED_POSSESSION, playerWithBall.Location.Copy()));
                }
                
            }
            prevPlayerWithPoss = playerWithBall;
            Projection? ballProjection = null;

            //Make the players do something
            if (isMakePlayersDoAction)
            {
                team1.MakeAllPlayersPlay(team2, ball, ref ball);
                team2.MakeAllPlayersPlay(team1, ball, ref ball);
            }
            //Update positions
            team1.UpdatePlayersPositions(walls);
            team2.UpdatePlayersPositions(walls);

            if (playerWithBall != null && playerWithBall.HasPosession)
            {
                ball.Velocity = playerWithBall.Velocity;
                ball.Location = playerWithBall.Location;
                ballProjection = walls.GetProjectionType(ball.Location, ball.ProjectedPosition, true);
            }
            else
            {
                ballProjection = ball.UpdatePosition(walls);
            }
            if (ballProjection == Projection.BALL_SCORED_IN_LEFT_GOAL
                || ballProjection == Projection.BALL_SCORED_IN_RIGHT_GOAL)
            {
                this.Walls_RaiseBallScoredEvent(this, ballProjection.Value);
                return;
            }
        }

        /// <summary>
        /// Method that resolves the possesion of the ball
        /// </summary>
        private Player vieForPossession()
        {
            //Get all players are close enough to the ball to vie for possesion
            List<Player> viablePlayers = team1.GetPlayersWithStealViability(ball.Location);
            viablePlayers.AddRange(team2.GetPlayersWithStealViability(ball.Location));
            
            if (viablePlayers.Count() == 0)
            {
                return null; 
            }
            else if(viablePlayers.Count() == 1)
            {
                if (viablePlayers[0].HasPosession)
                {
                    //If only one player is viable and that player already has possesion, do nothing
                    return viablePlayers[0];
                } 
                else
                {
                    
                    //If the player is of a different side then invoke the changed sides event
                    if(team1.TeamGoalSide == viablePlayers[0].GoalSide)
                    {
                        team1.HasPosession = false;
                        viablePlayers[0].HasPosession = true;
                        team1.HasPosession = true;
                        team2.HasPosession = false;
                    }
                    else
                    {
                        team2.HasPosession = false;
                        viablePlayers[0].HasPosession = true;
                        team2.HasPosession = true;
                        team1.HasPosession = false;
                    }
                    return viablePlayers[0];
                }
                    
            }

            //Set all players to not have possesion before assigning possession
            int winningInd = Probability.ChooseIntInRange(viablePlayers.Count() - 1);
            //Assign possesion to the winning player
            
            if (team1.TeamGoalSide == viablePlayers[winningInd].GoalSide)
            {
                team1.HasPosession = false;
                viablePlayers[winningInd].HasPosession = true;
                team1.HasPosession = true;
                team2.HasPosession = false;
            }
            else
            {
                team2.HasPosession = false;
                viablePlayers[winningInd].HasPosession = true;
                team2.HasPosession = true;
                team1.HasPosession = false;
            }
            return viablePlayers[winningInd];
        }

        private Team GetTeamWithGoalSide(GoalSide goalSide)
        {
            if (goalSide.Equals(GoalSide.NONE)) { return null; }
            return team1.TeamGoalSide.Equals(goalSide) ? team1 : team2;
        }

    }
}
