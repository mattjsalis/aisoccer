﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Utility;
using System.Linq;
using SoccerComponents;
using Actions;
using BehaviorTree;

namespace GameElements
{
    public class Player : CommonMovingEntity
    {

        public PlayerPositionEnum PositionEnum { get; set; }
        public string TeamName { get; set; }
        [XmlIgnore]
        public GoalSide GoalSide { get; set; }

        public Vect3 PositionAtKick { get; set; } = null;
        private double stealDistance = 5.0;

        public double TimeBeforeInterceptBall = 0.0;

        public double DistanceOnRunToGetOpen = 0.0;

        public IPlay PlayIMade;

        [XmlIgnore]
        public double MaxKickSpeed { get; } = 75.0;
        [XmlIgnore]
        public double MaxRunSpeed { get; } = 40.0;
        private bool hasPossession = false;
        public bool HasPosession {
            get {
                return hasPossession;
            }
            set {
                hasPossession = value;
                if (value) { HasBeenPassedTo = false; }
            }
        }
        private List<IPlay> availablePlays;
        [XmlIgnore]
        public List<IPlay> AvailablePlays {
            get
            {
                return availablePlays;
            }
            set
            {
                availablePlays = value.Select(play => play.ShallowCopyWithUser(this)).ToList();
            }
        }

        public bool HasBeenPassedTo { get; set; } = false;

        private Player(Player player) : base(player) {
            HasPosession = player.HasPosession;
            HasBeenPassedTo = player.HasBeenPassedTo;
            PositionAtKick = player.PositionAtKick?.Copy();
            GoalSide = player.GoalSide;
            TimeBeforeInterceptBall = player.TimeBeforeInterceptBall;
            DistanceOnRunToGetOpen = player.DistanceOnRunToGetOpen;
            PlayIMade = player.PlayIMade?.DeepCopy();
            PositionEnum = player.PositionEnum;
            availablePlays = player.AvailablePlays;
        }

        public Player(){}

        public Player(Vect3 position, Vect3 velocity)
        {
            Location = position?.Copy();
            Velocity = velocity?.Copy();
            
        }

        public Vect3 Play(ref Team ownTeamRef,
            Team ownTeam, 
            Team otherTeam, 
            Ball ball) 
        {
            this.TimeBeforeInterceptBall = Math.Max(0.0, TimeBeforeInterceptBall - SoccerMatch.TIME_STEP);
            this.DistanceOnRunToGetOpen = Math.Max(0.0, DistanceOnRunToGetOpen - SoccerMatch.TIME_STEP*Velocity.Magnitude);
            Player player = new Player(this);

            IPlay thePlay = 
                thePlay = availablePlays
                .Where(aPlay => aPlay.isViablePlay(this, ball,ownTeam, otherTeam))
                .OrderBy(play => play.estimatePlayScore(ownTeam, otherTeam, ball, this))
                .LastOrDefault();

            Ball refBall = new Ball(ball);

            if (thePlay != null)
            {
                this.PlayIMade = thePlay.ShallowCopyWithUser(this);
                thePlay.executePlay(ref ownTeamRef, otherTeam, ref refBall, ref player);
                this.Velocity = player.Velocity;
                this.HasPosession = player.HasPosession;
                this.PositionAtKick = player.PositionAtKick;
                this.DistanceOnRunToGetOpen = player.DistanceOnRunToGetOpen;
                this.TimeBeforeInterceptBall = player.TimeBeforeInterceptBall;
            }
            return thePlay is KickBall ?  refBall.Velocity : null;
        }

        public bool IsWithinStealingRange(Vect3 ballPos)
        {
            //If the player recently kicked the ball, make sure the ball has gone
            // at least as far as it's steal distance before it can steal again
            //if(TimeBeforeInterceptBall > 0.0) { return false; }
            double distanceToBall = Location.DistanceToVect(ballPos);
            return distanceToBall <= stealDistance;
        }

        public override IEntity DeepCopy()
        {
            return new Player(this);
        }

        /// <summary>
        /// Restores any state variables affecting the player to default values
        /// </summary>
        public void ResetPlayer()
        {
            TimeBeforeInterceptBall = 0.0;
            DistanceOnRunToGetOpen = 0.0;
            PositionAtKick = null;
        }

        public override bool Equals(object obj)
        {
            if(obj is Player)
            {
                Player other = (Player)obj;
                return other.GoalSide == GoalSide && other.PositionEnum == PositionEnum;
            }
            return false;
        }


    }
}
