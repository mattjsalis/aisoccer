﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace GameElements
{
    /// <summary>
    /// Class to capture the data of the game at a single instant
    /// </summary>
    [Serializable]
    public class GameFrame
    {

        public double Time = 0.0;
        public Team Team1 { get; set; }
        public Team Team2 { get; set; }
        public Ball Ball { get; set; }

        private GameFrame(double time, Team team1, Team team2, Ball ball, bool isDeepCopy)
        {
            Time = time;
            if (isDeepCopy)
            {
                this.Ball = (Ball)ball.DeepCopy();
                this.Team1 = (Team)team1.DeepCopy();
                this.Team2 = (Team)team2.DeepCopy();
            }
            else
            {
                this.Ball = (Ball)ball;
                this.Team1 = (Team)team1;
                this.Team2 = (Team)team2;
            }
                
        }

        public static  GameFrame GetDeepCopy(double time, Team team1, Team team2, Ball ball)
        {
            return new GameFrame(time, team1, team2, ball, true);
        }

        public static GameFrame GetShallowCopy(double time, Team team1, Team team2, Ball ball)
        {
            return new GameFrame(time, team1, team2, ball, false);
        }
    }
}
