﻿using GameElements;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;
using static System.Math;

namespace GameElements
{
    public class InterceptBall : IPlay
    {

        public bool didAttemptIntercept = false;

        public double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            if (SoccerMatch.IsRandomPlay)
            {
                return Probability.RandomBetween(0.0, 1.0);
            }
            //Todo: actually define this scoring
            return Probability.RandomBetween(0, 1);
        }

        public void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            Vect3 newPlayerVel = InterceptCalculation.GetMinTimeInterceptVelocityVector(player.MaxRunSpeed,
                player.Location, player.Velocity, ball.Location, ball.Velocity);
            didAttemptIntercept = true;
            player.Velocity = newPlayerVel;

        }


        public IPlay DeepCopy()
        {
            InterceptBall play = new InterceptBall();
            play.didAttemptIntercept = didAttemptIntercept;
            return play;
        }
        private static readonly int NUM_PLAYERS_ALLOWED_TO_INTERCEPT = 3;
        public bool isViablePlay(Player player,Ball ball, Team ownTeam, Team otherTeam)
        {
            bool isSucceedFast = player.TimeBeforeInterceptBall == 0.0 &&
                !ownTeam.HasPosession && !player.HasPosession
                && player.PositionAtKick == null;
            if (!isSucceedFast)
            {
                return false;
            }

            ////Am I one of the closest players?
            //IOrderedEnumerable<Vect3> orderedByDistToBall = ownTeam.GetPositions().OrderBy(p => p.DistanceToVect(ball.Position));
            //for (int i = 0; i < NUM_PLAYERS_ALLOWED_TO_INTERCEPT; i++)
            //{
            //    bool isOneOfClosest = orderedByDistToBall.ElementAt(i).Equals(player.Position);
            //    if (isOneOfClosest)
            //    {
            //        return true;
            //    }
            //}
            return true;

        }

        public IPlay ShallowCopyWithUser(IMovingEntity user)
        {
            throw new NotImplementedException();
        }
    }
}
