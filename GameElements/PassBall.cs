﻿using NeuralNet;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;
using System.Linq;
using GameElements;
using SoccerComponents;

namespace Actions
{
    public class PassBall : IPlay
    {

        private int numberOfSpeedsToEvaluate = 10;
        private int numberOfAzimuthsToEvalaute = 10;
        private Angle passSector = Angle.FromDegrees(90);
        

        public BestPass bestPass = new BestPass();


        /// <summary>
        /// Give a slight advantage to forward passes
        /// </summary>
        private Func<double, GoalSide, double> passScoreFunc = (changeInXOfPass, goalSide) =>
        {
            if (goalSide.Equals(GoalSide.LEFT))
            {
                if(changeInXOfPass < 0.0)
                {
                    return 0.95;
                }
                else
                {
                    return 1.05;
                }
            }
            if (goalSide.Equals(GoalSide.RIGHT))
            {
                if (changeInXOfPass < 0.0)
                {
                    return 1.05;
                }
                else
                {
                    return 0.95;
                }
            }
            return 1.0;
        };

        /// <summary>
        /// Neural net to predict the probability of a successful shot
        /// </summary>
        private ArtificialNeuralNet neuralNet = new ArtificialNeuralNet(2, 1, new int[] { 3, 3 }, -5, 5, -5, 5)
                                .SetOutputLayerActivationFunction(ActivationFunctionType.SIGMOID);

        private double TIME_BEFORE_CAN_INTERCEPT_AGAIN = 2.0;

        public void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            player.TimeBeforeInterceptBall = TIME_BEFORE_CAN_INTERCEPT_AGAIN;
            ball.Velocity = bestPass.PassVect.Times(bestPass.PassSpeed);
            ownTeam.Players[bestPass.PlayerToPassToInd].HasBeenPassedTo = true;
            player.HasPosession = false;
            player.PositionAtKick = null;
        }

        private double calculatePassScore(Vect3 passingPlayerPos,
                                                        Vect3 vectorToReceivingPlayer,
                                                            Vect3 passVector,
                                                            double passSpeed,
                                                            double playerSpeed,
                                                            GoalSide goalSide,
                                                            Team otherTeam)
        {
            //Find the predicted point of intercept of the own player obtaining pass
            double? minTimeToIntercept = InterceptCalculation.GetMinTimeToIntercept(
                passVector.RelativeAngleBetweenVectors(vectorToReceivingPlayer),
                playerSpeed,
                passSpeed,
                vectorToReceivingPlayer.Magnitude);
            if (minTimeToIntercept == null) { return double.MinValue; }

            //Get the number of players who could intercept the ball before my player
            int playersWithBetterInterceptChance = otherTeam.Players
                        .Select(player =>
                        {
                            Vect3 vectToOtherPlayer = player.Location.Minus(passingPlayerPos);
                            return InterceptCalculation.GetMinTimeToIntercept(
                                      passVector.RelativeAngleBetweenVectors(vectToOtherPlayer),
                                      player.MaxRunSpeed,
                                      passSpeed,
                                      vectToOtherPlayer.Magnitude);
                        })
                        .Where(timeToIntercept => timeToIntercept != null && timeToIntercept.Value < minTimeToIntercept)
                        .Count();

            //Calculate the probabilty of completing the pass
            double probabilityOfPass = neuralNet.CalculateNeuralNet(playersWithBetterInterceptChance, vectorToReceivingPlayer.Magnitude)[0];

            //return passScoreFunc.Invoke(passVector.X, goalSide)* probabilityOfPass;
            return probabilityOfPass;
        }

        /// <summary>
        /// Estimate the best score acheivable by a pass in this situation
        /// </summary>
        /// <param name="ownTeam"></param>
        /// <param name="otherTeam"></param>
        /// <param name="ball"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            
            //For now select 10 linearly spaced pass speeds and evaluate the results of each
            double[] kickSpeeds = ArrayUtilities.GetLinearlySpacedValues(10, player.MaxKickSpeed, numberOfSpeedsToEvaluate);
            int i_player = 0;
            foreach (Player teammate in ownTeam.Players)
            {
                if (player.Location.EqualTo(teammate.Location))
                {
                    i_player++;
                    continue;
                }
                Vect3 vectToTeammate = teammate.Location.Minus(player.Location);
                //Get the azimuths for the pass sector for this teammate
                Tuple<Angle, Angle> passAzimuthBounds =
                            GeometryUtility.GetBoundingAzimuths(player.Location,
                                    teammate.Location,
                                    passSector);
                Angle[] passAngles = ArrayUtilities
                                            .GetLinearlySpacedValues(passAzimuthBounds.Item1.Degrees,
                                                                     passAzimuthBounds.Item2.Degrees,
                                                                     numberOfAzimuthsToEvalaute)
                                            .Select(dub => Angle.FromDegrees(dub))
                                            .ToArray();
                //Evaluate azimuth and speed combinations
                for (int i_ks = 0; i_ks < numberOfSpeedsToEvaluate; i_ks++)
                {
                    foreach (Angle passAngle in passAngles)
                    {
                        
                        //Calculate the score for this pass
                        double passScore = calculatePassScore(player.Location,
                            vectToTeammate,
                            new Vect3(1, passAngle),
                            kickSpeeds[i_ks],
                            teammate.MaxRunSpeed,
                            ownTeam.TeamGoalSide,
                            otherTeam);
                        //Update the maximum cost found for the player
                        if(passScore > bestPass.PassScore)
                        {
                            bestPass.PassScore = passScore;
                            bestPass.PlayerToPassToInd = i_player;
                            bestPass.PassSpeed = kickSpeeds[i_ks];
                            bestPass.PassVect = new Vect3(1.0, passAngle);
                        }
                    }
                }

                i_player++;
            }
            if (SoccerMatch.IsRandomPlay)
            {
                return Probability.RandomBetween(0.0, 1.0);
            }
            return bestPass.PassScore;
        }

        public IPlay DeepCopy()
        {
            PassBall pass = new PassBall();
            pass.bestPass = bestPass.DeepCopy();
            return pass; ;
        }

        public bool isViablePlay(Player player,Ball ball, Team ownTeam, Team otherTeam)
        {
            return player.HasPosession;
        }

        public IPlay ShallowCopyWithUser(IMovingEntity user)
        {
            throw new NotImplementedException();
        }

        public class BestPass
        {
            public double PassScore { get; set; } = double.MinValue;
            public int PlayerToPassToInd { get; set; }
            public double PassSpeed { get; set; }
            public Vect3 PassVect { get; set; }

            public BestPass DeepCopy()
            {
                BestPass p = new BestPass();
                p.PassScore = PassScore;
                p.PlayerToPassToInd = PlayerToPassToInd;
                p.PassSpeed = PassSpeed;
                p.PassVect = PassVect?.Copy();
                return p;
            }
        }
    }
}



