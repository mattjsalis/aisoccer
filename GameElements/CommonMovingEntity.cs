﻿using System;
using Utility;
using System.Xml.Serialization;
using SoccerComponents;

namespace GameElements
{
    [Serializable]
    public abstract class CommonMovingEntity : IMovingEntity
    {
        public Vect3 Location { get; set; } = new Vect3();
        public Vect3 Velocity {get;set;} = new Vect3();
        [XmlIgnore]
        public Vect3 ProjectedPosition => Location.Plus(Velocity.Times(SoccerMatch.TIME_STEP));

        protected CommonMovingEntity() { }

        protected CommonMovingEntity(CommonMovingEntity component)
        {
            Location = component.Location.Copy();
            Velocity = component.Velocity.Copy();
        }

        public virtual Projection? UpdatePosition(Walls walls)
        {
            Tuple<Vect3, Vect3> posAndVelTup = walls.ResolveWallBounce(Location,
                ProjectedPosition,
                Velocity,
                false);
            Location = posAndVelTup.Item1; Velocity = posAndVelTup.Item2;
            return null;
        }

        public abstract IEntity DeepCopy();

    }
}
