﻿using System;
using System.Collections.Generic;
using System.Text;
using Utility;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using SoccerComponents;
using BehaviorTree;
using System.Threading.Tasks;

namespace GameElements
{
    /// <summary>
    /// Refers to the side of this teams own goal
    /// </summary>
    public enum GoalSide
    {
        NONE = 0,
        LEFT,
        RIGHT
    }
    [Serializable]
    public class Team : IEntity
    {
        /// <summary>
        /// The index of the player that has the ball upon resets
        /// </summary>
        [XmlIgnore]
        public int IndexOfPlayerWithBall
        {
            get;
            set;
        } = 0;

        /// <summary>
        /// The position configuration of players at the start of the game or after
        /// a point has been scored. Values are relative field origin (field center) 0, where:
        ///  X : Left wall == -1, Right wall == 1
        ///  Y : Bottom wall == -1, Top wall == 1
        /// </summary>
        /// 

        private Vect3[] defaultConfiguration = null;

        public PlayerPositionEnum[] DefaultPositions { get
            {
                if (TeamGoalSide.Equals(GoalSide.LEFT))
                {
                    return new PlayerPositionEnum[]
                    {
                        PlayerPositionEnum.RIGHT_ATTACK, PlayerPositionEnum.CENTER_ATTACK,PlayerPositionEnum.LEFT_ATTACK,
                        PlayerPositionEnum.RIGHT_MIDFIELD, PlayerPositionEnum.CENTER_MIDFIELD, PlayerPositionEnum.LEFT_MIDFIELD,
                        PlayerPositionEnum.RIGHT_DEFENSE, PlayerPositionEnum.CENTER_DEFENSE, PlayerPositionEnum.LEFT_DEFENSE,
                    };
                }
                else
                {
                    return new PlayerPositionEnum[]
                    {
                        PlayerPositionEnum.LEFT_ATTACK, PlayerPositionEnum.CENTER_ATTACK,PlayerPositionEnum.RIGHT_ATTACK,
                        PlayerPositionEnum.LEFT_MIDFIELD, PlayerPositionEnum.CENTER_MIDFIELD, PlayerPositionEnum.RIGHT_MIDFIELD,
                        PlayerPositionEnum.LEFT_DEFENSE, PlayerPositionEnum.CENTER_DEFENSE, PlayerPositionEnum.RIGHT_DEFENSE,
                    };
                }
            }
        }

        /// <summary>
        /// Default configuration. Either a default set of vectors based on
        /// TeamGoalSide or the value defined by the user.
        /// </summary>
        [XmlIgnore]
        public Vect3[] DefaultConfiguration
        {
            get
            {
                if (defaultConfiguration != null) { return defaultConfiguration; }
                if (TeamGoalSide.Equals(GoalSide.LEFT))
                {
                    return new Vect3[]
                            {//Attackers
                            new Vect3(-.1, -.5, 0),
                            new Vect3(-.1, 0, 0),
                            new Vect3(-.1, .5, 0),
                            //Midfielders
                            new Vect3(-.33, -.5, 0),
                            new Vect3(-.33, 0, 0),
                            new Vect3(-.33, .5, 0),
                            //Defenders
                            new Vect3(-.66, -.5, 0),
                            new Vect3(-.66, 0, 0),
                            new Vect3(-.66, .5, 0)};
                }
                else if (TeamGoalSide.Equals(GoalSide.RIGHT))
                {
                    return new Vect3[]
                            {//Attackers
                            new Vect3(.1, -.5, 0),
                            new Vect3(.1, 0, 0),
                            new Vect3(.1, .5, 0),
                            //Midfielders
                            new Vect3(.33, -.5, 0),
                            new Vect3(.33, 0, 0),
                            new Vect3(.33, .5, 0),
                            //Defenders
                            new Vect3(.66, -.5, 0),
                            new Vect3(.66, 0, 0),
                            new Vect3(.66, .5, 0)};
                }
                return defaultConfiguration;
            }

            set
            {
                //Check the bounds of the vectors to make sure all coordinates are
                // bounded between -1 and 1
                if (!value.All(vect => vect.X >= -1 && vect.X <= 1
                                        && vect.Y >= -1 && vect.Y <= 1
                                        && vect.Z == 0 // No floating players
                                        )
                  )
                {
                    throw new InvalidOperationException("XY Coordinates of default configuration must be bounded between -1 and 1" +
                        " and Z coordinates must be equal to 0 for team " + Name);
                }
                else if (Players.Length != 0 && value.Length != Players.Length)
                {
                    //If players have been defined, then check that an equal amount of 
                    // configuration vectors have been provided
                    throw new InvalidOperationException("The amounnt of default configuration vectors must match the amount of" +
                        " players for team " + Name);
                }
                defaultConfiguration = value;
            }
        }

        [XmlAttribute]
        public string Name
        {
            get;
            set;
        }
        [XmlAttribute]
        public GoalSide TeamGoalSide
        {
            get;
            set;
        }

        private bool hasPossession = false;
        public bool HasPosession
        {
            get
            {
                return Players.Where(player => player.HasPosession).Any();
            }
            set
            {
                if (!value)
                {
                    if (null != Players)
                    {
                        //Ensure all players don't have possession if team doesn't
                        foreach (Player player in Players)
                        {
                            if (null == player) { continue; }
                            player.HasPosession = false;
                        }
                    }
                }
                hasPossession = value;
            }
        }

        public int Score { get; set; } = 0;
        private Player[] players = null;
        public Player[] Players
        {
            get
            {
                return players;
            }
            set
            {
                players = new Player[value.Length];
                for (int i_plyr = 0; i_plyr < value.Length; i_plyr++)
                {
                    if (value[i_plyr] == null)
                    {
                        players[i_plyr] = new Player();
                    }
                    else
                    {
                        players[i_plyr] = value[i_plyr];
                    }
                }
            }
        }

        /// <summary>
        /// Initializes a team with the hardcoded position defaults based off of
        /// the provided goal side.
        /// </summary>
        /// <param name="goalSide"></param>
        /// <returns></returns>
        public static Team InitializeDefaultTeam(GoalSide goalSide)
        {
            if (goalSide.Equals(GoalSide.NONE))
            {
                throw new InvalidOperationException("Team cannot be initialized with side " + GoalSide.NONE.ToString());
            }
            Team team = new Team();
            team.TeamGoalSide = goalSide;
            team.defaultConfiguration = team.DefaultConfiguration;
            team.Players = new Player[team.DefaultConfiguration.Length];
            team.Name = goalSide.Equals(GoalSide.LEFT) ? DefaultTeam1Name : DefaultTeam2Name;
            PlayerPositionEnum[] positions = team.DefaultPositions;

            for (int i_player = 0; i_player < team.DefaultConfiguration.Length; i_player++)
            {
                team.Players[i_player] = new Player();
                team.Players[i_player].GoalSide = goalSide;
                team.Players[i_player].PositionEnum = positions[i_player];
            }
            return team;
        }

        public Team() { }
        public Team(string name, GoalSide side, params Player[] players)
        {
            Name = name;
            TeamGoalSide = side;
            Players = players;
            foreach (Player player in Players)
            {
                player.TeamName = Name;
                player.GoalSide = side;
            }

        }

        public static readonly string DefaultTeam1Name = "RED";
        public static readonly string DefaultTeam2Name = "BLUE";

        public bool IsPlayerOnTeam(Player aPlayer)
        {
            return aPlayer.TeamName.Equals(Name);
        }

        /// <summary>
        /// Private copy constructor
        /// </summary>
        /// <param name="team"></param>
        private Team(Team team)
        {
            Name = team.Name;
            TeamGoalSide = team.TeamGoalSide;
            HasPosession = team.HasPosession;
            Score = team.Score;
            Name = team.Name;
            if (team.Players != null && team.Players.Length > 0)
                Players = team.Players
                                .Select(player => (Player)player.DeepCopy())
                                .ToArray();
            defaultConfiguration = team.DefaultConfiguration;

        }
        public List<Vect3> GetPositions()
        {
            return Players.Select((player) => player.Location).ToList();
        }
        /// <summary>
        /// Gets players velocity vectors
        /// </summary>
        /// <returns>List of player velocities</returns>
        public List<Vect3> GetVelocities()
        {
            return Players.Select((player) => player.Velocity).ToList();
        }

        public List<Player> GetPlayersWithStealViability(Vect3 ballPos)
        {
            return Players
                    .Where(player => player.IsWithinStealingRange(ballPos))
                    .ToList();
        }


        bool isMultithread = false;

        public void MakeAllPlayersPlay(Team otherTeam, Ball ball, ref Ball ballRef)
        {
            if(!isMultithread)
            {
                MakeAllPlayersPlaySingleThreaded(otherTeam, ball, ref ballRef);
                return;
            }
            List<Task<Vect3>> tasks = new List<Task<Vect3>>();
            Team copy = (Team)DeepCopy();
            foreach (Player player in Players)
            {
                Task<Vect3> task = Task.Run(() =>
                {
                    return player.Play(ref copy, this, otherTeam, ball);
                });
                tasks.Add(task);
            }
            while(tasks.Any(t => !t.IsCompleted))
            {

            }
            foreach(Task<Vect3> task in tasks)
            {
                if(task.Result != null)
                {
                    ballRef.Velocity = task.Result.Copy();
                    break;
                }
            }
        }

        private void MakeAllPlayersPlaySingleThreaded(Team otherTeam, Ball ball, ref Ball ballRef)
        {
            Vect3 newBallVel = null;

            foreach (Player player in Players)
            {
                Team copy = this;
                Vect3 ballVel = player.Play(ref copy, this, otherTeam, ball);
                if (ballVel != null)
                {
                    newBallVel = ballVel;
                }
                hasPossession = hasPossession || player.HasPosession;
            }
            if (newBallVel != null)
            {
                ballRef.Velocity = newBallVel;
            }
        }

        public void UpdatePlayersPositions(Walls walls)
        {
            //List<Task<int>> tasks = new List<Task<int>>();

            foreach (Player player in Players) {
                //Task<int> updatePosTask = Task.Run(() =>
                //{
                    player.UpdatePosition(walls);
                //    return 0;
                //});
                //tasks.Add(updatePosTask);
            }
        }

        public void SetPlayerPositionsToDefault(Walls walls)
        {
            if (DefaultConfiguration == null
                || DefaultConfiguration.Count() != Players.Count())
            {
                throw new InvalidOperationException("The amounnt of default configuration vectors must match the amount of" +
                       " players for team " + Name);
            }
            for (int i_config = 0; i_config < DefaultConfiguration.Length; i_config++)
            {
                //Set the players positions relative to the current dimensions of the Walls
                Players[i_config].Location =
                    DefaultConfiguration[i_config].MultiplyByComponentFactors(walls.Width / 2,
                                walls.Height / 2,
                                1.0);
                Players[i_config].Velocity = new Vect3();
                Players[i_config].ResetPlayer();
            }
        }

        public TeamPossessionEnum GetTeamPossessionEnum(Team otherTeam)
        {
            TeamPossessionEnum teamPossess = TeamPossessionEnum.NONE;
            if (HasPosession)
            {
                teamPossess = TeamPossessionEnum.TEAM;
            }
            else if (otherTeam.HasPosession)
            {
                teamPossess = TeamPossessionEnum.OTHER;
            }
            return teamPossess;
        }

        public IEntity DeepCopy()
        {
            return new Team(this);
        }


    }
}
