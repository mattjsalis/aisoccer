﻿using BehaviorTree;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameElements
{
    public abstract class BehaviorTreeAction : IPlay
    {
        protected double[] xWallBounds;
        protected double[] yWallBounds;
        protected BehaviorTree.BehaviorTree behaviors;

        protected double[] locOfBest = null;

        public BehaviorTreeAction(BehaviorTree.BehaviorTree behaviorTree,
            double[] xWallBounds, double[] yWallBounds)
        {
            behaviors = behaviorTree;
            this.xWallBounds = xWallBounds;
            this.yWallBounds = yWallBounds;
        }

        public abstract IPlay DeepCopy();

        public abstract double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player);

        public double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player, PlayTypeEnum playType)
        {

            TeamPossessionEnum teamPossess = ownTeam.GetTeamPossessionEnum(otherTeam);
            Tuple<List<double[]>, double[]> locs = behaviors.ComputeBestAndSuggestedLocs(player.PositionEnum, teamPossess,
                player.HasPosession ? PlayerPosessionEnum.YES : PlayerPosessionEnum.NO,
                playType, xWallBounds, yWallBounds, player);
            

            locOfBest = locs.Item2;
            return behaviors.GetLeafNode(player.PositionEnum, teamPossess,
                player.HasPosession ? PlayerPosessionEnum.YES : PlayerPosessionEnum.NO,
                playType)
                .potentialAt(locs.Item2[0], locs.Item2[1]);
        }

        public abstract void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player);

        public abstract bool isViablePlay(Player player, Ball ball, Team ownTeam, Team otherTeam);
        public abstract IPlay ShallowCopyWithUser(IMovingEntity user);
    }
}
