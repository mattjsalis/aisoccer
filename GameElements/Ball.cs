﻿using System;
using System.Collections.Generic;
using System.Text;
using Utility;
using System.Xml.Serialization;
using SoccerComponents;

namespace GameElements
{
    
    public class Ball : CommonMovingEntity
    {
        private double COEFFICIENT_OF_FRICTION = 0.01;

        public Ball() { }
        public Ball(Ball ball)
            : base(ball)
        {

        }

        /// <summary>
        /// Updates the position of the ball. Reflects ball off of any wall
        /// Invokes event if ball is scored
        /// </summary>
        public override Projection? UpdatePosition(Walls walls)
        {
            //Apply to friction to ball
            Velocity = Velocity.Times(1 - COEFFICIENT_OF_FRICTION);
            Projection proj = walls.GetProjectionType(Location, ProjectedPosition, true);
            if(proj == Projection.BALL_SCORED_IN_LEFT_GOAL 
                || proj == Projection.BALL_SCORED_IN_RIGHT_GOAL)
            {
                return proj;
            }
            Tuple<Vect3,Vect3> posAndVelTup = walls.ResolveWallBounce(proj, Location, ProjectedPosition, Velocity, true);
            Location = posAndVelTup.Item1; Velocity = posAndVelTup.Item2;
            return proj;
        }



        public override IEntity DeepCopy()
        {
            return new Ball(this);
        }


    }
}
