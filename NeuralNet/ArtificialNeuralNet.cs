﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNet
{
    [Serializable]
    /// <summary>
    /// Encapsulates a simple artifical neural net
    /// </summary>
    public class ArtificialNeuralNet
    {

        public NeuralLayer InputLayer;
        public NeuralLayer[] HiddenLayers;
        public NeuralLayer OutputLayer;
        /// <summary>
        /// Void input constructor required for XML serialization
        /// </summary>
        public ArtificialNeuralNet() { 
            
        }
        /// <summary>
        /// Construct a neural net with random weights and biases
        /// </summary>
        /// <param name="numInputs"></param>
        /// <param name="numOutputs"></param>
        /// <param name="hiddenLayerNodeCounts"></param>
        public ArtificialNeuralNet(int numInputs, int numOutputs, int[] hiddenLayerNodeCounts,
                                   double lowerBoundWeight, double upperBoundWeight,
                                   double lowerBoundBias, double upperBoundBias)
        {
            //Initialize the input layer
            InputLayer = new NeuralLayer(numInputs);

            HiddenLayers = new NeuralLayer[hiddenLayerNodeCounts.Length];
            for(int i_layer=0;i_layer< hiddenLayerNodeCounts.Length; i_layer++)
            {
                int nodesInPrevLayer = i_layer == 0 ?
                                                numInputs
                                                :
                                                hiddenLayerNodeCounts[i_layer-1];
                //Assign each hidden layer the specifed number of nodes
                HiddenLayers[i_layer] = new NeuralLayer(hiddenLayerNodeCounts[i_layer]);
                //Assign random weights and biases
                HiddenLayers[i_layer].AssignRandomWeightsAndBiasesToNodes(nodesInPrevLayer,
                         lowerBoundWeight, upperBoundWeight,
                         lowerBoundBias, upperBoundBias);
                //If this is the first hidden layer, subscribe to InputLayer, otherwise subscribe to the previous hidden layer
                HiddenLayers[i_layer].SubscribeToPreviousLayer(
                    i_layer == 0 ? InputLayer : HiddenLayers[i_layer - 1]
                    );
            }

            OutputLayer = new NeuralLayer(numOutputs);
            OutputLayer.AssignRandomWeightsAndBiasesToNodes(hiddenLayerNodeCounts[hiddenLayerNodeCounts.Length-1],
                         lowerBoundWeight, upperBoundWeight,
                         lowerBoundBias, upperBoundBias);
            OutputLayer.SubscribeToPreviousLayer(HiddenLayers[HiddenLayers.Length - 1]);
        }
        /// <summary>
        /// Subscribes the layers to each other. MUST be called after deserialization
        /// </summary>
        /// <returns></returns>
        public ArtificialNeuralNet SubscribeLayers()
        {
            for (int i_layer = 0; i_layer < HiddenLayers.Length; i_layer++)
            {
                //If this is the first hidden layer, subscribe to InputLayer, otherwise subscribe to the previous hidden layer
                HiddenLayers[i_layer].SubscribeToPreviousLayer(
                    i_layer == 0 ? InputLayer : HiddenLayers[i_layer - 1]
                    );
            }
            OutputLayer.SubscribeToPreviousLayer(HiddenLayers[HiddenLayers.Length - 1]);
            return this;
        }

        public double[] CalculateNeuralNet(params double[] inputs)
        {
            if (!HiddenLayers[0].IsSubscribedToPrevLayer)
            {
                SubscribeLayers();
            }
            InputLayer.SeedLayer(inputs);
            InputLayer.CalculateLayer(null);

            return OutputLayer.GetNodeSignals();
        }
    
        public ArtificialNeuralNet SetOutputLayerActivationFunction(ActivationFunctionType activationFunc)
        {
            OutputLayer.SetActivationFunctionForAllNodes(activationFunc);
            return this;
        }
        
    }

    public class UnsubscribedLayersException : Exception
    {
        public UnsubscribedLayersException() { }
        public UnsubscribedLayersException(string message) : base(message) { }
        public UnsubscribedLayersException(string message, Exception inner) : base(message, inner){}
    }
}
