﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NeuralNet
{
    [Serializable]
    /// <summary>
    /// Represents a layer of nodes in a neural net
    /// </summary>
    public class NeuralLayer
    {
        public string Name { get; set; }
        [XmlIgnore]
        public int NumNodes { get { return Nodes.Length; } }
        [XmlIgnore]
        public bool IsSubscribedToPrevLayer { get; private set; }
        /// <summary>
        /// Event to invoke on neural nodes of next layer
        /// </summary>
        public event EventHandler<double[]> NodesCalculatedEvent;

        public NeuralNode[] Nodes;
        /// <summary>
        /// Void input constructor required for XML serialization
        /// </summary>
        public NeuralLayer() { }

        public NeuralLayer(int numNodes)
        {
            Nodes = new NeuralNode[numNodes];
            for(int i_node = 0; i_node < numNodes; i_node++) { Nodes[i_node] = new NeuralNode(); }
        }

        public void CalculateLayer(double[] previousLayerSignals)
        {
            double[] signals = CalculateNodeSignals(previousLayerSignals);
            NodesCalculatedEvent?.Invoke(this, signals);
        }

        private void NeuralLayer_NodesCalculatedEvent(object sender, double[] previousLayerSignals)
        {
            CalculateLayer(previousLayerSignals);
        }

        public void SubscribeToPreviousLayer(NeuralLayer prevLayer)
        {
            IsSubscribedToPrevLayer = true;
            prevLayer.NodesCalculatedEvent += NeuralLayer_NodesCalculatedEvent;
        }
        /// <summary>
        /// Assign random weights and biases to nodes in this layer
        /// </summary>
        /// <param name="numNodesInPrevLayer"></param>
        /// <param name="lowerBoundWeight"></param>
        /// <param name="upperBoundWeight"></param>
        /// <param name="lowerBoundBias"></param>
        /// <param name="upperBoundBias"></param>
        public void AssignRandomWeightsAndBiasesToNodes(int numNodesInPrevLayer,
                                        double lowerBoundWeight, double upperBoundWeight,
                                        double lowerBoundBias, double upperBoundBias)
        {
            foreach(NeuralNode node in Nodes)
            {
                node.AssignRandomWeightsAndBias(numNodesInPrevLayer,
                    lowerBoundWeight, upperBoundWeight,
                    lowerBoundBias, upperBoundBias);
            }
        }

        public double[] CalculateNodeSignals(double[] prevLayerSignals)
        {
            return Nodes.Select(node => node.CalculateSignal(prevLayerSignals)).ToArray();
        }
        
        /// <summary>
        /// Seeds the layer with signals. Primary use is the input layer of the
        /// Neural net
        /// </summary>
        /// <param name="signals"></param>
        public void SeedLayer(double[] signals)
        {
            if(signals.Length != Nodes.Length)
            {
                throw new ArgumentException("The number of input signals must match the number of " +
                    "nodes in the input layer.");
            }
            for(int i_sig = 0; i_sig < Nodes.Length; i_sig++)
            {
                Nodes[i_sig].SeedSignal(signals[i_sig]);
            }
        }

        public double[] GetNodeSignals()
        {
            return Nodes.Select(node => node.Signal).ToArray();
        }
    
        internal void SetActivationFunctionForAllNodes(ActivationFunctionType funcType)
        {
            foreach(NeuralNode node in Nodes)
            {
                node.ActivationFunction = funcType;
            }
        }
    
    }
}
