﻿using System;
using Utility;
using System.Xml.Serialization;

namespace NeuralNet
{
    [Serializable]
    public class NeuralNode
    {
        [XmlIgnore]
        public double Signal { get; private set; }

        public ActivationFunctionType ActivationFunction = ActivationFunctionType.TANH;
        [XmlIgnore]
        private Func<double, double> activationFunc;

        public double[] Weights;
        public double Bias;

        public NeuralNode() { }

        /// <summary>
        /// Calculates the signal of this neuron based on signals
        /// from previous layer and weights/biases
        /// </summary>
        /// <param name="signals"></param>
        public double CalculateSignal(double[] signals)
        {
            if (signals == null) { return Signal;}
            //Set the activation function if necessary
            if(activationFunc == null)
            {
                activationFunc = ActivationFunctionFactory.GetActivationFunction(ActivationFunction);
            }
            double totSig = 0.0;
            for (int i_sig = 0; i_sig < signals.Length; i_sig++)
            {
                totSig += signals[i_sig] * Weights[i_sig];
            }
            Signal = activationFunc.Invoke(totSig + Bias);
            return Signal;
        }
        /// <summary>
        /// Assign random values to the weights and biases of this neuron
        /// </summary>
        /// <param name="numNodesInPrevLayer"></param>
        /// <param name="lowerBoundWeight"></param>
        /// <param name="upperBoundWeight"></param>
        /// <param name="lowerBoundBias"></param>
        /// <param name="upperBoundBias"></param>
        public void AssignRandomWeightsAndBias(int numNodesInPrevLayer,
                                        double lowerBoundWeight, double upperBoundWeight,
                                        double lowerBoundBias, double upperBoundBias)
        {
            Weights = new double[numNodesInPrevLayer];
            for(int i_weight = 0; i_weight < numNodesInPrevLayer; i_weight++)
            {
                Weights[i_weight] = Probability.RandomBetween(lowerBoundWeight, upperBoundWeight);
            }
            Bias = Probability.RandomBetween(lowerBoundBias, upperBoundBias);
        }

        public void SeedSignal(double signal)
        {
            this.Signal = signal;
        }
    }
}
