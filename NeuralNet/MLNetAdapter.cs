﻿using Keras.Callbacks;
using Keras.Layers;
using Keras.Models;
using Keras.Optimizers;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms;
using Numpy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace NeuralNet
{
    public class MLNetAdapter
    {
        private static Dictionary<string, SpatialAwarenessInput> getInputTypeDict()
        {
            Dictionary<string, SpatialAwarenessInput> d = new Dictionary<string, SpatialAwarenessInput>();
            d.Add("Shoot", new ShotPlayInput());
            d.Add("PassBall", new PassPlayInput());
            return d;
        }

        public MLNetAdapter(Dictionary<string, List<List<double>>> data)
        {
            createAndTrainNeuralNet(data);
            Console.WriteLine();

        }

        public static void createAndTrainNeuralNet(Dictionary<string, List<List<double>>> data)
        {
            foreach(string key in data.Keys)
            {
                int inputSize = data[key][0].Count - 1;
                Pair<float[,], int[]> insAndOuts = getInputsAndOutputs(data[key]);
                NDarray inputs = np.array(insAndOuts.First);
                NDarray outputs = np.array(insAndOuts.Second);
                outputs = Keras.Utils.Util.ToCategorical(outputs);
                Input inputLayer = new Input(shape: new Keras.Shape(inputSize));
                BaseLayer hiddenLayer1 = new Dense(50, activation: "relu").Set(inputLayer);
                BaseLayer hiddenLayer2 = new Dense(50, activation: "relu").Set(hiddenLayer1);
                BaseLayer hiddenLayer3 = new Dense(50, activation: "relu").Set(hiddenLayer2);
                BaseLayer output = new Dense(3, activation: "softmax").Set(hiddenLayer3);
                Model neuralNet = new Model(new Input[] { inputLayer }, new BaseLayer[] { output });
                neuralNet.Compile(new Adam(), loss: "categorical_crossentropy", metrics: new string[] { "accuracy" });
                int numEpoch = 50;
                History history = neuralNet.Fit(inputs, outputs, 500, numEpoch, verbose : 0);
                double finalAccuracy = history.HistoryLogs["accuracy"][numEpoch - 1];
                string forPrint = string.Format("{0} : accuracy {1}", key, finalAccuracy);
                Console.WriteLine(forPrint);
            }
            
        }

        public static Pair<float[,], int[]> getInputsAndOutputs(List<List<double>> values)
        {
            int lastInd = values[0].Count - 1;
            int[] outputs = new int[values.Count];
            float[,] inputs = new float[values.Count, values[0].Count - 1];
            for(int i = 0; i < values.Count; i++)
            {
                outputs[i] = (int)values[i][lastInd];
                for (int j = 0; j < lastInd; j++)
                {
                    inputs[i,j] = (float)values[i][j];
                }
            }
            return new Pair<float[,], int[]>(inputs, outputs);
        }

         

    }

    public class ListDataWrapper
    {
        public float[] Features { get; private set; }
        [ColumnName("Label")]
        public float Reward = 0.0f;

        public void assignFromList(List<double> doubles)
        {
            Features = doubles.GetRange(0, doubles.Count - 1).Select(d => (float)d).ToArray();
            Reward = (float)doubles[doubles.Count - 1];
        }

    }


    public class SpatialAwarenessInput
    {
        double OwnPlayer1Dist = 0.0;
        double OwnPlayer1Az = 0.0;
        double OwnPlayer2Dist = 0.0;
        double OwnPlayer2Az = 0.0;
        double OwnPlayer3Dist = 0.0;
        double OwnPlayer3Az = 0.0;
        double OwnPlayer4Dist = 0.0;
        double OwnPlayer4Az = 0.0;
        double OwnPlayer5Dist = 0.0;
        double OwnPlayer5Az = 0.0;
        double OwnPlayer6Dist = 0.0;
        double OwnPlayer6Az = 0.0;
        double OwnPlayer7Dist = 0.0;
        double OwnPlayer71Az = 0.0;
        double OwnPlayer8Dist = 0.0;
        double OwnPlayer8Az = 0.0;
        double OwnPlayer9Dist = 0.0;
        double OwnPlayer9Az = 0.0;
        double OtherPlayer1Dist = 0.0;
        double OtherPlayer1Az = 0.0;
        double OtherPlayer2Dist = 0.0;
        double OtherPlayer2Az = 0.0;
        double OtherPlayer3Dist = 0.0;
        double OtherPlayer3Az = 0.0;
        double OtherPlayer4Dist = 0.0;
        double OtherPlayer4Az = 0.0;
        double OtherPlayer5Dist = 0.0;
        double OtherPlayer5Az = 0.0;
        double OtherPlayer6Dist = 0.0;
        double OtherPlayer6Az = 0.0;
        double OtherPlayer7Dist = 0.0;
        double OtherPlayer71Az = 0.0;
        double OtherPlayer8Dist = 0.0;
        double OtherPlayer8Az = 0.0;
        double OtherPlayer9Dist = 0.0;
        double OtherPlayer9Az = 0.0;
        double Reward = 0.0;

        public virtual void assignFromList(List<double> inputs)
        {
             OwnPlayer1Dist = inputs[0];
             OwnPlayer1Az = inputs[1];
            OwnPlayer2Dist = inputs[2];
            OwnPlayer2Az = inputs[3];
            OwnPlayer3Dist = inputs[4];
            OwnPlayer3Az = inputs[5];
            OwnPlayer4Dist = inputs[6];
            OwnPlayer4Az = inputs[7];
            OwnPlayer5Dist  = inputs[8];
             OwnPlayer5Az  = inputs[9];
             OwnPlayer6Dist  = inputs[10];
             OwnPlayer6Az  = inputs[11];
             OwnPlayer7Dist  = inputs[12];
             OwnPlayer71Az  = inputs[13];
             OwnPlayer8Dist  = inputs[14];
             OwnPlayer8Az  = inputs[15];
             OwnPlayer9Dist  = inputs[16];
             OwnPlayer9Az  = inputs[17];
             OtherPlayer1Dist  = inputs[18];
             OtherPlayer1Az  = inputs[19];
             OtherPlayer2Dist  = inputs[20];
             OtherPlayer2Az  = inputs[21];
             OtherPlayer3Dist  = inputs[22];
             OtherPlayer3Az  = inputs[23];
             OtherPlayer4Dist  = inputs[24];
             OtherPlayer4Az  = inputs[25];
             OtherPlayer5Dist  = inputs[26];
             OtherPlayer5Az  = inputs[27];
             OtherPlayer6Dist  = inputs[28];
             OtherPlayer6Az  = inputs[29];
             OtherPlayer7Dist  = inputs[30];
             OtherPlayer71Az  = inputs[31];
             OtherPlayer8Dist  = inputs[32];
             OtherPlayer8Az  = inputs[33];
             OtherPlayer9Dist  = inputs[34];
             OtherPlayer9Az  = inputs[35];
             
             Reward  = inputs[inputs.Count - 1];
        }
    
        public virtual SpatialAwarenessInput copy()
        {
            return new SpatialAwarenessInput();
        }
    }

    public class PassPlayInput : SpatialAwarenessInput
    {
        double PassVectX = 0.0;
        double PassVectY = 0.0;
        double PassVectZ = 0.0;
        double PassSpeed = 0.0;

        public override void assignFromList(List<double> inputs)
        {
            base.assignFromList(inputs);
            PassSpeed = inputs[inputs.Count - 1 - 4];
            PassVectX = inputs[inputs.Count - 1 - 3];
            PassVectY = inputs[inputs.Count - 1 - 2];
            PassVectZ = inputs[inputs.Count - 1 - 1];
        }

        public override SpatialAwarenessInput copy()
        {
            return new PassPlayInput();
        }
    }

    public class ShotPlayInput : SpatialAwarenessInput
    {
        double DistanceToGoal = 0.0;

        public override void assignFromList(List<double> inputs)
        {
            base.assignFromList(inputs);
            DistanceToGoal = inputs[inputs.Count - 1 - 1];
        }

        public override SpatialAwarenessInput copy()
        {
            return new ShotPlayInput();
        }
    }

}
