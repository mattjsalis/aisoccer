﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;

namespace NeuralNet
{
    /// <summary>
    /// Enum for the type of activation function
    /// </summary>
    public enum ActivationFunctionType
    {
        /// <summary>
        /// activation = signal
        /// </summary>
        LINEAR,
        /// <summary>
        /// activation = signal if greater than zero, else 0
        /// </summary>
        RELU,
        /// <summary>
        /// activation = 2/(1 + exp(-2*signal)) - 1
        /// </summary>
        TANH,
        /// <summary>
        /// activation = 1/(1+exp(-signal)
        /// </summary>
        SIGMOID
    }
    /// <summary>
    /// Class to generate a Func delegate given a activation function type
    /// </summary>
    public class ActivationFunctionFactory
    {
        public static Func<double,double> GetActivationFunction(ActivationFunctionType actType)
        {
            switch (actType)
            {
                case ActivationFunctionType.LINEAR:
                    return (signal) => signal;
                case ActivationFunctionType.RELU:
                    return signal => signal < 0.0 ? 0.0 : signal;
                case ActivationFunctionType.TANH:
                    return signal => 2/(1 + Exp(-2*signal)) - 1;
                case ActivationFunctionType.SIGMOID:
                    return signal => 1 / (1 + Exp(-signal));
            }

            //Default return is linear
            return (aDub) => aDub;
        }
    }
}
