﻿
using System;
using Utility;


namespace SoccerComponents
{
    public enum Projection
    {
        BOUNCE_OFF_RIGHT_WALL,
        BOUNCE_OFF_LEFT_WALL,
        BOUNCE_OFF_TOP_WALL,
        BOUNCE_OFF_BOTTOM_WALL,
        ON_PITCH,
        BALL_SCORED_IN_RIGHT_GOAL,
        BALL_SCORED_IN_LEFT_GOAL
    }

    public class Walls
    {
        //Wall boundaries
        public double LeftWall { get; private set; } = -62.5;
        public double RightWall { get; private set; }  = 62.5;
        public double TopWall { get; private set; }  = 42.5;
        public double BottomWall { get; private set; }  = -42.5;

        //Goal boundaries
        public double TopPost { get; private set; } = 5;
        public double BottomPost { get; private set; } = -5;
        
        public double Width { get { return RightWall - LeftWall; } }
        public double Height { get { return TopWall - BottomWall; } }

        public void SetWallDimensions(double width, double height, double goalWidth)
        {
            LeftWall = - width / 2;
            RightWall = width / 2;
            TopWall = height / 2;
            BottomWall = -height / 2;
            TopPost = goalWidth / 2;
            BottomPost = -goalWidth / 2;
        }

        public double[] GetXBounds()
        {
            return new double[]
            {
                LeftWall, RightWall
            };
        }

        public double[] GetYBounds()
        {
            return new double[]
            {
                BottomWall, TopWall
            };
        }

        /// <summary>
        /// Check to see if a position vector violates the wall boundaries
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public Projection GetProjectionType(Vect3 startingPosition, Vect3 projectedPosition, bool isBall)
        {
            if (projectedPosition.X < LeftWall)
            {
                if(isGoal(startingPosition, projectedPosition, isBall))
                {
                    return Projection.BALL_SCORED_IN_LEFT_GOAL;
                }
                return Projection.BOUNCE_OFF_LEFT_WALL;
            } else if (projectedPosition.X > RightWall)
            {
                if(isGoal(startingPosition, projectedPosition, isBall))
                {
                    return Projection.BALL_SCORED_IN_RIGHT_GOAL;
                }
                return Projection.BOUNCE_OFF_RIGHT_WALL;
            }

            if (projectedPosition.Y < BottomWall)
            {
                return Projection.BOUNCE_OFF_BOTTOM_WALL;
            } else if(projectedPosition.Y > TopWall)
            {
                return Projection.BOUNCE_OFF_TOP_WALL;
            }
            return Projection.ON_PITCH;
        }
        /// <summary>
        /// Checks if the y position is within the bounds of goal posts.
        /// Only accurate if the x component of the position projection 
        /// has penetrated the left or right wall.
        /// </summary>
        /// <param name="yPos"></param>
        /// <returns></returns>
        private bool isGoal(Vect3 startingPosition, Vect3 projectedPosition, bool isBall)
        {
            if (!isBall) { return false; }
            double yPos = double.MaxValue;
            if (projectedPosition.X < LeftWall)
            {
                yPos = startingPosition.Y 
                    + (projectedPosition.Y - startingPosition.Y) / (projectedPosition.X - startingPosition.X) 
                    * (LeftWall - startingPosition.X);
            } 
            else if(projectedPosition.X > RightWall)
            {
                yPos = startingPosition.Y 
                    + (projectedPosition.Y - startingPosition.Y) / (projectedPosition.X - startingPosition.X) 
                    * (RightWall - startingPosition.X);
            }

            return yPos > BottomPost && yPos < TopPost;
        }
        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="projectedPosition"></param>
        /// <returns></returns>
        public Tuple<Vect3,Vect3> GetBouncePosition(Projection projection, Vect3 projectedPosition, Vect3 projectedVelocity)
        {
            switch (projection)
            {
                case Projection.BOUNCE_OFF_BOTTOM_WALL:
                    return Tuple.Create(
                        new Vect3(projectedPosition.X, 2 * BottomWall - projectedPosition.Y, projectedPosition.Z),
                        projectedVelocity.MultiplyByComponentFactors(1, -1, 1)
                        );
                case Projection.BOUNCE_OFF_TOP_WALL:
                    return Tuple.Create(
                        new Vect3(projectedPosition.X, 2 * TopWall - projectedPosition.Y, projectedPosition.Z),
                        projectedVelocity.MultiplyByComponentFactors(1, -1, 1)
                        ); ;
                case Projection.BOUNCE_OFF_LEFT_WALL:
                    return Tuple.Create(
                        new Vect3(2 * LeftWall - projectedPosition.X, projectedPosition.Y, projectedPosition.Z),
                        projectedVelocity.MultiplyByComponentFactors(-1, 1, 1)
                        );
                case Projection.BOUNCE_OFF_RIGHT_WALL:
                    return Tuple.Create(
                        new Vect3(2 * RightWall - projectedPosition.X, projectedPosition.Y, projectedPosition.Z),
                        projectedVelocity.MultiplyByComponentFactors(-1, 1, 1)
                         );
                default:
                    return Tuple.Create(projectedPosition, projectedVelocity);
            }

        }


        /// <summary>
        /// Resolves the bounce of the component until it is either on the 
        /// pitch or a goal has been scored (in the case that the component
        /// in question is the ball).
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public  Tuple<Vect3,Vect3 > ResolveWallBounce(Vect3 startingPosition, 
            Vect3 projectedPosition, 
            Vect3 projectedVelocity,  
            bool isBall)
        {
            Projection projection = GetProjectionType(startingPosition, projectedPosition, isBall);
            return ResolveWallBounce(projection, startingPosition,  projectedPosition, projectedVelocity, isBall);
        }

        public Tuple<Vect3, Vect3> ResolveWallBounce(Projection projection, 
            Vect3 startPosition,
            Vect3 projectedPosition,
            Vect3 projectedVelocity,
            bool isBall)
        {
            Tuple<Vect3, Vect3> posVelTup = Tuple.Create(projectedPosition, projectedVelocity);
            while (!projection.Equals(Projection.ON_PITCH) 
)
            {
                projection = GetProjectionType(startPosition, projectedPosition, isBall);
                posVelTup = GetBouncePosition(projection, projectedPosition, projectedVelocity);
                if(!projection.Equals(Projection.BALL_SCORED_IN_LEFT_GOAL) || !projection.Equals(Projection.BALL_SCORED_IN_RIGHT_GOAL))
                {

                    break;
                }
            }
            return posVelTup;
        }


    }
}
