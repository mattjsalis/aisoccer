﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoccerComponents
{
    /// <summary>
    /// Interface for a general entity
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Method that returns a deep copy of the object
        /// </summary>
        /// <returns></returns>
        IEntity DeepCopy();
    }
}
