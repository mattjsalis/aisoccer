﻿using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace SoccerComponents
{

    public interface IMovingEntity : IEntity
    {
        Vect3 Location
        {
            get;
            set;
        }

        Vect3 Velocity
        {
            get;
            set;
        }

        /// <summary>
        /// Update position as a function of timestep, velocity and position
        /// </summary>
        Projection? UpdatePosition(Walls walls);
    }
}
