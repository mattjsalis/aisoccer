﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Optimization
{
    public class ParticleSwarm
    {
        double socialConstant;
        double ownConstant;
        double convergenceLimitFract;
        int numExploratoryGenerations;

        Random rand;
        Particle[] particles;

        List<double[]> bounds;

        IOptimizationSolution swarmBest;
        int randSeed;

        public ParticleSwarm(int numParticles, List<double[]> bounds, int randomSeed=123456, 
            double socialConstant=2.0, double ownConstant=2.0, double convergenceLimitFract=0.1,
            int numExploratoryGenerations=0)
        {
            rand = new Random(randomSeed);
            randSeed = randomSeed;
            this.socialConstant = socialConstant;
            this.ownConstant = ownConstant;
            this.bounds = bounds;
            this.convergenceLimitFract = convergenceLimitFract;
            this.numExploratoryGenerations = numExploratoryGenerations;
            initializeSwarm(false, numParticles);
        }

        public Particle GetParticle(int index)
        {
            return particles[index];
        }

        public IOptimizationSolution runOptimization(bool isMultiThread, int numGenerations, int numSameForRestart, ICostFunction costFunction)
        {
            int countSinceUpdated = 0;
            for(int i = 0; i < numGenerations; i++)
            {
                bool swarmBestUpdated = false;
                if(isMultiThread)
                {
                    swarmBestUpdated = runGenerationMultiThreaded(costFunction);
                }
                else
                {
                    swarmBestUpdated = runGenerationSingleThreaded(costFunction);
                }
                if(swarmBest != null)
                {
                    Console.WriteLine("Gen " + (i + 1) + ": " + swarmBest);
                }
                if(!swarmBestUpdated)
                {
                    countSinceUpdated++;
                }
                if(countSinceUpdated > numSameForRestart)
                {
                    initializeSwarm(true, particles.Length);
                    countSinceUpdated = 0;
                }
                if(null != swarmBest && swarmBest.IsSolution())
                {
                    break;
                }
            }

            return swarmBest;
        }

        public bool runGenerationMultiThreaded(ICostFunction costFunction)
        {
            bool isUpdatedSwarmBest = false;
            SortedSet<IOptimizationSolution> sols = new SortedSet<IOptimizationSolution>();
            List<Task<IOptimizationSolution>> tasks = new List<Task<IOptimizationSolution>>();
            for (int i = 0; i < particles.Length; i++)
            {
                Particle particle = particles[i];
                Task<IOptimizationSolution> paricleTask;
                if (i < this.numExploratoryGenerations)
                {
                    paricleTask = Task.Run(() => runWithRandomizedParams(costFunction, ref particle));
                }
                else
                {
                    paricleTask = Task.Run(() => runForParticleAndUpdateParms(costFunction, ref particle));
                }
                tasks.Add(paricleTask);
            }
            Task.WaitAll(tasks.ToArray());
            foreach(var task in tasks)
            {
                sols.Add(task.Result);
                Console.WriteLine(task.Result);
            }
            IOptimizationSolution bestInGen = sols.Last();
            
            if (null == swarmBest || bestInGen.CompareTo(swarmBest) > 0)
            {
                swarmBest = bestInGen.deepCopy();
                isUpdatedSwarmBest = true;
            }

            foreach(var task in tasks)
            {
                task.Dispose();
            }
            Particle fused = costFunction.fuseParticleSolutions(particles);
            SortedSet<Particle> sortedParticles = new SortedSet<Particle>();
            foreach (Particle p in particles)
            {
                sortedParticles.Add(p);
            }
            Particle worst = sortedParticles.First();
            worst.parameters.Clear();
            worst.parameters.AddRange(fused.parameters);

            return isUpdatedSwarmBest;
        }

        public bool runGenerationSingleThreaded(ICostFunction costFunction)
        {
            bool isUpdatedSwarmBest = false;
            SortedSet<IOptimizationSolution> sols = new SortedSet<IOptimizationSolution> ();
            for(int i=0; i<particles.Length;i++)
            {
                Particle particle = particles[i];
                IOptimizationSolution paricleSol;
                if (i < this.numExploratoryGenerations)
                {
                    paricleSol = runWithRandomizedParams(costFunction, ref particle);
                }
                else
                {
                    paricleSol = runForParticleAndUpdateParms(costFunction, ref particle);
                }
                Particle fused = costFunction.fuseParticleSolutions(particles);
                SortedSet<Particle> sortedParticles = new SortedSet<Particle>(particles);
                Particle worst = sortedParticles.First();
                worst.parameters.Clear();
                worst.parameters.AddRange(fused.parameters);

                Console.WriteLine("\t " + paricleSol);
                sols.Add(paricleSol);
            }
            IOptimizationSolution bestInGen = sols.Last();
            if(null == swarmBest || bestInGen.CompareTo(swarmBest) > 0)
            {
                swarmBest = bestInGen.deepCopy();
                isUpdatedSwarmBest = true;
            }
            return isUpdatedSwarmBest;
        }

        public IOptimizationSolution runForParticleAndUpdateParms(ICostFunction costFunction, ref Particle particle)
        {
            //Update particle params
            particle.updateParams(swarmBest, socialConstant, ownConstant);
            IOptimizationSolution sol = costFunction.computeCostFunction(particle.parameters);
            particle.updateBest(sol);
            return sol;
        }

        public IOptimizationSolution runWithRandomizedParams(ICostFunction costFunction, ref Particle particle)
        {
            particle.randomizeFromBounds(true, rand);
            IOptimizationSolution sol = costFunction.computeCostFunction(particle.parameters);
            particle.updateBest(sol);
            return sol;
        }


        public void initializeSwarm(bool isRememberSwarmBest, int numParticles)
        {
            particles = new Particle[numParticles];
            for (int i = 0; i < numParticles; i++)
            {
                particles[i] = new Particle(bounds, convergenceLimitFract, randSeed);
                particles[i].randomizeFromBounds(false, rand);
            }
            if(!isRememberSwarmBest)
            {
                swarmBest = null;
            }
        }
    }

    public class Particle : IComparable
    {
        public List<double[]> parameterBounds;
        public IOptimizationSolution bestFoundSoFar;

        public List<double> parameters;
        List<double> velocities;
        List<double> velocityLimits;
        Random rand;

        public Particle(List<double[]> parameterBounds, double convergenceLimitFract, int randSeed)
        {
            this.parameterBounds = parameterBounds;
            velocities= new List<double>();
            parameters = new List<double>();
            for(int i = 0; i < parameterBounds.Count;i++)
            {
                velocities.Add(0.0);
                parameters.Add(0.0);
            }
            velocityLimits = parameterBounds.Select(b => (b[1] - b[0]) * convergenceLimitFract).ToList();
            this.rand = new Random(randSeed);
        }

        public Particle(Particle p)
        {
            this.parameters = new List<double>(p.parameters);
            this.parameterBounds = new List<double[]>(p.parameterBounds);
            this.velocityLimits = new List<double>(p.velocityLimits);
            this.parameters = new List<double>(p.parameters);
            this.rand = p.rand;
        }

        public void SeedWithParams(List<double> parameters)
        {
            this.parameters = parameters;
        }

        public void CopyParamsFrom(List<int> paramIndicesToCopy, Particle other)
        {
            foreach(int i in paramIndicesToCopy)
            {
                parameters[i] = other.parameters[i];
            }
        }

        public void RandomizeAllExceptSelectIndices(List<int> indicesNotToRandomize)
        {
            for(int i=0;i<parameters.Count;i++)
            {
                if(indicesNotToRandomize.Contains(i))
                {
                    continue;
                }
                parameters[i] = Probability.RandomBetween(parameterBounds[i][0], parameterBounds[i][1], rand);
            }
        }

        public void randomizeFromBounds(bool isRememberBest, Random rand)
        {
            if(!isRememberBest)
            {
                bestFoundSoFar = null;
            }
            for(int i = 0; i < parameterBounds.Count; i++)
            {
                
                velocities[i] = 0;
                parameters[i] = Probability.RandomBetween(parameterBounds[i][0], parameterBounds[i][1], rand);
            }
        }

        protected virtual double updateParam(double constant, double diffBetweenParams)
        {
            return rand.NextDouble() * constant * diffBetweenParams;
        }

        public void updateParams(IOptimizationSolution swarmBest, double socialConst, double ownConst)
        {
            List<double> paramsForSol = null;
            List<double> paramsForOwnSol = null;
            if ( null != swarmBest)
                paramsForSol = swarmBest.GetParametersForSolution();
            if( null != bestFoundSoFar)
                paramsForOwnSol = bestFoundSoFar.GetParametersForSolution();
            bool allParametersStagnant = true;
            for (int i = 0;i<parameters.Count;i++)
            {
                double currVel = velocities[i];
                double socialComponent = 0.0, ownComponent = 0.0;
                if( null != paramsForSol)
                    socialComponent = updateParam(socialConst ,paramsForSol[i] - parameters[i]);
                if(null != paramsForOwnSol)
                    ownComponent = updateParam(ownConst , paramsForOwnSol[i] - parameters[i]);
                double momentum = currVel * rand.NextDouble();
                double rawVel =  socialComponent + ownComponent;
                double absVel = Math.Abs(rawVel);
                if((paramsForSol != null && paramsForOwnSol != null)
                    && allParametersStagnant
                    && absVel > 1e-4 * velocityLimits[i])
                {
                    allParametersStagnant = false;
                }
                double newVelMag = Math.Min(absVel, velocityLimits[i]);

                velocities[i] = Math.Sign(rawVel) * newVelMag;
                parameters[i] += velocities[i] + momentum;
                if (parameters[i] < parameterBounds[i][0])
                {
                    parameters[i] = parameterBounds[i][0];
                }
                else if(parameters[i] > parameterBounds[i][1])
                {
                    parameters[i] = parameterBounds[i][1];
                }
            }
        }

        public void updateBest(IOptimizationSolution solution)
        {
            if(bestFoundSoFar == null || solution.CompareTo(bestFoundSoFar) > 0)
            {
                bestFoundSoFar = solution.deepCopy();
            }
        }

        public int CompareTo(object obj)
        {
            Particle cast = (Particle)obj;
            if (Equals(cast))
            {
                return 0;
            }
            bool isBestFoundNull = null == this.bestFoundSoFar;
            bool isCastBestFoundNull = null == cast.bestFoundSoFar;
            if(isBestFoundNull && isCastBestFoundNull)
            {
                return GetHashCode().CompareTo(cast.GetHashCode());
            }
            else if(isBestFoundNull)
            {
                return -1;
            }
            else if(isCastBestFoundNull)
            {
                return 1;
            }
            return bestFoundSoFar.CompareTo(cast.bestFoundSoFar);
        }
    }

    public class GravityParticle
    {

    }


    public interface IOptimizationSolution : IComparable
    {
        List<double> GetParametersForSolution();

        bool isGoodEnoughToSearchAround();

        bool IsSolution();

        IOptimizationSolution deepCopy();

    }

    public interface ICostFunction
    {
        IOptimizationSolution computeCostFunction(List<double> parameters);

        Particle fuseParticleSolutions(Particle[] particles);
    }
}
