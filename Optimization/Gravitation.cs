﻿using ActionPotential;
using System;
using System.Collections.Generic;
using System.Linq;
using Utility;

namespace Optimization
{
    public class Gravitation
    {
        public double gravConst = 0.001;
        public ActionPotential.ActionPotential actionPotential;

        public Gravitation(
            ActionPotential.ActionPotential actionPotential)
        {
            this.actionPotential = actionPotential;
        }

        public List<List<double[]>> optimize(int iter, int paramsToStart,
            double[] xBounds, double[] yBounds, double epsilon)
        {
            List<List<double[]>> allLocs = new List<List<double[]>>();
            List < double[]> initialLocs = new List <double[] >();
            for (int i=0;i < iter;i++)
            {
                double x = Probability.RandomBetween(xBounds[0], xBounds[1]);
                double y = Probability.RandomBetween(yBounds[0], yBounds[1]);
                initialLocs.Add(new double[] { x, y });
            }
            allLocs.Add(initialLocs);

            int j = 0;
            while(j < iter)
            {
                List<double[]> lastLocs = allLocs[allLocs.Count - 1];
                int remainingParticles = lastLocs.Count;
                List<double> masses = lastLocs
                        .Select(coords => actionPotential.potentialAt(coords[0], coords[1]))
                        .ToList();
                double[,] forces = new double[remainingParticles,remainingParticles];
                List<double[]> vectorDirections = new List<double[]>();
                for (int ii=0; ii < remainingParticles; ii++)
                {
                    double[] p1Loc = lastLocs[ii];
                    for (int jj = ii; jj < remainingParticles; jj++)
                    {
                        double[] p2Loc = lastLocs[ii];

                        double manhattanDist = Math.Abs(p2Loc[0] - p1Loc[0]) + Math.Abs(p2Loc[1] - p1Loc[1]);
                        forces[ii, jj] = gravConst * masses[ii] * masses[jj] / (manhattanDist * manhattanDist);
                    }
                }

            }

            return null;
        }



    }
}
