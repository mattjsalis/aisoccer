﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using ActionPotential;
using AISoccer.UIElements;
using BehaviorTree;
using GameElements;
using Newtonsoft.Json;
using Optimization;
using SoccerComponents;
using Utility;

namespace AISoccer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AISoccerMain : Window
    {
        Queue<SoccerMatch> matches = new Queue<SoccerMatch>();
        UISoccerPitch uiSoccerPitch = null;
        bool isVisualize = true;
        bool isRunRealTime = true;
        bool runOptimizer = true;
        bool  isMultiThreadOptimization = true;
        public static readonly double MULTIPLE_OF_REALTIME = 2.0;

        readonly int numParticlesInSwarm = 32;
        readonly int numOptimizationRounds = 100;
        readonly int numGenerationsPso = 100;
        readonly int numGenerationsRestart = 25;

        static readonly double TIME_LIMIT = 60.0;
        static readonly double TIME_TO_CONSIDER_ACTION = 0.4;
       
        public static readonly string PARAM_DIR = "C:\\Users\\mattj\\source\\repos\\aisoccer\\learnedParameters";
        string solDir = Directory.GetParent(Directory.GetCurrentDirectory())
                .Parent.Parent.Parent.Parent.ToString();

        Task visTask = null;
        CancellationTokenSource tokenSource2;

        List<Task<Tuple<Scorer, Scorer>>> runningGames = new List<Task<Tuple<Scorer, Scorer>>>();

        public AISoccerMain()
        {
            InitializeComponent();
        }

        public void reset(GoalSide teamThatIsLearning)
        {
            GoalSide teamNotLearning = teamThatIsLearning == GoalSide.LEFT ? GoalSide.RIGHT : GoalSide.LEFT;
            Walls walls = new Walls();
            if (null != uiSoccerPitch)
            {
                uiSoccerPitch.RemovePlayersFromPitch();
            }
            else
            { 
                uiSoccerPitch = new UISoccerPitch(ref this.PitchCanvas);
            }
            walls.SetWallDimensions(uiSoccerPitch.SoccerPitch.Width, uiSoccerPitch.SoccerPitch.Height, uiSoccerPitch.GOAL_HEIGHT);
            Team team1 = Team.InitializeDefaultTeam(GoalSide.LEFT);
            Team team2 = Team.InitializeDefaultTeam(GoalSide.RIGHT);
            Ball ball = new Ball();


            IMovingEntity ownGoal = new Ball();
            ownGoal.Location = new Vect3(walls.LeftWall, 0.0, 0.0);
            IMovingEntity otherGoal = new Ball();
            otherGoal.Location = new Vect3(walls.RightWall, 0.0, 0.0);

            ISamplingStrategy sampling = new BoxSampling(30);
            sampling.SetBounds(new double[] {  50.0, 50.0, 50.0, 50.0, 25.0, 25.0, 25.0, 25.0 });

            IMovingEntity ballMovingEntity = ball;

            BehaviorTree.BehaviorTree behaviorTree1
                = BehaviorTree.BehaviorTree.GetRandomlyGenerated(ref ballMovingEntity, team2.Players,
                team1.Players, ref ownGoal, ref otherGoal, sampling);

            BehaviorTree.BehaviorTree behaviorTree2
                = BehaviorTree.BehaviorTree.GetRandomlyGenerated(ref ballMovingEntity, team1.Players,
                team2.Players, ref otherGoal, ref ownGoal, sampling);

            if (runOptimizer)
            {
                int sizeBoundsList = behaviorTree1.counter.Count;
                List<double[]> bounds = new List<double[]>(sizeBoundsList);
                for(int i=0;i<sizeBoundsList; i++)
                {
                    bounds.Add(null);
                }
                behaviorTree1.AssignBoundsToList(bounds);
                List<double> paramsTeamNotLearning = loadMostRecentParams(teamNotLearning, PARAM_DIR);
                List<double> mostRecentParamsTeamLearning = loadMostRecentParams(teamThatIsLearning, PARAM_DIR);
                if(paramsTeamNotLearning.Count() == 0)
                {
                    for (int i = 0; i < sizeBoundsList; i++)
                    {
                        paramsTeamNotLearning.Add(double.NaN);
                    }
                    behaviorTree1.AssignRandomBoundedValuesToList(paramsTeamNotLearning);
                }
                if (mostRecentParamsTeamLearning.Count() == 0)
                {
                    for (int i = 0; i < sizeBoundsList; i++)
                    {
                        mostRecentParamsTeamLearning.Add(double.NaN);
                    }
                    behaviorTree2.AssignRandomBoundedValuesToList(mostRecentParamsTeamLearning);
                }

                RunOptimization cf = new RunOptimization(walls, teamThatIsLearning, paramsTeamNotLearning, behaviorTree1);

                ParticleSwarm swarm = new ParticleSwarm(numParticlesInSwarm, bounds);
                swarm.GetParticle(0).SeedWithParams(mostRecentParamsTeamLearning);
                GameOptimizationSolution sol = swarm.runOptimization(isMultiThreadOptimization, numGenerationsPso,
                    numGenerationsRestart, cf) as GameOptimizationSolution;
                Console.WriteLine(sol);

                behaviorTree1.assignFromParameters(sol.GetParametersForSolution());
                String paramsAsJson = JsonConvert.SerializeObject(sol.GetParametersForSolution());

                File.WriteAllText(PARAM_DIR +  "\\" + DateTimeOffset.UtcNow.ToUnixTimeSeconds() + teamThatIsLearning + ".json", paramsAsJson);
            }
            else
            {
                List<double> paramsForTeamThatIsLearning = loadMostRecentParams(teamThatIsLearning, PARAM_DIR);
                List<double> paramsForTeamThatIsNotLearning = loadMostRecentParams(teamNotLearning, PARAM_DIR);
                if(teamThatIsLearning == GoalSide.LEFT)
                {
                    behaviorTree1.assignFromParameters(paramsForTeamThatIsLearning);
                    behaviorTree2.assignFromParameters(paramsForTeamThatIsNotLearning);
                }
                else
                {
                    behaviorTree1.assignFromParameters(paramsForTeamThatIsNotLearning);
                    behaviorTree2.assignFromParameters(paramsForTeamThatIsLearning);
                }
                SoccerMatch soccerMatch = SoccerMatch.GetShallowCopy(walls, ref team1, ref team2, ref ball);
                soccerMatch.TIME_LIMIT = 500.0;
                soccerMatch.TIME_TO_CONSIDER_ACTION = TIME_TO_CONSIDER_ACTION;
                matches.Enqueue(soccerMatch);
                //Initialize the allowable play list
                List<IPlay> listOfPlays1 = new List<IPlay> {
                 new KickBall(behaviorTree1, walls.GetXBounds(), walls.GetYBounds()),
                 new MoveTo(behaviorTree1, walls.GetXBounds(), walls.GetYBounds()),
                };

                List<IPlay> listOfPlays2 = new List<IPlay> {
                 new KickBall(behaviorTree2, walls.GetXBounds(), walls.GetYBounds()),
                 new MoveTo(behaviorTree2, walls.GetXBounds(), walls.GetYBounds()),
                };

                Task<Tuple<Scorer, Scorer>> scorers = Task.Run(() => soccerMatch.InitializeAndStartMatch(walls, listOfPlays1, listOfPlays2, GoalSide.LEFT, isRunRealTime, MULTIPLE_OF_REALTIME));
                runningGames.Add(scorers);

                uiSoccerPitch.AddTeam(team1, Colors.Red);
                uiSoccerPitch.AddTeam(team2, Colors.Blue);
                if (isVisualize)
                {
                    tokenSource2 = new CancellationTokenSource();
                    CancellationToken ct = tokenSource2.Token;
                    Task.Run(() =>
                    {
                        while (true)
                        {
                            ct.ThrowIfCancellationRequested();
                            PitchCanvas.Dispatcher.Invoke(() => RunVisualization(team1, team2, ball));
                        }

                    }, tokenSource2.Token);
                }
            }
        }

        public List<double> loadMostRecentParams(GoalSide team, string paramDir)
        {
            DirectoryInfo directory = new DirectoryInfo(paramDir);
            IEnumerable<FileInfo> files = directory.GetFiles()
                 .Where(f => f.Name.Contains(team.ToString()));
            if(files.Count() == 0)
            {
                return new List<double>();
            }
            FileInfo mostRecentMatching = files
                 .OrderByDescending(f => f.LastWriteTime)
                 .First();
            string jsonStr = File.ReadAllText(mostRecentMatching.FullName);
            List<double> paramsForTeam = JsonConvert.DeserializeObject<List<double>>(jsonStr);
            return paramsForTeam;
        }

        private void RunVisualization(Team team1, Team team2, Ball ball)
        {

            uiSoccerPitch.UpdatePlayerEllipsePositions(team1);
            uiSoccerPitch.UpdatePlayerEllipsePositions(team2);
            uiSoccerPitch.UpdateBallEllipse(ball.Location);
        }

        private void StartGameClick(object sender, RoutedEventArgs e)
        {
            if(runOptimizer)
            {
                GoalSide teamThatIsLearning = GoalSide.RIGHT;
                for(int i = 0; i < numOptimizationRounds; i++)
                {
                    reset(teamThatIsLearning);
                    if(teamThatIsLearning == GoalSide.LEFT)
                    {
                        teamThatIsLearning = GoalSide.RIGHT;
                    }
                    else
                    {
                        teamThatIsLearning = GoalSide.LEFT;
                    }
                }
            }
            if (null != tokenSource2)
            {
                tokenSource2.Cancel();
            }
            reset(GoalSide.LEFT);

            Console.WriteLine("Finished");
        }

        public class RunOptimization : ICostFunction
        {
            Walls walls;
            GoalSide teamThatIsLearning;
            List<double> paramsTeamNotLearning;
            BehaviorTree.BehaviorTree referenceTree;

            public RunOptimization( Walls walls, GoalSide teamThatIsLearning, List<double> paramsTeamNotLearning,
                BehaviorTree.BehaviorTree referenceTree)
            {
                this.walls = walls;
                this.teamThatIsLearning = teamThatIsLearning;
                this.paramsTeamNotLearning = paramsTeamNotLearning;
                this.referenceTree = referenceTree;
            }

            public IOptimizationSolution computeCostFunction(List<double> parameters)
            {

                Team team1 = Team.InitializeDefaultTeam(GoalSide.LEFT);
                Team team2 = Team.InitializeDefaultTeam(GoalSide.RIGHT);
                Ball ball = new Ball();
                SoccerMatch soccerMatch = SoccerMatch.GetShallowCopy(walls, ref team1, ref team2, ref ball);
                soccerMatch.TIME_LIMIT = TIME_LIMIT;
                soccerMatch.TIME_TO_CONSIDER_ACTION = TIME_TO_CONSIDER_ACTION;

                IMovingEntity ownGoal = new Ball();
                ownGoal.Location = new Vect3(walls.LeftWall, 0.0, 0.0);
                IMovingEntity otherGoal = new Ball();
                otherGoal.Location = new Vect3(walls.RightWall, 0.0, 0.0);

                ISamplingStrategy sampling = new BoxSampling(20);
                sampling.SetBounds(new double[] { 50.0, 50.0, 50.0, 50.0, 25.0, 25.0, 25.0, 25.0 });

                IMovingEntity ballMovingEntity = ball;

                BehaviorTree.BehaviorTree leftTree
                    = BehaviorTree.BehaviorTree.GetRandomlyGenerated(ref ballMovingEntity, team2.Players,
                    team1.Players, ref ownGoal, ref otherGoal, sampling);

                BehaviorTree.BehaviorTree rightTree
                    = BehaviorTree.BehaviorTree.GetRandomlyGenerated(ref ballMovingEntity, team1.Players,
                    team2.Players, ref otherGoal, ref ownGoal, sampling);


                List<IPlay> listOfPlays1 = null;
                List<IPlay> listOfPlays2 = null;

                if (teamThatIsLearning == GoalSide.LEFT)
                {
                    leftTree.assignFromParameters(parameters);
                    rightTree.assignFromParameters(paramsTeamNotLearning);
                }
                else
                {
                    rightTree.assignFromParameters(parameters);
                    leftTree.assignFromParameters(paramsTeamNotLearning);
                }

                 listOfPlays1 = new List<IPlay> {
                 new KickBall(leftTree, walls.GetXBounds(), walls.GetYBounds()),
                 new MoveTo(leftTree, walls.GetXBounds(), walls.GetYBounds()),
                };

                 listOfPlays2 = new List<IPlay> {
                 new KickBall(rightTree, walls.GetXBounds(), walls.GetYBounds()),
                 new MoveTo(rightTree, walls.GetXBounds(), walls.GetYBounds()),
                };

                Tuple<Scorer, Scorer> matchResult = soccerMatch.InitializeAndStartMatch(walls, listOfPlays1, listOfPlays2, teamThatIsLearning, false);
                double leftTeamScore = matchResult.Item1.computeScore().Item3;
                double rightTeamScore = matchResult.Item2.computeScore().Item3;

                double myTeamScore = teamThatIsLearning == GoalSide.LEFT ? leftTeamScore : rightTeamScore;
                double otherTeamScore = teamThatIsLearning == GoalSide.LEFT ? rightTeamScore : leftTeamScore;

                double differential = myTeamScore - otherTeamScore;

                return new GameOptimizationSolution( myTeamScore,  otherTeamScore,  differential,  parameters,
                   teamThatIsLearning == GoalSide.LEFT ? matchResult.Item1 : matchResult.Item2);
            }

            /**
             * Create a single particle with all of the best players
             * from all of the other particles
             */
            public Particle fuseParticleSolutions(Particle[] particles)
            {
                Particle fused = new Particle(particles[0]);
                Dictionary< PlayerPositionEnum, double> currBest = new Dictionary< PlayerPositionEnum, double>();
                Dictionary<PlayerPositionEnum, Particle> playerToContainingParticle = new Dictionary<PlayerPositionEnum, Particle>();
                foreach (Particle p in particles)
                {
                    GameOptimizationSolution gameOptSol =  p.bestFoundSoFar as GameOptimizationSolution;
                    Dictionary<PlayerPositionEnum, double> score = gameOptSol.scorer.GetPlayerScores();
                    foreach(PlayerPositionEnum player in score.Keys)
                    {
                        double scorePlayer = score[player];
                        if (!currBest.ContainsKey(player) || currBest[player] < scorePlayer)
                        {
                            currBest[player] = scorePlayer;
                            playerToContainingParticle[player] = p;
                        }
                    }
                }

                List<int> allIndices = new List<int>();
                foreach(PlayerPositionEnum player in playerToContainingParticle.Keys)
                {
                    List<int> relParamIndices = referenceTree.GetRelevantParametersForPlayer(player);
                    allIndices.AddRange(relParamIndices);
                    fused.CopyParamsFrom(relParamIndices, playerToContainingParticle[player]);
                }
                fused.RandomizeAllExceptSelectIndices(allIndices);
                return fused;
            }
        }

        public class GameOptimizationSolution : IOptimizationSolution
        {

            double myTeamScore;
            double otherTeamScore;
            double differential;
            List<double> parameters;
            public Scorer scorer;

            public GameOptimizationSolution(double myTeamScore, double otherTeamScore,
                double differential, List<double> parameters, Scorer scorer)
            {
                this.myTeamScore = myTeamScore;
                this.otherTeamScore = otherTeamScore;
                this.differential = differential;
                this.parameters = new List<double>(parameters);
                this.scorer = scorer;
            }

            public int CompareTo(object obj)
            {
                GameOptimizationSolution otherSol = (GameOptimizationSolution)obj;
                int compOnTeamScore = myTeamScore.CompareTo(otherSol.myTeamScore);
                if (compOnTeamScore != 0)
                {
                    return compOnTeamScore;
                }
                int compOnDiff = differential.CompareTo(otherSol.differential);
                if(compOnDiff != 0)
                {
                    return compOnDiff;
                }
                //Reverse the order of the comparison to minimize the other team score
                int compOnOtherTeamScore = otherSol.otherTeamScore.CompareTo(otherTeamScore);
                if (compOnTeamScore != 0)
                {
                    return compOnOtherTeamScore;
                }
                return -1;
            }

            public IOptimizationSolution deepCopy()
            {
                return new GameOptimizationSolution(myTeamScore, otherTeamScore, differential, parameters, scorer);
            }

            public List<double> GetParametersForSolution()
            {
                return parameters;
            }

            public bool isGoodEnoughToSearchAround()
            {
                return myTeamScore > 0.0;
            }

            public bool IsSolution()
            {
                return differential >= 4_000.0;
            }

            public override string ToString()
            {
                return "MyTeam: " + myTeamScore + ", Other Team: " + otherTeamScore +", Differential: " + differential;
            }
        }


        

    }
}
