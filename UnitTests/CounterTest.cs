﻿using BehaviorTree;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace UnitTests
{
    public class CounterTest
    {
        [Test]
        public void testCounter()
        {
            Counter counter = new Counter();
            int val = counter.GetThenIncrement();
            Assert.AreEqual(0, val);
            Assert.AreEqual(1, counter.Count);
            val = counter.GetThenIncrementBy(5);
            Assert.AreEqual(1, val);
            Assert.AreEqual(6, counter.Count);
            int[] range = counter.GetAndIncrementIndexRange(4);
            Assert.AreEqual(new int[] { 6, 9 }, range);
            Assert.AreEqual(10, counter.Count);

           val = counter.GetThenIncrementBy(0);
           Assert.AreEqual(val, counter.Count);
           range = counter.GetAndIncrementIndexRange(0);
            Assert.AreEqual(val, range[0]);
            Assert.AreEqual(range[0], range[1]);

        }
    }
}
