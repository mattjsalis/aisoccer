﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Utility;

namespace UnitTests
{
    public class ArrayUtilitiesTest
    {
        [Test]
        public void GetLinearlySpacedValuesTest()
        {
            Assert.AreEqual(new double[] { 5, 10, 15, 20, 25 },
                ArrayUtilities.GetLinearlySpacedValues(5, 25, 5));
        }
    }
}
