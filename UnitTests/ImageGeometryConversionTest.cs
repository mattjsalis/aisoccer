using NUnit.Framework;
using Utility;

namespace UnitTests
{
    public class ImageGeometryConversionTest
    {
        [SetUp]
        public void Setup()
        {
        }



        [Test]
        public void canvasPositionToFieldPositionTest()
        {
            Assert.AreEqual(new Vect3(-50.0, 50.0, 0.0),
                ImageGeometryConversion.CanvasPositionToFieldPosition(
                    new Vect3(10,10,0),
                    10,
                    10,
                    100,
                    100)
                );

            Assert.AreEqual(new Vect3(50.0, 50.0, 0.0),
                ImageGeometryConversion.CanvasPositionToFieldPosition(
                    new Vect3(110, 10, 0),
                    10,
                    10,
                    100,
                    100)
                );

            Assert.AreEqual(new Vect3(50.0, -50.0, 0.0),
                ImageGeometryConversion.CanvasPositionToFieldPosition(
                    new Vect3(110, 110, 0),
                    10,
                    10,
                    100,
                    100)
                );
            Assert.AreEqual(new Vect3(-50.0, -50.0, 0.0),
                ImageGeometryConversion.CanvasPositionToFieldPosition(
                    new Vect3(10, 110, 0),
                    10,
                    10,
                    100,
                    100)
                );
        }
    
        [Test]
        public void FieldPositionToCanvasPositionTest()
        {
            Assert.AreEqual(new Vect3(10, 10, 0.0),
                ImageGeometryConversion.FieldPositionToCanvasPosition(
                    new Vect3(-50, 50, 0),
                    10,
                    10,
                    100,
                    100)
                );

            Assert.AreEqual(new Vect3(110.0, 10.0, 0.0),
                ImageGeometryConversion.FieldPositionToCanvasPosition(
                    new Vect3(50, 50, 0),
                    10,
                    10,
                    100,
                    100)
                );

            Assert.AreEqual(new Vect3(110,110, 0.0),
                ImageGeometryConversion.FieldPositionToCanvasPosition(
                    new Vect3(50.0, -50.0, 0),
                    10,
                    10,
                    100,
                    100)
                );
            Assert.AreEqual(new Vect3(10, 110, 0.0),
                ImageGeometryConversion.FieldPositionToCanvasPosition(
                    new Vect3(-50, -50, 0),
                    10,
                    10,
                    100,
                    100)
                );
        }
    
    }
}