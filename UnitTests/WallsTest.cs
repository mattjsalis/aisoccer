﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using SoccerComponents;
using Utility;

namespace UnitTests
{
    public class WallsTest
    {
        
        [Test]
        public void GetProjectionTypeTest()
        {
            Walls walls = new Walls();
            Assert.AreEqual(Projection.ON_PITCH, walls.GetProjectionType(new Vect3(), new Vect3(), false));
            Assert.AreEqual(Projection.BOUNCE_OFF_RIGHT_WALL, walls.GetProjectionType(new Vect3(), new Vect3(double.PositiveInfinity,0,0), false));
            Assert.AreEqual(Projection.BOUNCE_OFF_LEFT_WALL, walls.GetProjectionType(new Vect3(), new Vect3(double.NegativeInfinity, 0, 0), false));
            Assert.AreEqual(Projection.BALL_SCORED_IN_RIGHT_GOAL, walls.GetProjectionType(new Vect3(), new Vect3(double.PositiveInfinity, 0, 0), true));
            Assert.AreEqual(Projection.BALL_SCORED_IN_LEFT_GOAL, walls.GetProjectionType(new Vect3(), new Vect3(double.NegativeInfinity, 0, 0), true));
            Assert.AreEqual(Projection.BOUNCE_OFF_TOP_WALL, walls.GetProjectionType(new Vect3(), new Vect3(0.0,double.PositiveInfinity, 0), false));
            Assert.AreEqual(Projection.BOUNCE_OFF_BOTTOM_WALL, walls.GetProjectionType(new Vect3(), new Vect3(0.0,double.NegativeInfinity, 0), false));
            Assert.AreEqual(Projection.BALL_SCORED_IN_LEFT_GOAL,
                walls.GetProjectionType(new Vect3(walls.LeftWall + 5, walls.TopWall, 0), 
                                        new Vect3(walls.LeftWall - 5, walls.BottomWall, 0), 
                                        true));
            Assert.AreEqual(Projection.BALL_SCORED_IN_RIGHT_GOAL,
                walls.GetProjectionType(new Vect3(walls.RightWall - 5, walls.TopWall, 0),
                                        new Vect3(walls.RightWall + 5, walls.BottomWall, 0),
                                        true));
        }
        //[Test]
        //public void ResolveWallBounceTest()
        //{
        //    Walls walls = new Walls();
        //    double height = walls.TopWall - walls.BottomWall;
        //    Assert.That(walls.ResolveWallBounce(new Vect3(), new Vect3(0.0, walls.TopWall + height / 2, 0), false).EqualTo(new Vect3()));
        //    Assert.That(walls.ResolveWallBounce(new Vect3(), new Vect3(-10.0, walls.TopWall + height / 2, 0), false).EqualTo(new Vect3(-10, 0.0,0.0)));
        //    Assert.AreEqual(new Vect3(-67.5, -42.5, 0),
        //        walls.ResolveWallBounce(new Vect3(walls.LeftWall + 5, walls.TopWall, 0),
        //                                new Vect3(walls.LeftWall - 5, walls.BottomWall, 0),
        //                                true));
        //}
    }
    
}
