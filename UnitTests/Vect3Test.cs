﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Utility;

namespace UnitTests
{
    public class Vect3Test
    {
        public void assertEquals(double expected, double actual, double epilson)
        {
            Assert.That(expected, Is.EqualTo(actual).Within(epilson));
        }

        [Test]
        public void RelativeAngleBetweenVectorsTest()
        {
            assertEquals(135,
                (new Vect3(1.0, 0.0, 0.0)).RelativeAngleBetweenVectors(new Vect3(-1.0, -1.0, 0.0)).Degrees,
                1E-6);

            assertEquals(45, 
                (new Vect3(1.0,0.0,0.0)).RelativeAngleBetweenVectors(new Vect3(1.0, 1.0, 0.0)).Degrees,
                1E-6);

            assertEquals(90,
                (new Vect3(1.0, 0.0, 0.0)).RelativeAngleBetweenVectors(new Vect3(0.0, 1.0, 0.0)).Degrees,
                1E-6);
            assertEquals(180,
                (new Vect3(1.0, 0.0, 0.0)).RelativeAngleBetweenVectors(new Vect3(-1.0, 0.0, 0.0)).Degrees,
                1E-6);

            
        }
    }
}
