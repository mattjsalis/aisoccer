﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Utility;
using System.Linq;

namespace UnitTests
{
    public class InterceptGeometryTest
    {

        public void assertEquals(double expected,double actual,double epilson)
        {
            Assert.That(expected, Is.EqualTo(actual).Within(epilson));
        }


        [Test]
        public void GetTimeToInterceptTest()
        {
            //60 deg angle should result in a inercept triangle with all equal sides
            // so that startingDistance = velocityOfInterceptor*time = velocityOfInterceptee*time
            //  meaning, time = startingDistance/velocityOfInterceptor 
            double[] timeToIntercept = InterceptCalculation.GetTimeToIntercepts(Angle.FromDegrees(60.0), 10.0, 10.0, 50.0);
            assertEquals(5.0, timeToIntercept[0],1E-6);

            //Should not be able to intercept when interceptor is orders of magnitude slower than interceptee
            timeToIntercept = InterceptCalculation.GetTimeToIntercepts(Angle.FromDegrees(60.0), 10.0, 1000.0, 50.0);
            Assert.That(timeToIntercept, Is.Null);
 
            //Following results from some Excel calculations
            timeToIntercept = InterceptCalculation.GetTimeToIntercepts(Angle.FromDegrees(45.0), 200.0, 10.0, 50.0);
            assertEquals(timeToIntercept[0],0.241609,1E-4);
            
            //This one should return two solutions
            timeToIntercept = InterceptCalculation.GetTimeToIntercepts(Angle.FromDegrees(45.0), 100, 110.0, 50.0);
            Assert.AreEqual(timeToIntercept.Length, 2);
            assertEquals(timeToIntercept.Max(), 3.34835, 1E-4);
            assertEquals(timeToIntercept.Min(), 0.35554096, 1E-4);

            //Test at angle of 90 deg
            timeToIntercept = InterceptCalculation.GetTimeToIntercepts(Angle.FromDegrees(90.0), 110.0, 100.0, 50.0);
            assertEquals(timeToIntercept[0], 1.091089451, 1E-4);

            //Test at angle of 180 deg
            timeToIntercept = InterceptCalculation.GetTimeToIntercepts(Angle.FromDegrees(180.0), 110.0, 100.0, 50.0);
            assertEquals(timeToIntercept[0], 5.0, 1E-4);

            //Test angle greater than 90
            timeToIntercept = InterceptCalculation.GetTimeToIntercepts(Angle.FromDegrees(135.0), 110, 100.0, 50.0);
            assertEquals(timeToIntercept[0], 3.689813711, 1E-4);

            //The quadratic for this one should return two negative times which should cause the code to return null
            timeToIntercept = InterceptCalculation.GetTimeToIntercepts(Angle.FromDegrees(135.0), 100, 110.0, 50.0);
            Assert.That(timeToIntercept, Is.Null);

        }

        [Test]
        public void GetMaxRelativeAngleTest()
        {
            assertEquals(180,InterceptCalculation.GetMaxRelativeAngle(1000.0, 10.0).Degrees, 1E-6);
            assertEquals(90, InterceptCalculation.GetMaxRelativeAngle(100.0, 100.0).Degrees, 1E-6);
            assertEquals(180,InterceptCalculation.GetMaxRelativeAngle(100, 0.0).Degrees, 1E-6);
            assertEquals(30, InterceptCalculation.GetMaxRelativeAngle(100, 200).Degrees, 1E-6);
        }
    }
}
