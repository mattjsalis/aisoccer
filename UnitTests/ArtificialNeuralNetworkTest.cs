﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NeuralNet;
using Utility;

namespace UnitTests
{
    public class ArtificialNeuralNetworkTest
    {
        [Test]
        public void TestGetSameResultEveryTime()
        {
            //Instantiate a new neural net
            ArtificialNeuralNet ann = new ArtificialNeuralNet(2, 1, new int[] { 3, 3 }, -1, 1, -5, 5);
            double[] result = ann.CalculateNeuralNet(new double[] { 2.0, 3.0 });
            for(int i = 0; i < 100; i++)
            {
                Assert.AreEqual(result[0], ann.CalculateNeuralNet(new double[] { 2.0, 3.0 })[0]);
            }

        }

        [Test]
        public void TestSerialization()
        {
            ArtificialNeuralNet ann = new ArtificialNeuralNet(2, 1, new int[] { 3, 3 }, -1, 1, -5, 5);
            double result = ann.CalculateNeuralNet(2.0, 3.0)[0];
            string annString = XMLSerializer<ArtificialNeuralNet>.SerializeToDocumentText(ann, true);
            ann = XMLSerializer<ArtificialNeuralNet>.Deserialize(annString);
            Assert.AreEqual(result, ann.CalculateNeuralNet(2.0, 3.0)[0]);
        }
    }
}
