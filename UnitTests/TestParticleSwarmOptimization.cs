﻿using ActionPotential;
using BehaviorTree;
using GameElements;
using NUnit.Framework;
using Optimization;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;
using Utility;

namespace UnitTests
{

    
    public class TestParticleSwarmOptimization
    {

        private Dictionary<PlayerPositionEnum, IMovingEntity> assignRandomPlayers()
        {
            Dictionary<PlayerPositionEnum, IMovingEntity> players = new Dictionary<PlayerPositionEnum, IMovingEntity>();
            List<PlayerPositionEnum> playerPositionEnums = EnumUtility.GetAsList<PlayerPositionEnum>(typeof(PlayerPositionEnum));
            foreach (PlayerPositionEnum playerPositionEnum in playerPositionEnums)
            {
                double randX = Probability.RandomBetween(-50.0, 50.0);
                double randY = Probability.RandomBetween(-25.0, 25.0);
                players[playerPositionEnum] = new Player(new Vect3(randX, randY, 0.0), new Vect3());
            }
            return players;
        }

        [Test]
        public void testOptimization()
        {
            for (int j = 0; j < 10; j++)
            {
                double[] xField = new double[] { -100.0, 100.0 };
                double[] yField = new double[] { -50.0, 50.0 };
                Dictionary<PlayerPositionEnum, IMovingEntity> ownTeamPlayers = assignRandomPlayers();
                Dictionary<PlayerPositionEnum, IMovingEntity> otherTeamPlayers = assignRandomPlayers();

                IMovingEntity[] ownTeam = new IMovingEntity[ownTeamPlayers.Count];
                IMovingEntity[] otherTeam = new IMovingEntity[otherTeamPlayers.Count];

                ownTeamPlayers.Values.CopyTo(ownTeam, 0);
                otherTeamPlayers.Values.CopyTo(otherTeam, 0);

                IMovingEntity ownGoal = new Player(new Vect3(xField[0], 0.0, 0.0), new Vect3());
                IMovingEntity otherGoal = new Player(new Vect3(xField[1], 0.0, 0.0), new Vect3());
                IMovingEntity ball = new Player(new Vect3(0.0, 0.0, 0.0), new Vect3());
                ISamplingStrategy box = new BoxSampling(20);
                box.SetBounds(new double[] { 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0 });

                Counter counter = new Counter();
                MacroActionPotential macro = LeafNode.GetMacroWithTwoBivariateNormalDist(ref ball, ref counter);
                List<double> paramList = new List<double>();
                for (int i = 0; i < counter.Count; i++)
                {
                    paramList.Add(double.NaN);
                }
                macro.AssignRandomBoundedValuesToList(paramList);
                Assert.False(paramList.Contains(double.NaN));
                macro.assignFromParameters(paramList);
                ActionPotentialTest.PlotHeatMap(ActionPotentialTest.solDir + "/output/bvndMacro.png", macro);

                counter = new Counter();
                LeafNode leafNode = LeafNode.GetDefaultParameterizablLeafNode(ref counter,
                       ref ball, otherTeam, ownTeam, ref ownGoal, ref otherGoal);
                paramList = new List<double>();
                for (int i = 0; i < counter.Count; i++)
                {
                    paramList.Add(double.NaN);
                }
                leafNode.AssignRandomBoundedValuesToList(paramList);
                Assert.False(paramList.Contains(double.NaN));
                leafNode.assignFromParameters(paramList);

                ActionPotentialCostFunction costFunction = new ActionPotentialCostFunction(leafNode);
                List<double[]> bounds = new List<double[]> { new double[] { -100.0, 100.0 }, new double[] { -50.0, 50.0 } };
                ParticleSwarm swarm = new ParticleSwarm(30, bounds);
                IOptimizationSolution sol = swarm.runOptimization(true, 100, 30, costFunction);
                List<double> parameters = sol.GetParametersForSolution();
                List<double[]> scatter = new List<double[]> { new double[] { parameters[0], parameters[1] } };
                ActionPotentialTest.PlotHeatMap(ActionPotentialTest.solDir + "/output/AAleafNodeParticleOpt" + j + ".png", leafNode,
                    scatter, scatter, -100.0, 100.0, -50.0, 50.0, 100, true, false);
            }

        }

        public class ActionPotentialCostFunction : ICostFunction
        {

            ActionPotential.ActionPotential actionPotential;
            

            public ActionPotentialCostFunction(ActionPotential.ActionPotential actionPotential) 
            { 
                this.actionPotential = actionPotential;
            }

            public IOptimizationSolution computeCostFunction(List<double> parameters)
            {
                double value = actionPotential.potentialAt(parameters[0], parameters[1]);
                return new ActionPotentialOptimizationSolution(value, parameters);
            }
        }

        public class ActionPotentialOptimizationSolution : IOptimizationSolution
        {

            double value;
            List<double> parameters;

            public ActionPotentialOptimizationSolution(double value, List<double> parameters)
            {
                this.value = value;
                this.parameters = parameters;
            }

            public int CompareTo(object obj)
            {
                ActionPotentialOptimizationSolution cast = obj as ActionPotentialOptimizationSolution;
                return value.CompareTo(cast.value);
            }

            public IOptimizationSolution deepCopy()
            {
                return new ActionPotentialOptimizationSolution(value, new List<double>(parameters));
            }

            public List<double> GetParametersForSolution()
            {
                return parameters;
            }

            public bool isGoodEnoughToSearchAround()
            {
                return true;
            }

            public override string ToString()
            {
                return "x: " + parameters[0] + ", y: " + parameters[1] + ", val: " +  value;
            }
        }
    }
}
