﻿using ActionPotential;
using BehaviorTree;
using GameElements;
using NUnit.Framework;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace UnitTests
{
    public class LeafNodeTest
    {
        private Dictionary<PlayerPositionEnum, IMovingEntity> assignRandomPlayers()
        {
            Dictionary<PlayerPositionEnum, IMovingEntity> players = new Dictionary<PlayerPositionEnum, IMovingEntity>();
            List<PlayerPositionEnum> playerPositionEnums = EnumUtility.GetAsList<PlayerPositionEnum>(typeof(PlayerPositionEnum));
            foreach (PlayerPositionEnum playerPositionEnum in playerPositionEnums)
            {
                double randX = Probability.RandomBetween(-50.0, 50.0);
                double randY = Probability.RandomBetween(-25.0, 25.0);
                players[playerPositionEnum] = new Player(new Vect3(randX, randY, 0.0), new Vect3());
            }
            return players;
        }


        [Test]
        public void testDefaultLeafNodeDefinitionDefinition()
        {
            double[] xField = new double[] { -50.0, 50.0 };
            double[] yField = new double[] { -25.0, 25.0 };
            Dictionary<PlayerPositionEnum, IMovingEntity> ownTeamPlayers = assignRandomPlayers();
            Dictionary<PlayerPositionEnum, IMovingEntity> otherTeamPlayers = assignRandomPlayers();

            IMovingEntity[] ownTeam = new IMovingEntity[ownTeamPlayers.Count];
            IMovingEntity[] otherTeam = new IMovingEntity[otherTeamPlayers.Count];

            ownTeamPlayers.Values.CopyTo(ownTeam, 0);
            otherTeamPlayers.Values.CopyTo(otherTeam, 0);

            IMovingEntity ownGoal = new Player(new Vect3(xField[0], 0.0, 0.0), new Vect3());
            IMovingEntity otherGoal = new Player(new Vect3(xField[1], 0.0, 0.0), new Vect3());
            IMovingEntity ball = new Player(new Vect3(0.0, 0.0, 0.0), new Vect3());
            ISamplingStrategy box = new BoxSampling(20);
            box.SetBounds(new double[] { 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0 });

            Counter counter = new Counter();
            MacroActionPotential macro = LeafNode.GetMacroWithTwoBivariateNormalDist(ref ball, ref counter);
            List<double> paramList = new List<double>();
            for (int i = 0; i < counter.Count; i++)
            {
                paramList.Add(double.NaN);
            }
            macro.AssignRandomBoundedValuesToList(paramList);
            Assert.False(paramList.Contains(double.NaN));
            macro.assignFromParameters(paramList);
            //ActionPotentialTest.PlotHeatMap(ActionPotentialTest.solDir + "/output/bvndMacro.png", macro);

            counter = new Counter();
            LeafNode leafNode = LeafNode.GetDefaultParameterizablLeafNode(ref counter,
                   ref ball, otherTeam, ownTeam, ref ownGoal, ref otherGoal);
            paramList = new List<double>();
            for (int i = 0; i < counter.Count; i++)
            {
                paramList.Add(double.NaN);
            }
            leafNode.AssignRandomBoundedValuesToList(paramList);
            Assert.False(paramList.Contains(double.NaN));
            leafNode.assignFromParameters(paramList);
            //ActionPotentialTest.PlotHeatMap(ActionPotentialTest.solDir + "/output/leafNode.png", leafNode);

            counter = new Counter();
            BehaviorTree.BehaviorTree behaviorTree = new BehaviorTree.BehaviorTree(ref counter,
                   ref ball, otherTeam, ownTeam, ref ownGoal, ref otherGoal, box);
            paramList = new List<double>();
            List<double[]> bounds = new List<double[]>();
            for (int i = 0; i < counter.Count; i++)
            {
                paramList.Add(double.NaN);
            }
            for (int i = 0; i < paramList.Count; i++)
            {
                bounds.Add(null);
            }
            behaviorTree.AssignRandomBoundedValuesToList(paramList);
            Assert.False(paramList.Contains(double.NaN));
            behaviorTree.AssignBoundsToList(bounds);
            for (int i = 0; i < paramList.Count; i++)
            {
                Assert.True(paramList[i] >= bounds[i][0], paramList[i] + "<" +  bounds[i][0]);
                Assert.True(paramList[i] <= bounds[i][1], paramList[i] + ">" + bounds[i][1]);
            }

            behaviorTree.assignFromParameters(paramList);

            BehaviorTree.BehaviorTree deepCopy = behaviorTree.DeepCopy() as BehaviorTree.BehaviorTree;
            deepCopy.AssignBoundsToList(bounds);
            deepCopy.AssignRandomBoundedValuesToList(paramList);
            for (int i = 0; i < paramList.Count; i++)
            {
                Assert.True(paramList[i] >= bounds[i][0], paramList[i] + "<" + bounds[i][0]);
                Assert.True(paramList[i] <= bounds[i][1], paramList[i] + ">" + bounds[i][1]);
            }
            deepCopy.assignFromParameters(paramList);


            Array playerPositions = Enum.GetValues(typeof(PlayerPositionEnum));
            Array teamPoss = Enum.GetValues(typeof(TeamPossessionEnum));
            Array playerPoss = Enum.GetValues(typeof(PlayerPosessionEnum));
            Array playTypes = Enum.GetValues(typeof(PlayTypeEnum));

            foreach(var playerPosition in playerPositions)
            {
                foreach (var teamPos in teamPoss)
                {
                    foreach (var playerPos in playerPoss)
                    {
                        foreach (var playType in playTypes)
                        {
                            

                            PlayerPositionEnum playerPositionEnum = (PlayerPositionEnum)playerPosition;
                            TeamPossessionEnum teamPosEnum = (TeamPossessionEnum)teamPos;
                            PlayerPosessionEnum playerPosEnum = (PlayerPosessionEnum)playerPos;
                            PlayTypeEnum playTypeEnum = (PlayTypeEnum)playType;

                            LeafNode leaf = behaviorTree.GetLeafNode(playerPositionEnum, teamPosEnum, playerPosEnum, playTypeEnum);
                            
                            
                            if(leaf ==null)
                            {
                                //Make sure leaf should be null
                                if(teamPosEnum == TeamPossessionEnum.TEAM && playerPosEnum == PlayerPosessionEnum.YES)
                                {
                                    Assert.Fail("Player with possession should have all moves available.");
                                }
                                if(teamPosEnum != TeamPossessionEnum.TEAM && playerPosEnum == PlayerPosessionEnum.YES)
                                {
                                    //Non-sensical enumeration: should be null
                                    continue;
                                }
                                if(playTypeEnum == PlayTypeEnum.MOVE_TO)
                                {
                                    Assert.Fail("All players should be able to move regardless of possession");
                                }
                                continue;
                            }
                            else
                            {
                                //Make sure leaf should not be null
                                if(playTypeEnum == PlayTypeEnum.KICK_BALL_TO && 
                                    !(teamPosEnum == TeamPossessionEnum.TEAM && playerPosEnum == PlayerPosessionEnum.YES))
                                {
                                    Assert.Fail("Leaf node should be null for kicking if the" +
                                        " player does not possess the ball"
                                        + "\n" + playTypeEnum
                                        + "\n" + teamPosEnum
                                        + "\n" + playerPosEnum);
                                }
                            }

                            IMovingEntity ownPlayer = ownTeamPlayers[(PlayerPositionEnum)playerPosition];
                            IMovingEntity otherPlayer = otherTeamPlayers[(PlayerPositionEnum)playerPosition];

                            Tuple<List<double[]>, double[]> ownTeamSuggested = 
                                behaviorTree.ComputeBestAndSuggestedLocs((PlayerPositionEnum)playerPosition,
                                 (TeamPossessionEnum)teamPos, (PlayerPosessionEnum)playerPos,
                                 (PlayTypeEnum)playType,
                                xField, yField, ownPlayer);

                            Tuple<List<double[]>, double[]> otherTeamSuggested =
                                behaviorTree.ComputeBestAndSuggestedLocs((PlayerPositionEnum)playerPosition,
                                 (TeamPossessionEnum)teamPos, (PlayerPosessionEnum)playerPos,
                                 (PlayTypeEnum)playType, xField, yField, otherPlayer);

                            ownTeamSuggested.Item1.AddRange(otherTeamSuggested.Item1);

                            List<double[]> bestPoints = new List<double[]>()
                            {
                                ownTeamSuggested.Item2, otherTeamSuggested.Item2
                            };

                            string suffix = playerPositionEnum + "_" + teamPosEnum + "_" + playerPosEnum + "_" + playTypeEnum + ".png";
                            ActionPotentialTest.PlotHeatMap(ActionPotentialTest.solDir 
                                + "/output/leafNode" + suffix, leaf,
                                ownTeamSuggested.Item1,
                                bestPoints,
                                xField[0], xField[1], yField[0], yField[1],
                                100, true);
                        }
                    }
                }
            }

            
        }
    }
}
