﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Utility;

namespace UnitTests
{
    public class GeometryUtilityTest
    {

        public void assertEquals(double expected, double actual, double epilson)
        {
            Assert.That(expected, Is.EqualTo(actual).Within(epilson));
        }

        [Test]
        public void IsInWedgeTest()
        {
            Vect3 edge1 = new Vect3(1, 1, 0);
            Vect3 edge2 = new Vect3(1, -1, 0);

            Assert.That(GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(1.0, 0.0, 0.0)));
            Assert.That(!GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(-1.0, 0.0, 0.0)));

            edge1 = new Vect3(-1, 1, 0);
            edge2 = new Vect3(-1, -1, 0);

            Assert.That(!GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(1.0, 0.0, 0.0)));
            Assert.That(GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(-1.0, 0.0, 0.0)));

            edge1 = new Vect3(-1, 1, 0);
            edge2 = new Vect3(1, 1, 0);

            Assert.That(GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(0.0, 1.0, 0.0)));
            Assert.That(!GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(0.0, -1.0, 0.0)));

            edge1 = new Vect3(-1, -1, 0);
            edge2 = new Vect3(1, -1, 0);

            Assert.That(!GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(0.0, 1.0, 0.0)));
            Assert.That(GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(0.0, -1.0, 0.0)));
            Assert.That(GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(1.0, -1.0, 0.0)));
            Assert.That(GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3(-1.0, -1.0, 0.0)));
            Assert.That(GeometryUtility.IsInWedge(new Vect3(), edge1, edge2, new Vect3()));
        }
    
        [Test]
        public void GetBoundingAzimuthsTest()
        {
            //Angle between vectors is 45 deg so answer should be 0 and 90 deg
            Tuple<Angle, Angle> bounds = GeometryUtility.GetBoundingAzimuths(new Vect3(), new Vect3(10, 10, 0), Angle.FromDegrees(90));
            assertEquals(0, bounds.Item1.Degrees, 1E-6);
            assertEquals(90, bounds.Item2.Degrees, 1E-6);

            bounds = GeometryUtility.GetBoundingAzimuths(new Vect3(), new Vect3(10, -10, 0), Angle.FromDegrees(90));
            assertEquals(270, bounds.Item1.Degrees, 1E-6);
            assertEquals(360, bounds.Item2.Degrees, 1E-6);

            bounds = GeometryUtility.GetBoundingAzimuths(new Vect3(), new Vect3(-10, -10, 0), Angle.FromDegrees(90));
            assertEquals(180, bounds.Item1.Degrees, 1E-6);
            assertEquals(270, bounds.Item2.Degrees, 1E-6);

            bounds = GeometryUtility.GetBoundingAzimuths(new Vect3(), new Vect3(-10, 10, 0), Angle.FromDegrees(90));
            assertEquals(90, bounds.Item1.Degrees, 1E-6);
            assertEquals(180, bounds.Item2.Degrees, 1E-6);


        }
   
        [Test]
        public void MinDistanceToVectorInterceptTest()
        {
            assertEquals(1.0,
                GeometryUtility.MinDistanceToVectorIntercept(new Vect3(), Vect3.XUnit, new Vect3(1, 1, 0)),
                1E-6);
        }
    }
}
