﻿using ActionPotential;
using GameElements;
using NUnit.Framework;
using ScottPlot;
using ScottPlot.Colormaps;
using ScottPlot.Plottables;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using Utility;

namespace UnitTests
{

    public class ActionPotentialTest
    {
        public static string solDir = Directory.GetParent(Directory.GetCurrentDirectory())
                .Parent.Parent.Parent.Parent.ToString();

        public static List<RootedCoordinateVector> getGradientsForPlot(List<Coordinates> gradCoords, ActionPotential.ActionPotential bvd)
        {
            return gradCoords.Select(gp =>
            {
                Vect3 grad = bvd.gradientAt(gp.X, gp.Y);
                grad.Z = 0.0;
                grad = grad.UnitVector.Times(5.0);
                Vector2 v2 = new Vector2(((float)grad.X), (float)grad.Y);
                RootedCoordinateVector rcv = new RootedCoordinateVector(gp, v2);
                return rcv;
            }
                )
                .ToList();
        }

        public static  List<Coordinates> getVectorFieldCoords()
        {
            double[] xVectorField = ArrayUtilities.GetLinearlySpacedValues(0.0, 100.0, 20);
            double[] yVectorField = ArrayUtilities.GetLinearlySpacedValues(0.0, 50.0, 20);
            List<Coordinates> gradCoords = new List<Coordinates>();
            foreach (double xCoord in xVectorField)
            {
                foreach (double yCoord in yVectorField)
                {
                    gradCoords.Add(new Coordinates(xCoord, yCoord));
                }
            }
            return gradCoords;
        }

        public static void PlotHeatMap(String fileName, ActionPotential.ActionPotential actionPotential,
            List<double[]> scatterPoints=null,
            List<double[]> scatterOfNote=null,
            double xMin=0.0, double xMax=100.0, double yMin=0.0, double yMax=50.0,
            int numToPlot=1000, bool isPlot=true, bool isPlotGradient=false)
        {
            double[] x = ArrayUtilities.GetLinearlySpacedValues(xMin, xMax, numToPlot);
            double[] y = ArrayUtilities.GetLinearlySpacedValues(yMin, yMax, numToPlot);
            

            double[,] field = actionPotential.computeField(x, y);


            if (isPlot)
            {
                ScottPlot.Plot plt1 = new ScottPlot.Plot();
                var hm1 = plt1.Add.Heatmap(field);
                hm1.Colormap = new ScottPlot.Colormaps.Turbo();
                plt1.Axes.SetLimits(xMin, xMax, yMin, yMax);
                hm1.Position = new CoordinateRect(xMin, xMax, yMin, yMax);
                if (isPlotGradient)
                {
                    List<Coordinates> gradCoords = getVectorFieldCoords();
                    List<RootedCoordinateVector> gradients = getGradientsForPlot(gradCoords, actionPotential);
                    VectorField vf = plt1.Add.VectorField(gradients);
                    vf.ArrowFillColor = new Color(255.0f, 255.0f, 255.0f);
                }
                
                if(null != scatterPoints)
                {
                    double[] xs = new double[scatterPoints.Count];
                    double[] ys = new double[scatterPoints.Count];
                    for(int i = 0;i< scatterPoints.Count; i++)
                    {
                        xs[i] = scatterPoints[i][0];
                        ys[i] = scatterPoints[i][1];
                    }

                    plt1.Add.Markers(xs, ys,MarkerShape.FilledSquare, 10, Color.FromColor(System.Drawing.Color.White));
                }
                if(scatterOfNote != null)
                {
                    double[] xsOfNote = new double[scatterOfNote.Count];
                    double[] ysOfNote = new double[scatterOfNote.Count];
                    for (int i = 0; i < scatterOfNote.Count; i++)
                    {
                        xsOfNote[i] = scatterOfNote[i][0];
                        ysOfNote[i] = scatterOfNote[i][1];
                    }
                    plt1.Add.Markers(xsOfNote, ysOfNote, MarkerShape.OpenSquare, 20, Color.FromColor(System.Drawing.Color.White));
                }
                plt1.Add.ColorBar(hm1);
                plt1.SavePng(fileName, 2000, 1000);
            }

        }

        [Test]
        public void testBivariateNormalDistribution()
        {
            Counter counter = new Counter();
            IMovingEntity ball = new Ball();
            IScalingFunction scalar = new Scalar(10.0);
            BivariateNormalDistribution bvd = new BivariateNormalDistribution(ref ball);

            bvd.MakeWithScalarValues(new double[] { 10.0, 10.0, 10.0, 0.0 });

            IScalingFunction scalarNeg = new Scalar(-10.0);
            BivariateNormalDistribution bvdNeg =
                new BivariateNormalDistribution(ref ball);
            bvdNeg.MakeWithScalarValues(new double[] { 10.0, 10.0, -10.0, 0.0 });

            double[] x = ArrayUtilities.GetLinearlySpacedValues(0.0, 100.0, 1000);
            double[] y = ArrayUtilities.GetLinearlySpacedValues(0.0, 50.0, 1000);

            List<Coordinates> gradCoords = getVectorFieldCoords();

            ball.Location = new Vect3(50.0, 10.0, 0.0);
            double[,] field1 = bvd.computeField(x, y);
            List<RootedCoordinateVector> grads1 = getGradientsForPlot(gradCoords, bvd);


            ball.Location = new Vect3(75.0, 25.0, 0.0);
            double[,] field2 = bvdNeg.computeField(x, y);
            List<RootedCoordinateVector> grads2 = getGradientsForPlot(gradCoords, bvdNeg);

            ball.Location = new Vect3(0.0, 33.0, 0.0);
            double[,] field3 = bvd.computeField(x, y);
            List<RootedCoordinateVector> grads3 = getGradientsForPlot(gradCoords, bvd);

            List < double[,]> fields = new List <double[,]> { field1, field2, field3};
            List<List<RootedCoordinateVector>> gradFields = new List<List<RootedCoordinateVector>> { grads1, grads2, grads3};


            for (int i=0;i<fields.Count;i++)
            {
                List<RootedCoordinateVector> gradients = gradFields[i];
                ScottPlot.Plot plt1 = new ScottPlot.Plot();
                
                
                var hm1 =  plt1.Add.Heatmap(fields.ElementAt(i));
                hm1.Colormap = new ScottPlot.Colormaps.Turbo();
                plt1.Axes.SetLimits(0.0, 100.0, 0.0, 50.0);
                hm1.Position = new CoordinateRect(0.0, 100.0, 0.0, 50.0);
                VectorField vf = plt1.Add.VectorField(gradients);
                vf.ArrowFillColor = new Color(255.0f, 255.0f, 255.0f);
                plt1.Add.ColorBar(hm1);
                plt1.SavePng(solDir + "/output/bivariateTest" + i + ".png", 2000, 1000);
            }

            double[,] summed = ActionPotential.ActionPotential.sumFields(fields);
            ScottPlot.Plot plt = new ScottPlot.Plot();
            var hm2 = plt.Add.Heatmap(summed);
            hm2.Colormap = new ScottPlot.Colormaps.Turbo();
            plt.Add.ColorBar(hm2);
            plt.SavePng(solDir + "/output/bivariateTestSummed.png", 2000, 1000);
        }

        [Test]
        public void testBivariateFunction()
        {
            Counter counter = new Counter();
            UnivariateFunction logisticFunction = new LogisticFunction(10.0, 1.0, 50.0, 0.0);
            UnivariateFunction sinFunction = new SinFunction(5.0, 0.5, 0.0);

            BivariateFunction bivariateFunction = new BivariateFunction(logisticFunction, sinFunction);
            PlotHeatMap(solDir + "/output/bivariateXLogisticYSin.png", bivariateFunction);

            UnivariateFunction logisticFunction2 = new LogisticFunction(50.0, 1.0, 50.0, 0.0);
            UnivariateFunction sinFunction0Amp = new SinFunction(0.0, 0.5, 0.0);
            IScalingFunction attractorPeakFunc = new BivariateFunction(logisticFunction2, sinFunction0Amp);

            IMovingEntity weakAttractor = new Ball();
            IMovingEntity strongAttractor = new Ball();
            weakAttractor.Location = new Vect3(50.0, 33.0, 0.0);
            strongAttractor.Location = new Vect3(80.0, 15.0, 0.0);

            BivariateNormalDistribution bndWeak = new BivariateNormalDistribution(ref weakAttractor,
                new Scalar(5.0), new Scalar(5.0), attractorPeakFunc, new Scalar(0.0));

            BivariateNormalDistribution bndStrong = new BivariateNormalDistribution(ref strongAttractor,
                 new Scalar(5.0), new Scalar(5.0), attractorPeakFunc, new Scalar(0.0));

            MacroActionPotential macro = new MacroActionPotential(new List<ActionPotential.ActionPotential>()
                { bivariateFunction, bndWeak, bndStrong});
            PlotHeatMap(solDir + "/output/bivariateXLogisticYSinWithAttractors.png", macro);


        }

        private UnivariateFunction[] GetBoundedUnivariateFunctions()
        {
            LogisticFunction boundedLogistic = new LogisticFunction(0.0, 0.0, 0.0, 0.0);
            boundedLogistic.SetBounds(new double[] { -50, 50, 0.0, 1.0, -100.0, 100.0, -50, 50 });
            SinFunction sinFunction = new SinFunction(0.0, 0.0, 0.0);
            sinFunction.SetBounds(new double[] { -50.0, 50.0,
                1e-3, 2.0, -100.0, 100.0, -100.0, 100.0 });
            Scalar scalar = new Scalar(0.0);
            scalar.SetBounds(new double[] { -100.0, 100.0 });
            return new UnivariateFunction[] { boundedLogistic , sinFunction, scalar };
        }

        private BivariateFunction GetBivariateWithRegisteredFuncs(ref Counter counter)
        {
            UnivariateFunction[] xFuncs = GetBoundedUnivariateFunctions();
            UnivariateFunction[] yFuncs = GetBoundedUnivariateFunctions();
            BivariateFunction bivariateFunction = new BivariateFunction(null, null);
            bivariateFunction.RegisterXFunction(xFuncs, ref counter);
            bivariateFunction.RegisterYFunction(yFuncs, ref counter);
            return bivariateFunction;
        }



        private BivariateFunction[] GetNBivariates(int n, ref Counter counter)
        {
            BivariateFunction[] bivariates = new BivariateFunction[n];
            for(int i=0; i < n;i++)
            {
                bivariates[i] = GetBivariateWithRegisteredFuncs(ref counter);
            }
            return bivariates;
        }

        private BivariateNormalDistribution GetBivariateNormalDistribution(int n, ref IMovingEntity ball, 
            ref Counter counter)
        {
            BivariateNormalDistribution bvd = new BivariateNormalDistribution(ref ball);

            Scalar correlationScalar = new Scalar(0.0);
            correlationScalar.SetBounds(new double[] { -0.9999, 0.99999 });

            bvd.RegisterCorrelationValueFuncs(new IScalingFunction[] { correlationScalar }, ref counter);
            bvd.RegisterScalingValueFuncs(GetNBivariates(n, ref counter), ref counter);
            bvd.RegisterStdevXFuncs(GetNBivariates(n, ref counter), ref counter);
            bvd.RegisterStdevYFuncs(GetNBivariates(n, ref counter), ref counter);
            return bvd;
        }

        private MacroActionPotential GetMacroActionPotential(ref IMovingEntity ball, ref Counter counter)
        {
            BivariateFunction[] bivariates = GetNBivariates(2, ref counter);
            BivariateNormalDistribution bvd = GetBivariateNormalDistribution(5, ref ball, ref counter);
            List<ActionPotential.ActionPotential> potentials = new List<ActionPotential.ActionPotential>();
            potentials.AddRange(bivariates);
            potentials.Add(bvd);
            MacroActionPotential macroActionPotential = new MacroActionPotential(potentials);
            return macroActionPotential;
        }

        [Test]
        public void testParameterBounds()
        {
            //Create a Bivariate normal distribution with BivariateFunction for every variable
            Counter counter = new Counter();
            IMovingEntity ball = new Ball();

            MacroActionPotential macro = GetMacroActionPotential(ref ball, ref counter);

            List<double> paramVals = new List<double> (counter.Count);
            List < double[]> paramBounds = new List<double[]>(counter.Count);
            for(int i =0 ; i < counter.Count;i++)
            {
                paramVals.Add(Double.NaN);
                paramBounds.Add(null);
            }
            for(int i = 0; i < 10;i++)
            {
                ball.Location = new Vect3(Probability.RandomBetween(-100.0, 100.0),
                    Probability.RandomBetween(-100.0, 100.0), 0.0);
                double x = Probability.RandomBetween(-100.0, 100.0);
                double y = Probability.RandomBetween(-100.0, 100.0);
                macro.AssignRandomBoundedValuesToList(paramVals);
                Assert.False(paramVals.Contains(Double.NaN));
                macro.AssignBoundsToList(paramBounds);
                for(int i_bounds=0;i_bounds<paramVals.Count;i_bounds++)
                {
                    double[] bounds = paramBounds[i_bounds];
                    if (!(paramVals[i_bounds] >= bounds[0]
                        && paramVals[i_bounds] <= bounds[1]))
                    {
                        Assert.Fail("param should have been between " +
                            bounds[0] + " and " + bounds[1] 
                            + " but was " + paramVals[i_bounds]);
                    }
                }

                macro.assignFromParameters(paramVals);
                macro.potentialAt(x, y);
                for (int j = 0; j < counter.Count; j++)
                {
                    paramVals[j] = Double.NaN;
                }

                PlotHeatMap(solDir + "/output/randomBoundedSamplingTest" + i + ".png", macro);
            }

        }
    }

}
