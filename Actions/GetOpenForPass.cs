﻿using GameElements;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace Actions
{
    public class GetOpenForPass : IPlay
    {
        public IPlay DeepCopy()
        {
            GetOpenForPass play = new GetOpenForPass();
            play.vectorToRunOn = vectorToRunOn?.Copy();
            return play;
        }

        /// <summary>
        /// The amount of players in a particular wedge that forces
        /// this player to consider a less "optimal" vector. Basically
        /// a parameter that stops the player from always choosing the
        /// 0 player wedge while still reducing the probability of overcrowding.
        /// </summary>
        private static readonly int THRESHOLD_PLAYER_COUNT = 3;

        private static readonly double DISTANCE_TO_RUN_ON_VECT = 10.0;

        /// <summary>
        /// The vector calculated by estimatePlayScore that should be used
        /// if this play is selected
        /// </summary>
        public Vect3 vectorToRunOn = null;

        public double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            double distanceToPossessingPlayer = player.Position.DistanceToVect(ball.Position);
            List<Vect3> allPositions = new List<Vect3>();
            //Find open space positions by considering all players
            allPositions.AddRange(ownTeam.GetPositions());
            allPositions.AddRange(otherTeam.GetPositions());
            SortedDictionary<int, List<Vect3>> wedgeDict = GeometryUtility
                .GetWedgesWithFewestPoints(distanceToPossessingPlayer, player.Position, 30, allPositions);
            double maxDotProd = -Double.MaxValue;
            Vect3 bestVect = new Vect3();
            for (int i=0;i< THRESHOLD_PLAYER_COUNT;i++)
            {
                //Evaluate the vectors
                bool isKeyInMap = wedgeDict.ContainsKey(i);
                if (!isKeyInMap) { continue; }
                Tuple<double, Vect3> bestVectFromList = pickBestVectFromList(player, wedgeDict[i]);
                if(bestVectFromList.Item1 > maxDotProd)
                {
                    maxDotProd = bestVectFromList.Item1;
                    bestVect = bestVectFromList.Item2;
                }
            }
            vectorToRunOn = bestVect;
            if (SoccerMatch.IsRandomPlay)
            {
                return Probability.RandomBetween(0.0, 1.0);
            }
            return 0.0;
        }

        /// <summary>
        /// Picks the vecotr most aligned with he vector from this player to the
        /// goal it is trying to score on
        /// </summary>
        /// <param name="player"></param>
        /// <param name="wedgeVects"></param>
        /// <returns></returns>
        private Tuple<double, Vect3> pickBestVectFromList(Player player, List<Vect3> wedgeVects)
        {
            Vect3 toGoal = SoccerMatch.Walls.Value.GetVectToGoalCenter(player).UnitVector;
            Vect3 mostTowardGoal = new Vect3();
            double greatestDotProduct = -Double.MaxValue;
            foreach (Vect3 v in wedgeVects)
            {
                //How much is this vect towards the player's goal?
                double dotProduct = toGoal.DotProduct(v);
                if(dotProduct > greatestDotProduct)
                {
                    greatestDotProduct = dotProduct;
                    mostTowardGoal = v;
                }
            }
            return Tuple.Create(greatestDotProduct, mostTowardGoal);
        }

        public void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            player.DistanceOnRunToGetOpen = DISTANCE_TO_RUN_ON_VECT;
            player.Velocity = vectorToRunOn.UnitVector.Times(player.MaxRunSpeed);
        }

        public bool isViablePlay(Player player,Ball ball, Team ownTeam, Team otherTeam)
        {
            return !player.HasPosession && ownTeam.HasPosession;
        }
    }
}
