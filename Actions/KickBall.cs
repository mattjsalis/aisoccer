﻿using BehaviorTree;
using GameElements;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actions
{
    public class KickBall : IPlay
    {

        private BehaviorTree.BehaviorTree behaviors;

        public KickBall(BehaviorTree.BehaviorTree behaviorTree)
        {
            behaviors = behaviorTree;
        }

        public IPlay DeepCopy()
        {
            throw new NotImplementedException();
        }

        public double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            throw new NotImplementedException();
        }

        public void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            throw new NotImplementedException();
        }

        public bool isViablePlay(Player player, Ball ball, Team ownTeam, Team otherTeam)
        {
            throw new NotImplementedException();
        }
    }
}
