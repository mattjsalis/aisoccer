﻿using GameElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace Actions
{
    public class ShadowClosestOpposingPlayer : IPlay
    {

        public Vect3 playerPosToShadow = null;
        public Vect3 playerVelToShadow = null;

        public IPlay DeepCopy()
        {
            ShadowClosestOpposingPlayer play = new ShadowClosestOpposingPlayer();
            play.playerPosToShadow = playerPosToShadow?.Copy();
            play.playerVelToShadow = playerVelToShadow?.Copy();
            return play;
        }

        public double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            if (SoccerMatch.IsRandomPlay)
            {
                return Probability.RandomBetween(0.0, 1.0);
            }
            return Probability.RandomBetween(0.0, 1.0);
        }
        private static readonly int numPlayersToSwarmOpposing = 2;
        public void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            Vect3 playerPos = player.Position;
            IOrderedEnumerable<Player> playersOrderedByDistance = 
                otherTeam.Players.OrderBy(p => p.Position.DistanceToVect(playerPos));
            bool foundPlayerToShadow = false;
            int iPlyr = 0;
            Player closestOpposingPlayer = null;

            while (!foundPlayerToShadow && iPlyr < playersOrderedByDistance.Count() - 1)
            {
                
                closestOpposingPlayer = playersOrderedByDistance.ElementAt(iPlyr);
                double thisPlayersDistance = player.Position.DistanceToVect(closestOpposingPlayer.Position);
                int numberPlayersCloser = ownTeam.GetPositions()
                    .Select(p => p.DistanceToVect(closestOpposingPlayer.Position))
                    .Where(d => d < thisPlayersDistance)
                    .Count();
                if(numberPlayersCloser + 1 < numPlayersToSwarmOpposing)
                {
                    foundPlayerToShadow = true;
                    break;
                }
                iPlyr++;
            }
            if (!foundPlayerToShadow)
            {
                closestOpposingPlayer = playersOrderedByDistance.ElementAt(0);
            }

            playerPosToShadow = closestOpposingPlayer.Position;
            playerVelToShadow = closestOpposingPlayer.Velocity;

            Vect3 newVel = InterceptCalculation.GetMinTimeInterceptVelocityVector(player.MaxRunSpeed,
                player.Position, player.Velocity, closestOpposingPlayer.Position, closestOpposingPlayer.Velocity);
            player.Velocity = newVel;
        }

        public bool isViablePlay(Player player, Ball ball, Team ownTeam, Team otherTeam)
        {
            return otherTeam.HasPosession;
        }
    }
}
