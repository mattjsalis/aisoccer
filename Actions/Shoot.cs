﻿using System;
using System.Collections.Generic;
using System.Text;
using Utility;
using System.Linq;
using NeuralNet;
using GameElements;

namespace Actions
{
    /// <summary>
    /// Class represents a Shoot on goal play
    /// </summary>
    public class Shoot : IPlay
    {

        public double distanceToGoal = 0.0;
        public int numPlayersInGoalWedge = 0;

        public override bool Equals(object obj)
        {
            bool isShoot = obj is Shoot;
            if (!isShoot) { return false; }
            Shoot other = (Shoot)obj;
            return other.distanceToGoal == distanceToGoal 
                && other.numPlayersInGoalWedge == numPlayersInGoalWedge;

        }
        public Func<double, double> ShootEvaluationFunc { get; set; } = (probSuccess) =>
        {
            return probSuccess * 1.25;
        };

        /// <summary>
        /// Neural net to predict the probability of a successful shot
        /// </summary>
        private ArtificialNeuralNet neuralNet = new ArtificialNeuralNet(2, 1, new int[] { 3, 3 },-5, 5, -5, 5)
                                .SetOutputLayerActivationFunction(ActivationFunctionType.SIGMOID);

        /// <summary>
        /// Returns a "score" that is used to decide if shooting id the best option
        /// </summary>
        /// <param name="scoreFunc"></param>
        /// <param name="gameData"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            
            Tuple<Vect3, Vect3> vectsToGoalPosts = SoccerMatch.Walls.Value.GetVectsFromPlayerToGoalPosts(player);
            int numOpposingPlayersInGoalWedge =
                            otherTeam
                            .Players
                            .Where(plyr =>
                                        GeometryUtility.IsInWedge(player.Position,
                                                        vectsToGoalPosts.Item1,
                                                        vectsToGoalPosts.Item2,
                                                        plyr.Position)
                                        )
                            .Count();
            double distToGoal = 0.5 * (vectsToGoalPosts.Item1.Magnitude + vectsToGoalPosts.Item2.Magnitude);
            distanceToGoal = distToGoal;
            numPlayersInGoalWedge = numOpposingPlayersInGoalWedge;
            if (SoccerMatch.IsRandomPlay)
            {
                return Probability.RandomBetween(0.0, 1.0);
            }
            double probabilityOfSuccessfulPass = neuralNet.CalculateNeuralNet(numOpposingPlayersInGoalWedge, distToGoal)[0];
            return ShootEvaluationFunc.Invoke(probabilityOfSuccessfulPass);
        }

        /// <summary>
        /// Shoot at the center of the goal for now
        /// </summary>
        /// <param name="gameData"></param>
        /// <param name="player"></param>
        public void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            ball.Velocity = SoccerMatch.Walls.Value.GetVectToGoalCenter(player).UnitVector.Times(player.MaxKickSpeed);
            player.PositionAtKick = player.Position;
            player.HasPosession = false;
        }

        public IPlay DeepCopy()
        {
            Shoot play = new Shoot();
            play.distanceToGoal = distanceToGoal;
            play.numPlayersInGoalWedge = numPlayersInGoalWedge;
            return play;
        }

        public bool isViablePlay(Player player,Ball ball, Team ownTeam, Team otherTeam)
        {
            return player.HasPosession;
        }
    }
}
