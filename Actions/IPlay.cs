﻿using GameElements;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace Actions
{
    /// <summary>
    /// Interface for all plays that a player may choose
    /// </summary>
    public interface IPlay
    {
        /// <summary>
        /// Determines if this play type is allowed for the player
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        bool isViablePlay(Player player, Ball ball, Team ownTeam, Team otherTeam);
        /// <summary>
        /// Executes the play based on the player and other information in the game
        /// </summary>
        /// <param name="player"></param>
        /// <param name="playersTeam"></param>
        /// <param name="opposingTeam"></param>
        /// <param name="ball"></param>
        void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player);

        /// <summary>
        /// Method that estimates the value of this play relative to
        /// other plays
        /// </summary>
        /// <returns></returns>
        double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player);

        IPlay DeepCopy();

        
    }
}
