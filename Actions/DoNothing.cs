﻿using GameElements;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace Actions
{
    public class DoNothing : IPlay
    {


        public IPlay DeepCopy()
        {
            return new DoNothing();
        }

        public double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            if (SoccerMatch.IsRandomPlay)
            {
                return Probability.RandomBetween(0.0, 1.0);
            }
            return 0.1;
        }

        public void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            //Do nothing!
        }

        public bool isViablePlay(Player player, Ball ball, Team ownTeam, Team otherTeam)
        {
            //Doing nothing is always a choice unless the player has possesion
            return !player.HasPosession;
        }
    }
}
