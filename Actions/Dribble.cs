﻿using GameElements;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace Actions
{
    public class Dribble : IPlay
    {

        public Vect3 relPosition = null;
        const double DEG_INCREMENT_TO_CONSIDER = 5.0;
        public IPlay DeepCopy()
        {
            Dribble dribble = new Dribble();
            dribble.relPosition = relPosition?.Copy();
            return dribble;
        }

        public double estimatePlayScore(Team ownTeam, Team otherTeam, Ball ball, Player player)
        {
            double speed = player.Velocity.Magnitude;
            if (speed == 0.0)
            {
                speed = player.MaxRunSpeed;
            }
            double distanceToNextStep = SoccerMatch.TIME_STEP* speed;
            Pair<double, Vect3> potentialAndRelPosition = GetRelativePositionForDribble(otherTeam.GetPositions(),
                player, distanceToNextStep, DEG_INCREMENT_TO_CONSIDER);
            double score = 1 / potentialAndRelPosition.First;
            relPosition = potentialAndRelPosition.Second;
            if (SoccerMatch.IsRandomPlay)
            {
                return Probability.RandomBetween(0.0, 1.0);
            }
            return score > 1.0 ? 1.0 : score;
        }

        public void executePlay(ref Team ownTeam, Team otherTeam, ref Ball ball, ref Player player)
        {
            player.Position = player.Position.Plus(relPosition);
        }

        public static Pair<double, Vect3> GetRelativePositionForDribble(List<Vect3> otherTeamPositions,
            Player player, double distanceToNextPosition, double degIncrementToConsider)
        {
            Vect3 toGoal = SoccerMatch.Walls.Value.GetVectToGoalCenter(player).UnitVector;

            StealPotential allStealPotentials = new StealPotential(otherTeamPositions);
            //For now, simply select the best position with angles from 0 to 359 deg
            Vect3 bestPos = null;
            double deg = 0; int i = 0;
            double minPotential = Double.MaxValue;
            while (deg < 360.0)
            {
                Vect3 potentialPos = new Vect3(distanceToNextPosition, Angle.FromDegrees(deg));
                double toGoalAngleConsideration = Math.Exp(-potentialPos.RelativeAngleBetweenVectors(toGoal).Radians);
                double potential = allStealPotentials.getPotential(player.Position.Plus(potentialPos)) 
                    - toGoalAngleConsideration;
                if (potential < minPotential)
                {
                    minPotential = potential;
                    bestPos = potentialPos;
                }
                deg += degIncrementToConsider;
                i++;
            }
            minPotential = minPotential <= 0.0 ? 1E-3 : minPotential;
            return new Pair<double, Vect3>(minPotential, bestPos);
        }

        public bool isViablePlay(Player player,Ball ball, Team ownTeam, Team otherTeam)
        {
            return player.HasPosession;
        }
    }
}
