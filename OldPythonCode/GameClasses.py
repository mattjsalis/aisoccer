# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 09:04:00 2019

@author: Matt
"""
import TeamClasses
import matplotlib
import numpy as np
import datetime
import imageio
import os
import pandas as pd
import AISoccerNeuralNet as ai

dtTime = datetime.datetime
plt = matplotlib.pyplot
#LAMBDA expressions: Should probably just create functions that can be shared between scripts
dist = lambda pos1,pos2 : np.sqrt(pow((pos2[0] - pos1[0]),2) + pow((pos2[1] - pos1[1]),2))
norm = lambda aList : np.linalg.norm(aList)
multByScalar = lambda aList,scalar : list(map(lambda x : x*scalar,aList))
addLists = lambda list1,list2 : list(map(lambda x1,x2 : float(x1)+float(x2),list1,list2))
drawBox = lambda xc,yc,width,height,edgeColor,faceColor,ax : ax.add_patch(matplotlib.patches.Rectangle((xc-width/2,yc-height/2),width,height,0,edgecolor=edgeColor,facecolor=faceColor))
drawCircle  = lambda xc,yc,radius,edgeColor,faceColor,ax : ax.add_patch(matplotlib.patches.Circle((xc,yc),radius,edgecolor=edgeColor,facecolor=faceColor))
getAz = lambda vel : np.arctan(vel[1]/vel[0])


class Game:
    def __init__(self,gameTime,dt,inputScaler,encoder,scorePredictor):
        self.gameTime = gameTime
        self.dt = dt
        self.teams = []
        self.gameBall = Ball()
        self.teamWithBall = ""
        self.redScore = 0
        self.otherScore = 0
        #Neural net components:
        self.inputScaler = inputScaler #this components scales the input to the net
        self.scorePredictor = scorePredictor #this is the neural net to be used by the players
        self.encoder = encoder
    def start(self,boolRecord,probRandomMove):
        dt = self.dt
        ball = self.gameBall
        ball.loc = self.teams[0].players[0].loc
        ballAtStartOfPosession = ball.loc
        redScoreAtStartOfPosession = 0
        otherScoreAtStartOfPosession = 0
        gameData = {}
        time = 0
        while time < self.gameTime:
            for team in self.teams:
                for player in team.players:
                    diceRollRand = np.random.rand(1)[0]
                    if probRandomMove > diceRollRand:
                        player.doSomethingRandom(ball,dt)
                    else:
                        player.useNeuralNet(self.inputScaler,self.encoder,self.scorePredictor,ball,self.teams,dt)
            if ball.underPossesion == False:
                #TODO make sure that if a player dribbles into the goal with the ball that the score is counted
                boolRedScore,boolOtherScore = ball.moveBall(ball.vel,dt)
                if boolRedScore == True:
                    self.redScore = self.redScore + 1
                if boolOtherScore == True:
                    self.otherScore = self.otherScore + 1
            #get team currently in posession
            teamWithLastPosession = self.teamWithBall
            self.skirmish()
            gameData = self.recordData(time,gameData)
            if self.teamWithBall != teamWithLastPosession and self.teamWithBall != "":
                print("Ball stolen!")
                gameData = self.scorePosession(gameData,redScoreAtStartOfPosession,otherScoreAtStartOfPosession,ballAtStartOfPosession,5)
                redScoreAtStartOfPosession = self.redScore
                otherScoreAtStartOfPosession = self.otherScore
                ballAtStartOfPosession = self.gameBall.loc
                
            print("Score is Red: " + str(self.redScore) + " Other: " + str(self.otherScore))
            print("Time: " + str(time) + "sec : Ball at " + str(self.gameBall.loc))
            self.getFrame(boolRecord)
            time = time + self.dt
        gameData = self.scorePosession(gameData,redScoreAtStartOfPosession,otherScoreAtStartOfPosession,ballAtStartOfPosession,5)
        gameData = pd.DataFrame(data=gameData)
        return gameData
    def scorePosession(self,gameData,redScoreAtStartOfPosession,otherScoreAtStartOfPosession,ballAtStartOfPosession,numActionsToScore):
        #get defensive teams score
        offScore = 0
        defScore = 0
        for team in self.teams:
            if team.hasBall == True: #team that just obtained ball must have been on defense
                if "red" in team.side.lower():
                    distAtBeginning = dist(ballAtStartOfPosession,[-50,0])
                    distAtEnd = dist(self.gameBall.loc,[-50,0])
                    defScore = (distAtEnd - distAtBeginning)/10
                else:
                    distAtBeginning = dist(ballAtStartOfPosession,[50,0])
                    distAtEnd = dist(self.gameBall.loc,[50,0])
                    defScore = (distAtEnd - distAtBeginning)/10
            if team.hasBall == False:
                if "red" in team.side.lower():
                    distAtBeginning = dist(ballAtStartOfPosession,[50,0])
                    distAtEnd = dist(self.gameBall.loc,[50,0])
                    offScore = (distAtBeginning - distAtEnd)/10 + 50*(self.redScore - redScoreAtStartOfPosession)
                else:
                    distAtBeginning = dist(ballAtStartOfPosession,[-50,0])
                    distAtEnd = dist(self.gameBall.loc,[-50,0])
                    offScore = (distAtBeginning - distAtEnd)/10 + 50*(self.otherScore - otherScoreAtStartOfPosession)
        
        numActionsToKeep = numActionsToScore*(len(self.teams[0].players) + len(self.teams[1].players))
        actionCount = 0
        numMovesAdded = len(gameData["action"]) - len(gameData["possesion_score"])
        #reduce stored data down
        for i in range(len(gameData["possesion_score"]),len(gameData["action"])):
            actionCount = actionCount + 1
            if (numMovesAdded - actionCount) > numActionsToKeep:
                for key in gameData.keys():
                    if len(gameData[key]) > 0 and key != "possesion_score":
                        try:
                            gameData[key].pop(len(gameData["possesion_score"]))
                        except:
                            print("error")
        #add scores to remaining data elements
        for i in range(len(gameData["possesion_score"]),len(gameData["action"])):
                if gameData["team_has_possesion"][i] == 1:
                    gameData["possesion_score"].append(offScore)
                else:
                    gameData["possesion_score"].append(defScore)
        return gameData
    
    def recordData(self,time,gameData):
        if time == 0:
            gameData = {}
            gameData["action"] = []
            gameData["team"] = []
            gameData["team_has_possesion"] = []
            gameData["player_has_possesion"] = []
            for aTeam in self.teams:
                for player in aTeam.players:
                    indStr = str(aTeam.players.index(player))
                    gameData[aTeam.side + "_pos_x_" + indStr] = []
                    gameData[aTeam.side + "_pos_y_" + indStr] = []
                    gameData[aTeam.side + "_vel_x_" + indStr] = []
                    gameData[aTeam.side + "_vel_y_" + indStr] = []
            gameData["player_pos_x"] = []
            gameData["player_pos_y"] = []
            gameData["player_vel_x"] = []
            gameData["player_vel_y"] = []
            gameData["ball_pos_x"] = []
            gameData["ball_pos_y"] = []
            gameData["ball_vel_x"] = []
            gameData["ball_vel_y"] = []
            gameData["dist_to_goal"] = []
            gameData["dist_to_own_goal"] = []
            gameData["move_to_x"] = []
            gameData["move_to_y"] = []
            gameData["move_to_vel"] = []
            gameData["vel_kick"] = []
            gameData["az_kick"] = []

            gameData["possesion_score"] = []

        else:
            for team in self.teams:
                for player in team.players:
                    for team1 in self.teams:
                        for player1 in team1.players:
                            indStr = str(team1.players.index(player1))
                            gameData[team1.side + "_pos_x_" + indStr].append(player1.loc[0])
                            gameData[team1.side + "_pos_y_" + indStr].append(player1.loc[1])
                            gameData[team1.side + "_vel_x_" + indStr].append(player1.vel[0])
                            gameData[team1.side + "_vel_y_" + indStr].append(player1.vel[1])

                    if "red" in team.side.lower():
                        gameData["team"].append(1)
                    else:
                        gameData["team"].append(0)
                        
                    if player.hasBall == True:
                        gameData["player_has_possesion"].append(1)
                    else:
                        gameData["player_has_possesion"].append(0)
                        
                    if team.hasBall == True:
                        gameData["team_has_possesion"].append(1)
                    else:
                        gameData["team_has_possesion"].append(0)
                    gameData["ball_pos_x"].append(self.gameBall.loc[0])
                    gameData["ball_pos_y"].append(self.gameBall.loc[1])
                    gameData["ball_vel_x"].append(self.gameBall.vel[0])
                    gameData["ball_vel_y"].append(self.gameBall.vel[1])
                    gameData["player_pos_x"].append(player.loc[0])
                    gameData["player_pos_y"].append(player.loc[1])
                    gameData["player_vel_x"].append(player.vel[0])
                    gameData["player_vel_y"].append(player.vel[1])
                    gameData["action"].append(player.lastAction)
                    gameData["move_to_vel"].append(norm(player.vel))
                    if player.lastAction == 4:
                        gameData["move_to_x"].append(player.targetSpot[0])
                        gameData["move_to_y"].append(player.targetSpot[1])
                        gameData["vel_kick"].append(0)
                        gameData["az_kick"].append(0)
                    elif player.lastAction == 1:
                        gameData["move_to_x"].append(0)
                        gameData["move_to_y"].append(0)
                        gameData["vel_kick"].append(norm(self.gameBall.vel))
                        if norm(self.gameBall.vel[0]) != 0:
                            gameData["az_kick"].append(getAz(self.gameBall.vel))
                        else:
                            gameData["az_kick"].append(0)
                    else:
                        gameData["move_to_x"].append(0)
                        gameData["move_to_y"].append(0)
                        gameData["vel_kick"].append(0)
                        gameData["az_kick"].append(0)
                    
                    if "red" in team.side.lower():
                        gameData["dist_to_goal"].append(dist(player.loc,[50,0]))
                        gameData["dist_to_own_goal"].append(dist(player.loc,[-50,0]))
                    else:
                        gameData["dist_to_goal"].append(dist(player.loc,[-50,0]))
                        gameData["dist_to_own_goal"].append(dist(player.loc,[50,0]))
                  
        return gameData                
    def addTeam(self,numDefenders,numAttackers,side):
        self.teams.append(TeamClasses.Team(numDefenders,numAttackers,side))
    def skirmish(self):
        playersInSkirmish = []
        for team in self.teams:
            for player in team.players:
                if dist(player.loc,self.gameBall.loc) < player.trapRadius:
                    playersInSkirmish.append(player)
                    break
        if len(playersInSkirmish) != 0:
            diceRoll = np.random.rand(1)[0]
            playerRolls = list(np.random.rand(len(playersInSkirmish)))
            bestDist = 1000
            i = 0
            for player in playersInSkirmish:
                distToRoll = dist([playerRolls[i],0],[diceRoll,0])
                if distToRoll < bestDist:
                    skirmishWinner = i
                    bestDist = distToRoll
                i = i + 1
            
            for i in range(0,len(playersInSkirmish)):
                if i == skirmishWinner:
                    playersInSkirmish[i].hasBall = True
                    for team in self.teams:
                        if playersInSkirmish[i] in team.players:
                            self.teamWithBall = team.side
                            team.hasBall = True
                else:
                    playersInSkirmish[i].hasBall = False
            self.gameBall.loc = playersInSkirmish[skirmishWinner].loc
            self.gameBall.vel = playersInSkirmish[skirmishWinner].vel
            self.gameBall.underPossesion = True
        else:
            self.gameBall.underPossesion = False

        return 
            
    def updateLocations(self):
        return 0
    def getFrame(self,boolRecord):
        fig = plt.figure(1)
        plt.clf()
        ax = fig.add_subplot(111)
        drawBox(0,0,100,50,(0,0,0),(0,0.5,0),ax) #field
        drawBox(-44,0,12,30,(1,1,1),(0,0.5,0),ax) # redPenaltyArea
        drawBox(44,0,12,30,(1,1,1),(0,0.5,0),ax) # otherPenaltyArea
        drawBox(-47,0,6,15,(1,1,1),(0,0.5,0),ax) # redGoalArea
        drawBox(47,0,6,15,(1,1,1),(0,0.5,0),ax) # otherGoalArea
        drawBox(-51,0,2,7,(0,0,0),(1,1,1),ax) # redGoal
        drawBox(51,0,2,7,(0,0,0),(1,1,1),ax) # redGoal
        drawCircle(0,0,9,(1,1,1),(0,0.5,0),ax) # centerCircle
        ax.set_xlim([-55,55])
        ax.set_ylim([-30,30])
        ax.set_aspect("equal")
        plt.plot([0,0],[-25,25],color='white')
        for team in self.teams:
            for player in team.players:
                drawCircle(player.loc[0],player.loc[1],0.75,(0,0,0),team.side,ax)
        drawCircle(self.gameBall.loc[0],self.gameBall.loc[1],0.5,(0,0,0),(1,1,1),ax)
        if boolRecord == True:
            dateExt = str(dtTime.now()).replace(" ","_").replace(":","")[:-7]
            plt.savefig("AISoccer" + dateExt + ".png")
    def makeGif(self,folder,idString):
        files = os.listdir(folder)
        images = []
        for aFile in files:
            if idString in aFile:
                images.append(imageio.imread(aFile))
                os.remove(aFile)
        dateExt = str(dtTime.now()).replace(" ","_").replace(":","")[:-7]
        imageio.mimsave("AISoccerMovie" + dateExt + ".gif",images,duration=self.dt)

        
unitVect = lambda vect : [vect[0]/(norm(vect)+0.00001),vect[1]/(norm(vect)+0.00001)]       

class Ball:
    def __init__(self):
        self.loc = [0,0]
        self.vel = [0,0]
        self.maxVel = 50
        self.wallBoundaries = [50,25] #min/max x, min/max y
        self.goalWidth = 7 #width of goal
        self.underPossesion = False
    def moveBall(self,vel,dt):
        #account for ball bouncing here
        projLoc,boolRedScore,boolOtherScore = self.checkBoundaries(self.loc,vel,dt)
        if self.underPossesion == False and norm(self.vel) != 0:
            unitVel = unitVect(self.vel)
            vmag = norm(self.vel)
            self.vel = multByScalar(unitVel,vmag*(1 - 0.2*dt)) #account for friction
        return boolRedScore,boolOtherScore
    def checkBoundaries(self,loc,vel,dt):
        #create booleans for scoring
        boolRedScore = False
        boolOtherScore = False
        projLoc = addLists(loc,multByScalar(vel,dt))
        #first check to see if team has scored
        while abs(projLoc[0]) > self.wallBoundaries[0] or abs(projLoc[1]) > self.wallBoundaries[1]:
            if abs(projLoc[0]) > self.wallBoundaries[0] and abs(projLoc[1]) < self.goalWidth/2:
                if np.sign(projLoc[0]) == -1:
                    print("Other team scored!")
                    boolOtherScore = True
                    projLoc = [0,0]
                    vel = [0,0]
                else:
                    print("Red team scored!")
                    boolRedScore = True
                    projLoc = [0,0]
                    vel = [0,0]
            else:
                timeToXIntercept = abs(self.wallBoundaries[0] - abs(loc[0]))/abs(vel[0]+0.0001)
                timeToYIntercept = abs(self.wallBoundaries[1] - abs(loc[1]))/abs(vel[1]+0.0001)
                #logic to handle if ball crosses both boundaries
                if abs(projLoc[0]) > self.wallBoundaries[0] and abs(projLoc[1]) > self.wallBoundaries[1]:
                    
                    if timeToXIntercept < timeToYIntercept:
                        #ball will hit back wall before side wall
                        dt = dt - timeToXIntercept
                        yAtIntercept = timeToXIntercept*vel[1] + loc[1]
                        vel[0] = -vel[0]
                        newX = np.sign(projLoc[0])*self.wallBoundaries[0] + vel[0]*dt
                        newY = yAtIntercept + vel[1]*dt
                        loc = [np.sign(projLoc[0])*self.wallBoundaries[0],yAtIntercept]
                    else:
                        #ball will hit side wall before back wall
                        dt = dt - timeToYIntercept
                        xAtIntercept = timeToYIntercept*vel[0] + loc[0]
                        vel[1] = -vel[1]
                        newY = np.sign(projLoc[1])*self.wallBoundaries[1] + vel[1]*dt
                        newX = xAtIntercept + vel[0]*dt
                        loc = [xAtIntercept,np.sign(projLoc[1])*self.wallBoundaries[1]]
                elif abs(projLoc[0]) > self.wallBoundaries[0]:
                    dt = dt - timeToXIntercept
                    yAtIntercept = timeToXIntercept*vel[1] + loc[1]
                    vel[0] = -vel[0]
                    newX = np.sign(projLoc[0])*self.wallBoundaries[0] + vel[0]*dt
                    newY = yAtIntercept + vel[1]*dt
                    loc = [np.sign(projLoc[0])*self.wallBoundaries[0],yAtIntercept]
                elif abs(projLoc[1]) > self.wallBoundaries[1]:
                    dt = dt - timeToYIntercept
                    xAtIntercept = timeToYIntercept*vel[0] + loc[0]
                    vel[1] = -vel[1]
                    newY = np.sign(projLoc[1])*self.wallBoundaries[1] + vel[1]*dt
                    newX = xAtIntercept + vel[0]*dt
                    loc = [xAtIntercept,np.sign(projLoc[1])*self.wallBoundaries[1]]
                #update projected location   
                projLoc = [newX,newY]
        self.loc = projLoc
        self.vel = vel
        return projLoc,boolRedScore,boolOtherScore
       
#redTeam = TeamClasses.Team(4,3,"red")
#blueTeam = TeamClasses.Team(4,3,"blue")
#teams = [redTeam,blueTeam]
##player = redTeam.players[0]
##player.hasBall = True
##options = player.useNeuralNet(sc_x,encoder,scorePredictor,Ball(),teams,0.1)
#game = Game(30,0.1,[],[],[])
#game.addTeam(4,3,"red")
#game.addTeam(4,3,"blue")      
#newGameData = game.start(False,1)
sc_x = [];encoder = [];scorePredictor = []
for gencnt in range(0,100):
    print("Generation count is " + str(gencnt))
    for gamecnt in range(0,100):
        print("Game count is " + str(gamecnt))
        game = Game(30,0.1,sc_x,encoder,scorePredictor)
        game.addTeam(4,3,"red")
        game.addTeam(4,3,"blue")
        if gencnt == 0:
            newGameData = game.start(False,1)
        elif gamecnt == 99:
            newGameData = game.start(True,0)
            game.makeGif(os.getcwd(),'AISoccer2019')
        else:
            newGameData = game.start(False,0.1)
            
        if gencnt == 0 and gamecnt == 0:
            gameData = newGameData
        else:
            gameData = gameData.append(newGameData,ignore_index=True)
            
    yDataFrame = pd.DataFrame(gameData["possesion_score"])
    Xcols = gameData.columns.tolist()
    Xcols.pop(gameData.columns.tolist().index("possesion_score"))
    XDataFrame = pd.DataFrame(gameData[Xcols])
    sc_x,sc_y,X_test,encoder,scorePredictor = ai.TrainNNFromGameData(XDataFrame,yDataFrame)
    