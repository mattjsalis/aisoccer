# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 08:04:31 2019

@author: Matt
"""
import numpy as np
import pandas as pd
import scipy.special as ss
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

erf = lambda x : ss.erf(x)
erfinv = lambda x : ss.erfinv(x)
randNorm = lambda mu,sigma : np.random.normal(mu,sigma,1)[0]
dist = lambda pos1,pos2 : np.sqrt(pow((pos2[0] - pos1[0]),2) + pow((pos2[1] - pos1[1]),2))
norm = lambda aList : np.linalg.norm(aList)
multByScalar = lambda aList,scalar : list(map(lambda x : x*scalar,aList))
addLists = lambda list1,list2 : list(np.array(list1) + np.array(list2))
unitVect = lambda vect : [vect[0]/(norm(vect)+1e-9),vect[1]/(norm(vect)+1e-9)]
randBetween = lambda minVal,maxVal : minVal + (maxVal - minVal)*np.random.rand(1)[0]
class Player:
    def __init__(self,role):
        self.role = role
        if "attacker" in role.lower():
            self.Pkick = 0.8
        elif "defender" in role.lower():
            self.Pkick = 0.9
        elif "goalie" in role.lower():
            self.Pkick = 0.9
        
        #Transient parameters
        self.loc = [0,0] #xLoc,yLoc of player
        self.vel = [0,0]
        self.hasBall = False
        self.lastAction = 0
        #Static parameters
        self.sigmaKick = 2*np.pi/180/(np.sqrt(2)*erfinv(self.Pkick))
        self.maxVel = 9
        self.wallBoundaries = [50,25]
        self.trapRadius = 1.0
        self.targetSpot = self.loc
    def kickBall(self,magVel,azToKickTo,ballObj,dt):
        if self.hasBall == True:
           if magVel > ballObj.maxVel:
               magVel = ballObj.maxVel
           kickAz = azToKickTo + randNorm(0,self.sigmaKick)
           vel = [magVel*np.cos(kickAz),magVel*np.sin(kickAz)]
           ballObj.moveBall(vel,dt)
        ballObj.underPossesion = False  
        self.hasBall = False
    def stopInPlace(self,ball):
       self.vel = [0,0]
       if self.hasBall == True:
           ball.loc = self.loc
    def continueOnPath(self,ball,dt):
       projLoc = [self.loc[0] + self.vel[0]*dt,self.loc[1] + self.vel[1]*dt]
       if abs(projLoc[0]) > self.wallBoundaries[0]:
           self.loc[0] = np.sign(projLoc[0])*self.wallBoundaries[0]
       else:
           self.loc[0] = projLoc[0]
       if abs(projLoc[1]) > self.wallBoundaries[1]:
           self.loc[1] = np.sign(projLoc[1])*self.wallBoundaries[1]  
       else:
           self.loc[1] = projLoc[1]
       if self.hasBall == True:
           ball.loc = self.loc
    def runToSpot(self,ball,spot,magVel,dt):
        #snap to spot if dist to spot is less than distance traveled in given time
       if magVel > self.maxVel:
           magVel = self.maxVel
       vect = unitVect([spot[0] - self.loc[0],spot[1] - self.loc[1]])
       vel = multByScalar(vect,magVel)
       self.vel = vel
       projLoc = [self.loc[0] + vel[0]*dt,self.loc[1] + vel[1]*dt]
       if abs(projLoc[0]) > self.wallBoundaries[0]:
           self.loc[0] = np.sign(projLoc[0])*self.wallBoundaries[0]
       else:
           self.loc[0] = projLoc[0]
       if abs(projLoc[1]) > self.wallBoundaries[1]:
           self.loc[1] = np.sign(projLoc[1])*self.wallBoundaries[1]  
       else:
           self.loc[1] = projLoc[1]
       if self.hasBall == True:
           ball.loc = self.loc
    def doSomethingRandom(self,ball,dt):
       diceRoll = np.random.randint(1,5)
       if diceRoll == 1 and self.hasBall == True:
           magVel = np.random.rand(1)[0]*ball.maxVel
           azToKickBall = np.random.rand(1)[0]*2*np.pi
           self.kickBall(magVel,azToKickBall,ball,dt)
       elif diceRoll == 2:
           self.stopInPlace(ball)
       elif diceRoll == 3:
           self.continueOnPath(ball,dt)
       elif diceRoll == 4:
           spot = [randBetween(-50,50),randBetween(-25,25)]
           magVel = randBetween(0,self.maxVel) 
           self.runToSpot(ball,spot,magVel,dt)
       else:
           self.continueOnPath(ball,dt)
           diceRoll = 3
       self.lastAction = diceRoll
       return diceRoll
    def useNeuralNet(self,inputScaler,encoder,scorePredictor,ball,teams,dt):
       #This method should create a list of options for the player to do and then evaluate each option
       #using the provided neural net (scorePredictor)
       options = {}
       options["player_pos_x"] = [self.loc[0]]; options["player_pos_y"] = [self.loc[1]]
       options["player_vel_x"] = [self.vel[0]]; options["player_vel_y"] = [self.vel[1]]
       options["ball_pos_x"] = [ball.loc[0]];options["ball_pos_y"] = [ball.loc[1]]
       options["ball_vel_x"] = [ball.vel[0]];options["ball_vel_y"] = [ball.vel[1]]
       if self.hasBall == True:
           options["player_has_possesion"] = [1]
       else:
           options["player_has_possesion"] = [0]
       for team in teams:
           if self in team.players:
               if "red" in team.side.lower():
                   options["dist_to_own_goal"] = [dist(self.loc,[-50,0])]
                   options["dist_to_goal"] = [dist(self.loc,[50,0])]
                   options["team"] = [1]
               else:
                   options["dist_to_own_goal"] = [dist(self.loc,[50,0])]
                   options["dist_to_goal"] = [dist(self.loc,[-50,0])]
                   options["team"] = [0]
                   
               if team.hasBall == True:
                   options["team_has_possesion"] = [1]
               else:
                   options["team_has_possesion"] = [0]
           
           for player in team.players:
               indStr = str(team.players.index(player))
               options[team.side + "_pos_x_" + indStr] = [player.loc[0]]
               options[team.side + "_pos_y_" + indStr] = [player.loc[1]]
               options[team.side + "_vel_x_" + indStr] = [player.vel[0]]
               options[team.side + "_vel_y_" + indStr] = [player.vel[1]]
       #create list of static keys (things that don't change across the available options)
       staticKeys = list(options.keys())
       #create options
       #first option is always to continueOnPath
       options["action"] = [3]
       #all actions must either be zero or a value so fill in the move_to and kick commands as zeros
       options["move_to_x"] = [0]; options["move_to_y"] = [0]; options["move_to_vel"] = [0];
       options["az_kick"] = [0]; options["vel_kick"] = [0];
       #second option is to stop in place
       options["action"].append(2)
       for key in staticKeys:
           options[key].append(options[key][0])
       #all actions must either be zero or a value so fill in the move_to and kick commands as zeros
       options["move_to_x"].append(0); options["move_to_y"].append(0); options["move_to_vel"].append(0);
       options["az_kick"].append(0); options["vel_kick"].append(0);
       #only allow the player to have kick options if it has the ball
       if self.hasBall == True:
           prospectiveAzimuth = np.linspace(0,2*np.pi,num=30)
           prospectiveVel = np.linspace(0.1,ball.maxVel,num=20)
           for az in prospectiveAzimuth:
               for vel in prospectiveVel:
                   for key in staticKeys:
                       options[key].append(options[key][0])
                   options["action"].append(1)
                   options["az_kick"].append(az)
                   options["vel_kick"].append(vel)
                   options["move_to_x"].append(0); options["move_to_y"].append(0);
                   options["move_to_vel"].append(0);
       else:
           for key in staticKeys:
               options[key].append(options[key][0])
           options["action"].append(1)
           options["az_kick"].append(0)
           options["vel_kick"].append(0)
           options["move_to_x"].append(0); options["move_to_y"].append(0);
           options["move_to_vel"].append(0);
       #move-to option
       prospectiveX = list(np.linspace(self.loc[0] - 10,self.loc[0] + 10,num=5))
       prospectiveY = list(np.linspace(self.loc[1] - 10,self.loc[1] + 10,num=5))
       prospectiveVel = list(np.linspace(0.1,self.maxVel,num=10))
       for x in prospectiveX:
           for y in prospectiveY:
               for vel in prospectiveVel:
                   for key in staticKeys:
                       options[key].append(options[key][0])
                   options["action"].append(4)
                   options["az_kick"].append(0)
                   options["vel_kick"].append(0)
                   options["move_to_x"].append(x); options["move_to_y"].append(y);
                   options["move_to_vel"].append(vel);
       options = pd.DataFrame(data=options)
       sortedCols = list(np.sort(options.columns.tolist()))
       options = pd.DataFrame(data=options[sortedCols])
       X = options.iloc[:, :].values
       X = encoder.transform(X).toarray()
       X = X[:,1:]
       X = inputScaler.transform(X)
       scores = list(scorePredictor.predict(X))
       maxScore = max(scores)
       indMaxScore = scores.index(maxScore)
       bestOption = options.iloc[indMaxScore,:]
       if bestOption["action"] == 1:
           if self.hasBall == True:
               self.kickBall(bestOption["vel_kick"],bestOption["az_kick"],ball,dt)
           else:
               self.continueOnPath(ball,dt)
               return 3
       elif bestOption["action"] == 2:
           self.stopInPlace(ball)
       elif bestOption["action"] ==3:
           self.continueOnPath(ball,dt)
       elif bestOption["action"] == 4:
           spot = [bestOption["move_to_x"],bestOption["move_to_y"]]
           self.runToSpot(ball,spot,bestOption["move_to_vel"],dt)
       return bestOption["action"]
       
class Team:
    def __init__(self,numDefenders,numAttackers,side):
        self.players = []
        self.side = side
        self.hasBall = False
        defGap = 50/(numDefenders+1)
        offGap = 50/(numAttackers+1)
        for i in range(0,numDefenders):
            newPlayer = Player("Defender")
            if "red" in side.lower():
                newPlayer.az = 0
                newPlayer.loc = [-30,-25 + (i+1)*(defGap)]#assume a field 100 (x)  by 50 (y) 
            else:
                newPlayer.az = 180
                newPlayer.loc = [30,-25 + (i+1)*(defGap)]#assume a field 100 (x)  by 50 (y) 
            self.players.append(newPlayer)
        for i in range(0,numAttackers):
            newPlayer = Player("Attacker")
            if "red" in side.lower():
                newPlayer.az = 0
                newPlayer.loc = [-10,-25 + (i+1)*(offGap)]#assume a field 100 (x)  by 50 (y) 
            else:
                newPlayer.az = 180
                newPlayer.loc = [10,-25 + (i+1)*(offGap)]#assume a field 100 (x)  by 50 (y)
            self.players.append(newPlayer)
        newPlayer = Player("Goalie")
        if "red" in side.lower():
            newPlayer.loc = [-45,0]
        else:
            newPlayer.loc = [45,0]
        self.players.append(newPlayer)
            



