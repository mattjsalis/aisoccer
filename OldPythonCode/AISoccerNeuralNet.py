# -*- coding: utf-8 -*-
"""
Created on Sun Aug 11 09:27:11 2019

@author: Matt
"""

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense

def TrainNNFromGameData(XDataFrame,yDataFrame):
    Xcols = XDataFrame.columns.tolist()
    Xcols = list(np.sort(Xcols))
    XDataFrame = XDataFrame[Xcols]
    X = XDataFrame.iloc[:, :].values
    y = yDataFrame.iloc[:, :].values
    #labelencoder_action = LabelEncoder()
    onehotencoder = OneHotEncoder(categorical_features = [0])
    X = onehotencoder.fit_transform(X).toarray()
    X = X[:,1:]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)
    sc_x = StandardScaler()
    X_train = sc_x.fit_transform(X_train)
    X_test = sc_x.transform(X_test)
    
    sc_y = StandardScaler()
    y_train = sc_y.fit_transform(y_train.reshape(-1,1))
    y_test = sc_y.transform(y_test.reshape(-1,1))
    
    # Initialising the ANN
    scorePredictor = Sequential()
    # Adding the input layer and the first hidden layer
    scorePredictor.add(Dense(output_dim = np.size(X,1), init = 'uniform', activation = 'relu', input_dim = np.size(X,1)))
    # Adding the second hidden layer
    scorePredictor.add(Dense(output_dim = 10, init = 'uniform', activation = 'relu'))
    scorePredictor.add(Dense(output_dim = 10, init = 'uniform', activation = 'relu'))
    # Adding the output layer
    scorePredictor.add(Dense(output_dim = 1, init = 'uniform', activation = 'sigmoid'))
    # Compiling the ANN
    scorePredictor.compile(optimizer = 'adam', loss = 'mse', metrics = ['accuracy'])
    # Fitting the ANN to the Training set
    scorePredictor.fit(X_train, y_train, batch_size = 1000, nb_epoch = 100)
    return sc_x,sc_y,X_test,onehotencoder,scorePredictor


