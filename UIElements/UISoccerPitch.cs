﻿using GameElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Utility;

namespace AISoccer.UIElements
{
    /// <summary>
    /// Represents the UI portion of the soccer pitch
    /// </summary>
    public class UISoccerPitch
    {
        #region UI elements
        private Canvas pitchCanvas;
        public Rectangle SoccerPitch { get; } = new Rectangle();
        private Line midLine = new Line();
        private Ellipse centerArc = new Ellipse();
        private Ellipse ball = new Ellipse();
        private Rectangle rightGoal = new Rectangle();
        private Rectangle leftGoal = new Rectangle();
        private Dictionary<GoalSide, Ellipse[]> playerEllipses = new Dictionary<GoalSide, Ellipse[]> ();


        private ToolTip deleteShapeTt = new ToolTip();

        private ContextMenu playerContextMenu = new ContextMenu();
        #endregion

        #region Dimension variables
        private readonly double PLAYER_DIMENSION = 10.0;
        /// <summary>
        /// Padding between the canvas and the field on the left side
        /// </summary>
        private readonly double LEFT_PADDING = 10.0;
        /// <summary>
        /// Padding between the canvas and the field on the top side
        /// </summary>
        private readonly double TOP_PADDING = 10.0;
        private double GOAL_WIDTH = 10;
        public double GOAL_HEIGHT = 45;
        private double CENTER_ARC_DIAMETER = 100;
        private readonly double BALL_DIAMETER = 7.5;
        #endregion


        private int startIndexPlayers = 7;

        /// <summary>
        /// Constructor takes in Canvas and initializes the field
        /// </summary>
        /// <param name="pitchCanvas"></param>
        public UISoccerPitch(ref Canvas pitchCanvas)
        {
            this.pitchCanvas = pitchCanvas;
            pitchCanvas.Children.Add(SoccerPitch);
            pitchCanvas.Children.Add(midLine);
            pitchCanvas.Children.Add(centerArc);
            pitchCanvas.Children.Add(rightGoal);
            pitchCanvas.Children.Add(leftGoal);
            
            pitchCanvas.Children.Add(ball);
            setSizesAndColors();
            setToolTips();
            initializeContextMenus();
        }

        public void RemovePlayersFromPitch()
        {
            pitchCanvas.Children.RemoveRange(startIndexPlayers, 18);
            playerEllipses.Clear();
        }

        public void AddTeam(Team team, Color teamColor)
        {
            Ellipse[] ellipses = team.Players
                                        .Select(player => getNewPlayerEllipse(player, teamColor))
                                        .ToArray();

            playerEllipses.Add(team.TeamGoalSide, ellipses);
            foreach(Ellipse ellipse in ellipses)
            {
                Canvas.SetZIndex(ellipse, 1);
                pitchCanvas.Children.Add(ellipse);
            }
        }

        public void UpdatePlayerEllipsePositions(Team team)
        {
            Ellipse[] ellipses = playerEllipses[team.TeamGoalSide];
            for (int i_plyr = 0; i_plyr < team.Players.Length; i_plyr++)
            {
                updateShapePositionFromFieldPosition(ref ellipses[i_plyr], team.Players[i_plyr].Location);
            }
        }

        public void UpdateBallEllipse(Vect3 ballPos)
        {
            Canvas.SetZIndex(ball, 2);
            updateShapePositionFromFieldPosition(ref ball, ballPos);
            
        }

        

        /// <summary>
        /// Ensure position of field and field markers are consistent relative to
        /// the current size of the parent canvas and that UI elements are appropriately
        /// colored
        /// </summary>
        private void setSizesAndColors()
        {
            SoccerPitch.Fill = Brushes.Green;
            SoccerPitch.SetValue(Canvas.LeftProperty, LEFT_PADDING);
            SoccerPitch.SetValue(Canvas.TopProperty, TOP_PADDING);
            SoccerPitch.Width = pitchCanvas.Width - 2 * LEFT_PADDING;
            SoccerPitch.Height = pitchCanvas.Height - 2 * TOP_PADDING;

            ball.Fill = Brushes.White;
            ball.Width = ball.Height = BALL_DIAMETER;
            setShapeAsDraggable(ball);
            updateShapePositionFromFieldPosition(ref ball, new Vect3());

            midLine.StrokeThickness
                = centerArc.StrokeThickness
                = rightGoal.StrokeThickness
                = leftGoal.StrokeThickness = 2.0;

            midLine.Stroke = Brushes.White;
            midLine.X1 = midLine.X2 = LEFT_PADDING + SoccerPitch.Width / 2;
            midLine.Y1 = TOP_PADDING;
            midLine.Y2 = TOP_PADDING + SoccerPitch.Height;

            centerArc.Stroke = Brushes.White;
            centerArc.Width = centerArc.Height = CENTER_ARC_DIAMETER;
            updateShapePositionFromFieldPosition(ref centerArc, new Vect3());

            rightGoal.Stroke = leftGoal.Stroke = Brushes.Black;
            leftGoal.Width = rightGoal.Width = GOAL_WIDTH;
            leftGoal.Height = rightGoal.Height = GOAL_HEIGHT;
            leftGoal.SetValue(Canvas.LeftProperty, LEFT_PADDING - GOAL_WIDTH);
            leftGoal.SetValue(Canvas.TopProperty, TOP_PADDING + SoccerPitch.Height / 2.0 - GOAL_HEIGHT / 2);
            rightGoal.SetValue(Canvas.LeftProperty, LEFT_PADDING + SoccerPitch.Width);
            rightGoal.SetValue(Canvas.TopProperty, TOP_PADDING + SoccerPitch.Height / 2.0 - GOAL_HEIGHT / 2);
        }

        private void setToolTips()
        {
            deleteShapeTt.Content = "Press D to delete";
            
        }

        private void initializeContextMenus()
        {
            MenuItem deleteItem = new MenuItem();
            deleteItem.Header = "Delete";
            playerContextMenu.Items.Add(deleteItem);

        }

        private void updateShapePositionFromFieldPosition(ref Ellipse shape, Vect3 fieldPosition)
        {
            Vect3 canvasPosition = ImageGeometryConversion.FieldPositionToCanvasPosition(fieldPosition, 
                TOP_PADDING, 
                LEFT_PADDING, 
                SoccerPitch.Width, 
                SoccerPitch.Height, 
                shape.Height, 
                shape.Width);
            Canvas.SetLeft(shape, canvasPosition.X);
            Canvas.SetTop(shape, canvasPosition.Y);
        }

        private Ellipse getNewPlayerEllipse(Player player, Color color)
        {
            Ellipse playerEllipse = new Ellipse();
            playerEllipse.Width = playerEllipse.Height = PLAYER_DIMENSION;
            playerEllipse.Fill = new SolidColorBrush(color);
            
            updateShapePositionFromFieldPosition(ref playerEllipse, player.Location);
            setShapeAsDraggable(playerEllipse);
            return playerEllipse;
        }

        private void setShapeAsDraggable(Shape shape)
        {
            shape.MouseLeftButtonDown += UIElement_MouseLeftButtonDown;
            shape.MouseLeftButtonUp += UIElement_MouseLeftButtonUp;
            shape.MouseMove += Any_MouseMove;
        }


        #region UI events
        private Shape _lastClickedUIElement;
        private Shape _lastRightClickedShape;
        
        private void UIElement_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _lastClickedUIElement = sender as Shape;
            _lastClickedUIElement.Stroke = Brushes.Black;
            _lastClickedUIElement.StrokeThickness = 2;
            _lastClickedUIElement.CaptureMouse();

        }

        private void UIElement_RightButtonDown(object sender, MouseButtonEventArgs e)
        {
            _lastRightClickedShape = sender as Shape;
        }

        private void UIElement_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _lastClickedUIElement.ReleaseMouseCapture();
            _lastClickedUIElement.Stroke = null;
            _lastClickedUIElement = null;
        }

        private void UIElement_Delete(object sender, KeyboardEventArgs e)
        {
            
        }

        private void Any_MouseMove(object sender, MouseEventArgs e)
        {
            if (_lastClickedUIElement != null && Mouse.LeftButton == MouseButtonState.Pressed)
            {
                Point mouseLoc = Mouse.GetPosition(pitchCanvas);
                if (isMouseWithinFieldBounds(mouseLoc))
                {
                    Canvas.SetLeft(_lastClickedUIElement, mouseLoc.X - _lastClickedUIElement.Width/2);
                    Canvas.SetTop(_lastClickedUIElement, mouseLoc.Y - _lastClickedUIElement.Height / 2);
                }
            }
        }

        private bool isMouseWithinFieldBounds(Point point)
        {

            if(point == null || point.X < LEFT_PADDING || point.Y < TOP_PADDING)
            {
                return false;
            }
            if(point.X > pitchCanvas.Width - LEFT_PADDING
                || point.Y > pitchCanvas.Height - TOP_PADDING)
            {
                return false;
            }
            return true;
        }


        #endregion
    }
}
