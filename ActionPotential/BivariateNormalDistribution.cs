﻿using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Reflection;
using Utility;

namespace ActionPotential
{

    public class BivariateNormalDistribution : ActionPotential
    {
        private double normalizationAt0Sigma;

        private IMovingEntity entityToCenterOn;
        private IScalingFunction scalingFunction;

        private IScalingFunction stdevX;
        private IScalingFunction stdevY;
        private IScalingFunction correlation;

        public BivariateNormalDistribution(ref IMovingEntity entityToCenterOn)
            
        {
            this.entityToCenterOn = entityToCenterOn;
        }

        /**
         * Does not deep copy the member variable "entityToCenterOn"
         */
        public override IParameterizable DeepCopy()
        {
            BivariateNormalDistribution copy =  new BivariateNormalDistribution(ref entityToCenterOn,
             stdevX.DeepCopy() as IScalingFunction,
             stdevY.DeepCopy() as IScalingFunction, 
             scalingFunction.DeepCopy() as IScalingFunction,
             correlation.DeepCopy() as IScalingFunction);
            copy.DeepCopyDictionaries(this);
            return copy;
        }

        public override ActionPotential GetCopyCenteredOn(IMovingEntity movingEntity)
        {
            BivariateNormalDistribution copy = new BivariateNormalDistribution(ref movingEntity);
            copy.scalingFunction = scalingFunction;
            copy.stdevX = stdevX;
            copy.stdevY = stdevY;
            copy.correlation = correlation;
            copy.normalizationAt0Sigma = normalizationAt0Sigma;
            return copy;
        }

        public BivariateNormalDistribution(ref IMovingEntity entityToCenterOn,
            IScalingFunction stdevX, IScalingFunction stdevY, IScalingFunction scalingFunction,
            IScalingFunction correlation)

        {
            this.entityToCenterOn = entityToCenterOn;
            this.scalingFunction = scalingFunction;
            this.correlation = correlation;
            this.stdevX = stdevX;
            this.stdevY = stdevY;
            normalizationAt0Sigma = 1.0 / potentialAtRelPos(0.0, 0.0, 1.0);
        }

        public override void assignFromParameters(List<double> parameters)
        {
            base.assignFromParameters(parameters);
            int stdevXInd = (int)parameters[mapVarExpressKeyToCounterInd[0]];
            int stdevYInd = (int)parameters[this.mapVarExpressKeyToCounterInd[1]];
            int scalingFuncInd = (int)parameters[this.mapVarExpressKeyToCounterInd[2]];
            int correlationInd = (int)parameters[this.mapVarExpressKeyToCounterInd[3]];

            stdevX = varExpressions[0][stdevXInd] as IScalingFunction;
            stdevY = varExpressions[1][stdevYInd] as IScalingFunction;
            scalingFunction = varExpressions[2][scalingFuncInd] as IScalingFunction;
            correlation = varExpressions[3][correlationInd] as IScalingFunction;

            normalizationAt0Sigma = 1.0 / potentialAtRelPos(0.0, 0.0, 1.0);
            if(double.IsNaN(normalizationAt0Sigma))
            {
                potentialAtRelPos(0.0, 0.0, 1.0);
                Console.WriteLine();
            }
        }

        public void RegisterStdevXFuncs(IScalingFunction[] stdevXFuncs, ref Counter counter)
        {
            this.registerParameterizableForValue(0, stdevXFuncs, ref counter);
        }

        public void RegisterStdevYFuncs(IScalingFunction[] stdevYFuncs, ref Counter counter)
        {
            this.registerParameterizableForValue(1, stdevYFuncs, ref counter);
        }

        public void RegisterScalingValueFuncs(IScalingFunction[] scalingFunctions, ref Counter counter)
        {
            registerParameterizableForValue(2, scalingFunctions, ref counter);
        }

        public void RegisterCorrelationValueFuncs(IScalingFunction[] scalingFunctions, ref Counter counter)
        {
            registerParameterizableForValue(3, scalingFunctions, ref counter);
        }

        /**
         * <summary>
         * Convenience method for when the user wants to define all values {stdevX, stdevY, scaling, correlation}
         * as scalars and not functions of <x,y>
         * </summary>
         */
        public void MakeWithScalarValues(double[] scalars)
        {
            stdevX = new Scalar(scalars[0]);
            stdevY = new Scalar(scalars[1]);
            scalingFunction = new Scalar(scalars[2]);
            correlation = new Scalar(scalars[3]);
            double potAtRelPos = potentialAtRelPos(0.0, 0.0, 1.0);
            if(Math.Abs(potAtRelPos) < 1e-6)
            {
                normalizationAt0Sigma = 0.0;
            }
            else
            {
                normalizationAt0Sigma = 1.0 / potAtRelPos;
            }
            if(double.IsNaN(normalizationAt0Sigma))
            {
                potentialAtRelPos(0.0, 0.0, 1.0);
                Console.WriteLine();
            }
        }

        /**
         * <summary>
         * gradient of A * e^(
         *   -B*(((x - a)/b)^2
         *   + ((y-c)/d)^2
         *   - E((x - a)(y - c)/(b*d)))
         *      )
         * </summary>
         */
        public override Vect3 gradientAt(double x, double y)
        {
            Vect3 positionToCenterOn = entityToCenterOn.Location;
            double peak = scalingFunction.computePeak(positionToCenterOn.X, positionToCenterOn.Y);
            double correlation = this.correlation.computePeak(positionToCenterOn.X, positionToCenterOn.Y);
            double stdDevX = this.stdevX.computePeak(positionToCenterOn.X, positionToCenterOn.Y);
            double stdDevY = this.stdevY.computePeak(positionToCenterOn.X, positionToCenterOn.Y);
            x = (x - positionToCenterOn.X);
            y = (y - positionToCenterOn.Y);
            if(x == 0.0 && y == 0.0)
            {
                //Gradient is flat
                return new Vect3(1e-6, 1e-6, 0.0);
            }

            double corrSq = correlation * correlation;
            double A = 1.0/(2.0 * Math.PI * stdDevX * stdDevY * Math.Sqrt(1.0 - corrSq));
            double B = 1.0 / (2.0 * (1.0 - corrSq));
            double E = 2.0 * correlation;
            double a = 0.0;
            double b = stdDevX;
            double c = 0.0;
            double d = stdDevY;
            double meanXMinusX = a - x;
            double meanYMinusY = c - y;
            double insideExp = -B * (meanXMinusX * meanXMinusX / b / b  + meanYMinusY * meanYMinusY / d / d - ( E * (meanYMinusY) * (meanXMinusX) ) / (b * d));
            double expTerm = A * B * Math.Exp(insideExp);
            double xTerm = (2.0 * d * (meanXMinusX) + b * E * (-meanYMinusY) )/ (b * b * d);
            double yTerm = -(d* E * (-meanXMinusX) + 2.0 * b * meanYMinusY )/ (b * d * d);
            

            return new Vect3(expTerm * xTerm, expTerm * yTerm, 0.0).Times(peak * normalizationAt0Sigma);
        }

        public override int numParams()
        {
            return 0;
        }

        public override double potentialAt(double x, double y)
        {
            
            //Apply the f(x,y) centered on the current entities position
            Vect3 positionToCenterOn = entityToCenterOn.Location;
            double peak = scalingFunction.computePeak(positionToCenterOn.X, positionToCenterOn.Y);

            x = (x - positionToCenterOn.X);
            y = (y - positionToCenterOn.Y);

            return normalizationAt0Sigma * potentialAtRelPos( x,  y, peak);
        }

        protected double potentialAtRelPos(double x, double y, double peak)
        {
            double correlation = this.correlation.computePeak(x, y);
            double stdDevX = Math.Max(1e-9,Math.Abs(this.stdevX.computePeak(x, y)));
            double stdDevY = Math.Max(1e-9, Math.Abs(this.stdevY.computePeak(x, y)));

            double correlationSq = correlation * correlation;
            double oneMinusCorrSq = 1 - correlationSq;
            // 1/2πσXσYsqrt(1−ρ^2)
            double term1 = 1.0 / (2.0 * Math.PI * stdDevX * stdDevY * Math.Sqrt(oneMinusCorrSq));
            //−1/2(1−ρ^2)
            double term2 = -1.0 / (2.0 * (oneMinusCorrSq));

            //((x−μX)/σX)^2 + ((y−μY)/σY)^2−2ρ(x−μX)(y−μY)/(σXσY)
            double xMinusMeanX = x;
            double yMinusMeanY = y;

            double term3_1 = Math.Pow(xMinusMeanX / stdDevX, 2.0);
            double term3_2 = Math.Pow(yMinusMeanY / stdDevY, 2.0);
            double term3_3 = 2 * correlation * xMinusMeanX * yMinusMeanY / (stdDevX * stdDevY);
            double term3 = term3_1 + term3_2 - term3_3;

            return peak * term1 * Math.Exp(term2 * term3);
        }

        public override void centerOn(IMovingEntity entity)
        {
            this.entityToCenterOn = entity;
        }

        public override double potentialAt(double x, double y, IMovingEntity movingEntity)
        {
            throw new NotImplementedException();
        }

        public override IMovingEntity GetMovingEntity()
        {
            return entityToCenterOn;
        }
    }
}
