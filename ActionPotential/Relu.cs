﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActionPotential
{
    public class Relu : BoundedParameterizable, UnivariateFunction
    {
        public double hingePoint;
        public double slope;
        public double maxValue;
        public double minValue;
        private bool isStartAtMax;

        public Relu(double hingePoint, double slope, double minValue, double maxValue)
        {
            this.hingePoint = hingePoint;
            this.slope = slope;
            this.minValue = minValue;
            this.maxValue = maxValue;
            isStartAtMax = slope < 0.0; 
        }

        public override void assignFromParameters(List<double> parameters)
        {
            hingePoint = parameters[paramRange[0]];
            slope = parameters[paramRange[0] + 1];
            minValue = parameters[paramRange[0] + 2];
            maxValue = parameters[paramRange[0] + 3];
            if(minValue > maxValue)
            {
                double temp = maxValue;
                maxValue = minValue;
                minValue = temp;
            }
            isStartAtMax = slope < 0.0;
        }

        public double computeDerivative(double x)
        {
            throw new NotImplementedException();
        }

        public double computeValue(double x)
        {
            if(isStartAtMax)
            {
                return x < hingePoint ? maxValue :
                    Math.Max(minValue, maxValue + slope * (x - hingePoint));
            }
            else
            {
                return x > hingePoint ? maxValue :
                    Math.Max(minValue, maxValue - slope * (hingePoint - x));
            }
        }

        public override IParameterizable DeepCopy()
        {
            return new Relu(hingePoint, slope, minValue, maxValue);
        }

        public override int numParams()
        {
            return 4;
        }
    }
}
