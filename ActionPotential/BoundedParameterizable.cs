﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace ActionPotential
{
    public abstract class BoundedParameterizable : IParameterizable
    {
        protected int[] paramRange;
        protected double[] bounds;

        public virtual List<int> getAllRelevantParameterIndices()
        {
            List<int> paramIndices = new List<int>();
            for (int i = paramRange[0]; i <= paramRange[1]; i++)
            {
                paramIndices.Add(i);
            }
            return paramIndices;
        }

        public virtual void AssignBoundsToList(List<double[]> paramBoundsList)
        {
            int boundsCounter = 0;
            for (int i = paramRange[0]; i <= paramRange[1]; i++)
            {
                int nextBound = boundsCounter + 1;
                double[] paramBounds = new double[] { bounds[boundsCounter], bounds[nextBound] };
                boundsCounter += 2;
                paramBoundsList[i] = paramBounds;
                if (paramRange[0] == paramRange[1])
                {
                    break;
                }
            }
        }

        public abstract void assignFromParameters(List<double> parameters);

        public virtual void AssignRandomBoundedValuesToList(List<double> paramList)
        {
            int boundsCounter = 0;
            for (int i = paramRange[0];i <= paramRange[1];i++)
            {
                int nextBound = boundsCounter + 1;
                double randVal = Probability.RandomBetween(bounds[boundsCounter], bounds[nextBound]);
                boundsCounter += 2;
                paramList[i] = randVal;
                if (paramRange[0] == paramRange[1])
                {
                    break;
                }
            }
        }

        public abstract IParameterizable DeepCopy();

        public void DeepCopyBoundsFrom(BoundedParameterizable other)
        {
            if (null != other.paramRange)
            {
                this.paramRange = new int[other.paramRange.Length];
                for (int i = 0; i < other.paramRange.Length; i++)
                {
                    paramRange[i] = other.paramRange[i];
                }
            }
            if(null != other.bounds)
            {
                this.bounds = new double[other.bounds.Length];
                for (int i = 0; i < other.bounds.Length; i++)
                {
                    bounds[i] = other.bounds[i];
                }
            }
        }

        public double[] GetBounds()
        {
            return bounds;
        }
        public abstract int numParams();
        public void SetBounds(double[] bounds)
        {
            this.bounds = bounds;
        }
        public void SetParamRange(int[] paramRange)
        {
            this.paramRange = paramRange;
        }

    }
}
