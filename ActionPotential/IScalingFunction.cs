﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionPotential
{
    /**
     * Scales another function linearly
     */
    public interface IScalingFunction : IParameterizable
    {
        double computePeak(double x, double y);
    }
}
