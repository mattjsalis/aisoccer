﻿using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utility;

namespace ActionPotential
{
    public class BoxSampling : BoundedParameterizable, ISamplingStrategy
    {

        int numSamples;
        double xLowerFromPlayerPosition;
        double xUpperFromPlayerPosition;
        double yLowerFromPlayerPosition;
        double yUpperFromPlayerPosition;

        private BoxSampling(int numSamples, double xLowerFromPlayerPosition, double xUpperFromPlayerPosition,
            double yLowerFromPlayerPosition, double yUpperFromPlayerPosition)
        {
            this.numSamples = numSamples;
            this.xLowerFromPlayerPosition = xLowerFromPlayerPosition;
            this.xUpperFromPlayerPosition = xUpperFromPlayerPosition;
            this.yLowerFromPlayerPosition = yLowerFromPlayerPosition;
            this.yUpperFromPlayerPosition = yUpperFromPlayerPosition;
        }

        public override IParameterizable DeepCopy()
        {
            BoxSampling copy = new BoxSampling(numSamples, xLowerFromPlayerPosition, 
                xUpperFromPlayerPosition, yLowerFromPlayerPosition, yUpperFromPlayerPosition);
            copy.DeepCopyBoundsFrom(this);
            return copy;
        }

        public BoxSampling(int numSamples)
        {
            this.numSamples = numSamples;
        }

        public override void assignFromParameters(List<double> parameters)
        {
            xLowerFromPlayerPosition = parameters[paramRange[0]];
            xUpperFromPlayerPosition = parameters[paramRange[0] + 1];
            yLowerFromPlayerPosition = parameters[paramRange[0] + 2];
            yUpperFromPlayerPosition = parameters[paramRange[0] + 3];
        }

        public ISamplingStrategy copyWithNewParamIndexes(ref Counter counter)
        {
            BoxSampling copy = new BoxSampling(numSamples);
            copy.SetBounds(bounds);
            int[] paramRange = counter.GetAndIncrementIndexRange(numParams());
            copy.SetParamRange(paramRange);
            return copy;
        }

        bool multiThread = false;

        public double[] GetLocationSuggestion(IMovingEntity player, IMovingEntity ball, ActionPotential actionPotential,
            double[] xFieldBounds, double[] yFieldBounds, List<double[]> suggestions = null)
        {
            Vect3 playerPosition = player.Location;
            double xLowerBox = Math.Max(xFieldBounds[0], playerPosition.X - xLowerFromPlayerPosition);
            double xUpperBox = Math.Min(xFieldBounds[1], playerPosition.X + xUpperFromPlayerPosition);
            double yLowerBox = Math.Max(yFieldBounds[0], playerPosition.Y - yLowerFromPlayerPosition);
            double yUpperBox = Math.Min(yFieldBounds[1], playerPosition.Y + yUpperFromPlayerPosition);

            
            Task<double>[] tasks = null;
            if (multiThread && suggestions == null)
            {
                suggestions = new List<double[]>(numSamples);
            }
            if(multiThread)
            {
                tasks = new Task<double>[numSamples];
            }

            int nSamplesToTake = numSamples - 1;
            double currLocPot = actionPotential.potentialAt(playerPosition.X, playerPosition.Y);
            double[] bestLoc = new double[] { playerPosition.X, playerPosition.Y };
            double bestScore = currLocPot;

            if(ball.Location.X <= xUpperBox && ball.Location.X >= xLowerBox
                && ball.Location.Y <= yUpperBox && ball.Location.Y >= yLowerBox)
            {
                double potential = actionPotential.potentialAt(ball.Location.X, ball.Location.Y, player);
                if (bestLoc == null || potential > bestScore)
                {
                    bestScore = potential;
                    bestLoc = new double[] { ball.Location.X, ball.Location.Y };
                }
                nSamplesToTake--;
            }

            for (int i=0;i< nSamplesToTake; i++)
            {
                
                double x = Probability.RandomBetween(xLowerBox, xUpperBox);
                double y = Probability.RandomBetween(yLowerBox, yUpperBox);
                double[] suggestion = new double[] { x, y };
                if (null != suggestions)
                {
                    suggestions.Add(suggestion);
                }

                if(multiThread)
                {
                    Task<double> res = Task.Run(() => actionPotential.potentialAt(x, y));
                    tasks[i] = res;
                }
                else
                {
                    double potential = actionPotential.potentialAt(x, y, player);
                    if (bestLoc == null || potential > bestScore)
                    {
                        bestScore = potential;
                        bestLoc = suggestions[i];
                    }
                }

                
            }

            if (multiThread)
            {
                for (int i = 0; i < tasks.Length; i++)
                {
                    Task<double> task = tasks[i];
                    if (bestLoc == null || task.Result > bestScore)
                    {
                        bestScore = task.Result;
                        bestLoc = suggestions[i];
                    }
                }
            }

            return bestLoc;
        }



        public override int numParams()
        {
            return 4;
        }


    }
}
