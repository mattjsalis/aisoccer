﻿using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace ActionPotential
{
    public interface ISamplingStrategy : IParameterizable
    {
        double[] GetLocationSuggestion(IMovingEntity player, IMovingEntity ball, ActionPotential actionPotential,
            double[] xFieldBounds, double[] yFieldBounds, List<double[]> outSuggestions=null);

        ISamplingStrategy copyWithNewParamIndexes(ref Counter counter);
            
    }
}
