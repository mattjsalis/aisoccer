﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActionPotential
{
    public class NormalDistribution : BoundedParameterizable, UnivariateFunction
    {

        private double mean;
        private double varianceSq;
        private double scale;

        public NormalDistribution()
        {

        }
        public NormalDistribution(double mean, double varianceSq, double scale=1.0)
        {
            this.mean = mean;
            this.varianceSq = varianceSq;
            this.scale = scale;
        }

        public override void assignFromParameters(List<double> parameters)
        {
             mean = parameters[paramRange[0]];
            varianceSq = parameters[paramRange[0] + 1];
            scale = parameters[paramRange[0] + 2];
        }

        public double computeDerivative(double x)
        {
            throw new NotImplementedException();
        }

        public double computeValue(double x)
        {
            return scale / Math.Sqrt(2 * Math.PI * varianceSq) 
                * Math.Exp(-Math.Pow(x - mean, 2.0)/(2.0 * varianceSq)); 
        }

        public override IParameterizable DeepCopy()
        {
            NormalDistribution norm = new NormalDistribution(mean, varianceSq, scale);
            norm.DeepCopyBoundsFrom(this);
            return norm;
        }

        public override int numParams()
        {
            return 3;
        }
    }
}
