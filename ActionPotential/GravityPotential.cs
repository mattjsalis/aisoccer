﻿using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace ActionPotential
{
    public class GravityPotential : ActionPotential
    {
        private IMovingEntity entityToCenterOn;

        private IScalingFunction massOfProspectivePosition;
        private IScalingFunction maxValue;
        private IScalingFunction exponentX;
        private IScalingFunction exponentY;

        private GravityPotential(ref IMovingEntity entityToCenterOn, IScalingFunction massOfProspectivePosition, IScalingFunction maxValue, IScalingFunction exponentX, IScalingFunction exponentY)
        {
            this.entityToCenterOn = entityToCenterOn;
            this.massOfProspectivePosition = massOfProspectivePosition;
            this.maxValue = maxValue;
            this.exponentX = exponentX;
            this.exponentY = exponentY;
        }

        public void RegisterMassFuncs(IScalingFunction[] massOfProspectivePositions, ref Counter counter)
        {
            this.registerParameterizableForValue(0, massOfProspectivePositions, ref counter);
        }

        public void RegisterMaxValueFuncs(IScalingFunction[] maxValueFuncs, ref Counter counter)
        {
            this.registerParameterizableForValue(1, maxValueFuncs, ref counter);
        }

        public void RegisterExponentXFuncs(IScalingFunction[] exponentXFuncs, ref Counter counter)
        {
            this.registerParameterizableForValue(2, exponentXFuncs, ref counter);
        }

        public void RegisterExponentYFuncs(IScalingFunction[] exponentYFuncs, ref Counter counter)
        {
            this.registerParameterizableForValue(3, exponentYFuncs, ref counter);
        }

        public override void assignFromParameters(List<double> parameters)
        {
            base.assignFromParameters(parameters);
            int massInd = (int)parameters[mapVarExpressKeyToCounterInd[0]];
            int maxValueInd = (int)parameters[this.mapVarExpressKeyToCounterInd[1]];
            int expXInd = (int)parameters[this.mapVarExpressKeyToCounterInd[2]];
            int expYInd = (int)parameters[this.mapVarExpressKeyToCounterInd[3]];

            massOfProspectivePosition = varExpressions[0][massInd] as IScalingFunction;
            maxValue = varExpressions[1][maxValueInd] as IScalingFunction;
            exponentX = varExpressions[2][expXInd] as IScalingFunction;
            exponentY = varExpressions[3][expYInd] as IScalingFunction;
        }


        public GravityPotential(ref IMovingEntity entityToCenterOn)
        {
            this.entityToCenterOn = entityToCenterOn;
        }

        public override ActionPotential GetCopyCenteredOn(IMovingEntity entity)
        {
            GravityPotential gravPot = new GravityPotential(ref entityToCenterOn);
            gravPot.massOfProspectivePosition = massOfProspectivePosition;
            gravPot.maxValue = maxValue;
            gravPot.exponentX = exponentX;
            gravPot.exponentY = exponentY;
            return gravPot;
        }

        public override Vect3 gradientAt(double x, double y)
        {
            throw new NotImplementedException();
        }

        public override int numParams()
        {
            return 0;
        }

        public override double potentialAt(double x, double y)
        {
            double locAttractorX = entityToCenterOn.Location.X;
            double locAttractorY = entityToCenterOn.Location.Y;
            double diffX = locAttractorX - x;
            double diffY = locAttractorY - y;

            double term1 = 1.0 / (diffX * diffX);
            double term2 = 1.0 / (diffY * diffY);

            double mass = massOfProspectivePosition.computePeak(locAttractorX, locAttractorY);
            double maxValue = Math.Abs(this.maxValue.computePeak(locAttractorX, locAttractorY));
            double expX = exponentX.computePeak(locAttractorX, locAttractorY);
            double expY = exponentY.computePeak(locAttractorX, locAttractorY);

            if(diffX == 0.0 && diffY == 0.0)
            {
                return maxValue * Math.Sign(mass);
            }

            double grav = 0.0;
            if (diffX != 0.0)
            {
                grav += mass * 1.0 / (diffX * diffX);
            }
            else
            {
                grav += maxValue * Math.Sign(mass);
            }

            if(diffY != 0.0)
            {
                grav += mass * 1.0 / (diffY * diffY);
            }
            else
            {
                grav += maxValue * Math.Sign(mass);
            }
            if (Math.Sign(grav) < 0)
            {
                return Math.Max(-maxValue, grav);
            }
            return Math.Min(maxValue, grav);
        }

        public override double potentialAt(double x, double y, IMovingEntity movingEntity)
        {
            throw new NotImplementedException();
        }

        public override IParameterizable DeepCopy()
        {
            GravityPotential copy = new GravityPotential(ref entityToCenterOn,
                massOfProspectivePosition.DeepCopy() as IScalingFunction, 
                maxValue.DeepCopy() as IScalingFunction,
                exponentX.DeepCopy() as IScalingFunction,
                exponentY.DeepCopy() as IScalingFunction);
            copy.DeepCopyDictionaries(this);
            return copy;
        }

        public override IMovingEntity GetMovingEntity()
        {
            return entityToCenterOn;
        }
    }
}
