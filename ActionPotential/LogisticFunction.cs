﻿using System;
using System.Collections.Generic;
using System.Text;
using Utility;
using static Numpy.np;

namespace ActionPotential
{
    public class LogisticFunction : BoundedParameterizable, UnivariateFunction
    {
        /**
         * {lowerL, upperL, lowerK, upperK, lowerXo, upperXo}
         */
        //double[] bounds;
        //int[] paramRange;
        private double L;
        private double k;
        private double xo;
        private double constant = 0.0;
        
        public LogisticFunction(double L, double k, double xo, double constant = 0.0)
        {
            this.L = L;
            this.k = k;
            this.xo = xo;
        }

        public override void assignFromParameters(List<double> parameters)
        {
            this.L = parameters[paramRange[0]];
            this.k = parameters[paramRange[0] + 1];
            this.xo = parameters[paramRange[0] + 2];
            this.constant = parameters[paramRange[0] + 3];
        }

        public double computeDerivative(double x)
        {
            double expTerm = Math.Exp(-k * (-xo + x));
            return (expTerm * k * L) / Math.Pow(1 + expTerm, 2.0);
        }

        public double computeValue(double x)
        {
            return L / (1 + Math.Exp(-k * (x - xo))) + constant;
        }

        public override IParameterizable DeepCopy()
        {
            LogisticFunction logFunc = new LogisticFunction(L, k, xo, constant);
            logFunc.DeepCopyBoundsFrom(this);
            return logFunc;
        }

        public override int numParams()
        {
            return 4;
        }



    }
}
