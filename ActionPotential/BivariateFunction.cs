﻿using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using Utility;

namespace ActionPotential
{
    public class BivariateFunction : ActionPotential
    {
        protected  UnivariateFunction xFunc;
        protected  UnivariateFunction yFunc;

        public BivariateFunction(UnivariateFunction xFunc, UnivariateFunction yFunc)
        {
            this.yFunc = yFunc;
            this.xFunc = xFunc;
        }

        public override void assignFromParameters(List<double> parameters)
        {
            base.assignFromParameters(parameters);
            int xFuncInd = (int)parameters[this.mapVarExpressKeyToCounterInd[0]];
            int yFuncInd = (int)parameters[this.mapVarExpressKeyToCounterInd[1]];
            
            xFunc = this.varExpressions[0][xFuncInd] as UnivariateFunction;
            yFunc = this.varExpressions[1][yFuncInd] as UnivariateFunction;
        }

        public void RegisterXFunction(UnivariateFunction[] xFuncs, ref Counter counter)
        {
            this.registerParameterizableForValue(0, xFuncs,ref counter);
        }

        public void RegisterYFunction(UnivariateFunction[] yFuncs, ref Counter counter)
        {
            this.registerParameterizableForValue(1,yFuncs, ref counter);
        }

        public override Vect3 gradientAt(double x, double y)
        {
            double xComp = xFunc.computeDerivative(x);
            double yComp = yFunc.computeDerivative(y);
            return new Vect3(xComp, yComp, 0.0);
        }

        public override int numParams()
        {
            return 0;
        }

        public override double potentialAt(double x, double y)
        {
            return xFunc.computeValue(x) + yFunc.computeValue(y);
        }

        public override void centerOn(IMovingEntity entity)
        {
            throw new NotImplementedException();
        }

        public override ActionPotential GetCopyCenteredOn(IMovingEntity entity)
        {
            throw new NotImplementedException();
        }

        public override double potentialAt(double x, double y, IMovingEntity movingEntity)
        {
            throw new NotImplementedException();
        }

        public override IParameterizable DeepCopy()
        {
            UnivariateFunction copyX = xFunc.DeepCopy() as UnivariateFunction;
            UnivariateFunction copyY = yFunc.DeepCopy() as UnivariateFunction;
            BivariateFunction bf = new BivariateFunction(copyX, copyY);
            bf.DeepCopyDictionaries(this);
            return bf;
        }

        public override IMovingEntity GetMovingEntity()
        {
            throw new NotImplementedException();
        }
    }
}
