﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionPotential
{
    public class Scalar : BoundedParameterizable, IScalingFunction, UnivariateFunction
    {
        public double scalar { get; set; }
        public Scalar(double scalar)
        {
            this.scalar = scalar;
        }

        public double computePeak(double x, double y)
        {
            return scalar;
        }

        public override void assignFromParameters(List<double> parameters)
        {
            scalar = parameters[paramRange[0]];
        }

        public override int numParams()
        {
            return 1;
        }

        public double computeValue(double x)
        {
            return scalar;
        }

        public double computeDerivative(double x)
        {
            return 0.0;
        }

        public override IParameterizable DeepCopy()
        {
            Scalar copy =  new Scalar(scalar);
            copy.DeepCopyBoundsFrom(this);
            return copy;

        }
    }
}
