﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActionPotential
{
    /**
     * f(x) = y
     * f'(x) = y'
     */
    public interface UnivariateFunction : IParameterizable
    {

         double computeValue(double x);

         double computeDerivative(double x);
    }
}
