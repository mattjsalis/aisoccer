﻿
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Utility;

namespace ActionPotential
{
    public abstract class ActionPotential : BoundedParameterizable, IScalingFunction, ICenteredOn
    {
        /**
         * <summary>
         * Maps the expression key to a list of possible expressions
         * </summary>
         */
        protected Dictionary<int, List<IParameterizable>> varExpressions = new Dictionary<int, List<IParameterizable>>();
        /**
         * <summary>
         * Maps the key from varExpressions to index in the param list
         * that is associated with it
         * </summary>
         */
        protected Dictionary<int, int> mapVarExpressKeyToCounterInd = new Dictionary<int, int>();

        protected void DeepCopyDictionaries(ActionPotential copyFrom)
        {
            foreach(int key in copyFrom.varExpressions.Keys)
            {
                varExpressions[key] = copyFrom.varExpressions[key].Select(p => p.DeepCopy()).ToList();
            }
            foreach (int key in copyFrom.mapVarExpressKeyToCounterInd.Keys)
            {
                mapVarExpressKeyToCounterInd[key] = copyFrom.mapVarExpressKeyToCounterInd[key];
            }
            this.DeepCopyBoundsFrom(copyFrom);
        }

        public override List<int> getAllRelevantParameterIndices()
        {
            List<int> relParams = new List<int>();
            foreach(int key in mapVarExpressKeyToCounterInd.Values)
            {
                relParams.Add(key);
            }
            foreach(List<IParameterizable> parameterizables in varExpressions.Values)
            {
                foreach(IParameterizable p in parameterizables)
                {
                    relParams.AddRange(p.getAllRelevantParameterIndices());
                }
            }
            return relParams;
        }

        public abstract double potentialAt(double x, double y);

        public abstract Vect3 gradientAt(double x, double y);

        public virtual void centerOn(IMovingEntity entity)
        {
            throw new NotImplementedException();
        }

        public override void assignFromParameters(List<double> parameters)
        {
            foreach (List<IParameterizable> parameterizables in varExpressions.Values)
            {
                foreach (IParameterizable parameterizable in parameterizables)
                {
                    parameterizable.assignFromParameters(parameters);
                }
            }
        }

        public override void AssignRandomBoundedValuesToList(List<double> paramList)
        {
            foreach(List<IParameterizable> parameterizables in varExpressions.Values)
            {
                foreach(IParameterizable parameterizable in parameterizables)
                {
                    parameterizable.AssignRandomBoundedValuesToList(paramList);
                }
            }
            //This loop will assign the parameter describing the expression to be
            // used for ech value
            foreach(int valueInd in  mapVarExpressKeyToCounterInd.Keys)
            {
                int numExpressions = varExpressions[valueInd].Count;
                int indInParamList = mapVarExpressKeyToCounterInd[valueInd];
                double randInd = Probability.RandomBetween(0.0, numExpressions - 0.00000001);
                paramList[indInParamList] = randInd;
            }
        }

        public override void AssignBoundsToList(List<double[]> paramBoundsList)
        {
            foreach (List<IParameterizable> parameterizables in varExpressions.Values)
            {
                foreach (IParameterizable parameterizable in parameterizables)
                {
                    parameterizable.AssignBoundsToList(paramBoundsList);
                }
            }
            //Defines the bounds for the expression index
            foreach (int valueInd in mapVarExpressKeyToCounterInd.Keys)
            {
                int numExpressions = varExpressions[valueInd].Count;
                int indInParamList = mapVarExpressKeyToCounterInd[valueInd];
                double[] bounds = new double[] { 0.0, numExpressions - 0.00000001 };
                paramBoundsList[indInParamList] = bounds;
            }
        }

        protected void registerParameterizableForValue(int valueInd,
            IParameterizable[] parameterizables, ref Counter counter)
        {
            if (!varExpressions.ContainsKey(valueInd))
            {
                varExpressions[valueInd] = new List<IParameterizable>();
                mapVarExpressKeyToCounterInd[valueInd] = counter.GetThenIncrement();
            }
            varExpressions[valueInd].AddRange(parameterizables);
            foreach (IParameterizable parameterizable in parameterizables)
            {
                int[] paramRange = counter.GetAndIncrementIndexRange(parameterizable.numParams());
                parameterizable.SetParamRange(paramRange);
            }
        }

        public double computePeak(double x, double y)
        {
            return potentialAt(x, y);
        }

        /**
         * Return the computation of this action potential
         * with X increasing the the left of the matrix and
         * Y decreasing in the second dimension of the matrix
         */
        public double[,] computeField(double[] X, double[] Y)
        {
            int yMaxInd = Y.Length - 1;
            double[,] field = new double[Y.Length,X.Length];
            for(int i=0;i<Y.Length;i++)
            {
                double y = Y[i];
                for (int j =0;j < X.Length;j++)
                {
                    double x = X[j];
                    double z = potentialAt(x, y);
                    field[yMaxInd - i,j] = z;
                }
            }
            return field;
        }

        public static double[,] sumFields(List<double[,]> fields)
        {
            double[,] superimposed = new double[fields[0].GetLength(0),
                fields[0].GetLength(1)];
            for(int i=0;i<fields.Count;i++)
            {
                double[,] field = fields[i];
                for(int j=0;j<field.GetLength(0);j++)
                {
                    for(int k=0;k<field.GetLength(1);k++)
                    {
                        if (i == 0)
                        {
                            superimposed[j, k] = field[j, k];
                        }
                        else
                        {
                            superimposed[j, k] += field[j, k];
                        }
                    }
                }
                
            }
            return superimposed;
        }

        public abstract double potentialAt(double x, double y, IMovingEntity movingEntity);

        public abstract ActionPotential GetCopyCenteredOn(IMovingEntity entity);
        public abstract IMovingEntity GetMovingEntity();
    }
}