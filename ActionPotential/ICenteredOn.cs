﻿using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActionPotential
{
    /**
     * Some Action potential functions need to be centered on a particular point.
     * Add method to recenter.
     */
    public interface ICenteredOn
    {
        void centerOn(IMovingEntity entity);

        ActionPotential GetCopyCenteredOn(IMovingEntity entity);

        IMovingEntity GetMovingEntity();
    }
}
