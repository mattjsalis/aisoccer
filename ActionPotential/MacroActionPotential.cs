﻿using LibOptimization.Optimization;
using SoccerComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace ActionPotential
{
    /**
     * Combines multiple action potentials
     */
    public class MacroActionPotential : ActionPotential
    {
        List<ActionPotential> potentials;
        private int numVar;

        public MacroActionPotential(List<ActionPotential> potentials)
        {
            this.potentials = potentials;
            numVar = potentials.Select(p => p.numParams()).Sum();
        }

        public override void AssignRandomBoundedValuesToList(List<double> paramList)
        {
            foreach(ActionPotential potential in potentials)
            {
                potential.AssignRandomBoundedValuesToList(paramList);
            }
        }

        public override void AssignBoundsToList(List<double[]> paramBoundsList)
        {
            foreach(ActionPotential potential in potentials)
            {
                potential.AssignBoundsToList(paramBoundsList);
            }
        }

        /**
         * Assign all child potentials
         */
        public override void assignFromParameters(List<double> parameters)
        {
            foreach(ActionPotential ap in potentials)
            {
                ap.assignFromParameters(parameters);
            }
        }

        public override Vect3 gradientAt(double x, double y)
        {
            return potentials
                .Select(p => p.gradientAt(x, y))
                .Aggregate((v1, v2) => v1.Plus(v2));
        }

        public override List<int> getAllRelevantParameterIndices()
        {
            List<int> result = new List<int>();
            foreach(ActionPotential potential in potentials)
            {
                result.AddRange(potential.getAllRelevantParameterIndices());
            }
            return result;
        }

        public override int numParams()
        {
            return 0;
        }

        public override double potentialAt(double x, double y)
        {
            double summed = 0.0;
            foreach (ActionPotential potential in potentials)
            {
                summed += potential.potentialAt(x, y);
            }
            return summed;
        }

        public override void centerOn(IMovingEntity entity)
        {
            potentials.ForEach(p => p.centerOn(entity));
        }

        public override ActionPotential GetCopyCenteredOn(IMovingEntity entity)
        {
            List<ActionPotential> copied = potentials.Select(p=>p.GetCopyCenteredOn(entity)).ToList();
            return new MacroActionPotential(copied);
        }

        public override double potentialAt(double x, double y, IMovingEntity movingEntity)
        {
            throw new NotImplementedException();
        }

        public override IParameterizable DeepCopy()
        {
            List<ActionPotential> copiedPotentials = potentials.Select(p=>p.DeepCopy() as ActionPotential).ToList();
            return new MacroActionPotential(copiedPotentials);
        }

        public override IMovingEntity GetMovingEntity()
        {
            return potentials[0].GetMovingEntity();
        }
    }
}
