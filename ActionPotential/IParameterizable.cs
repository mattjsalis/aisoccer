﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActionPotential
{
    public interface IParameterizable
    {
        /**
         * <summary>
         * <param>parameters : all parameters from the policy optimizer</param>
         * </summary>
         */
        void assignFromParameters(List<double> parameters);
        int numParams();

        void SetParamRange(int[] paramRange);

        double[] GetBounds();

        void SetBounds(double[] bounds);

        void AssignRandomBoundedValuesToList(List<double> paramList);

        void AssignBoundsToList(List<double[]> paramBoundsList);

        List<int> getAllRelevantParameterIndices();

        IParameterizable DeepCopy();
    }
}
