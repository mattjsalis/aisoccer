﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActionPotential
{
    public class SinFunction : BoundedParameterizable,  UnivariateFunction
    {
        //int[] paramRange;
        private double amplitude;
        private double internalCoeff;
        private double verticalShift;
        private double phaseShift;

        public SinFunction(double amplitude, double internalCoeff, double phaseShift)
        {
            this.amplitude = amplitude;
            this.internalCoeff = internalCoeff;
            this.phaseShift = phaseShift;
        }

        public override void assignFromParameters(List<double> parameters)
        {
            amplitude = parameters[paramRange[0]];
            internalCoeff = parameters[paramRange[0] + 1];
            verticalShift = parameters[paramRange[0] + 2];
            phaseShift = parameters[paramRange[0] + 3];
        }

        public  double computeDerivative(double x)
        {
            return -internalCoeff * amplitude * Math.Cos(internalCoeff * (x + phaseShift));
        }

        public  double computeValue(double x)
        {
            return amplitude * Math.Sin(internalCoeff * (x + phaseShift)) + verticalShift;
        }

        public override IParameterizable DeepCopy()
        {
            SinFunction copy = new SinFunction(amplitude, internalCoeff, phaseShift);
            copy.DeepCopyBoundsFrom(this);
            return copy;
        }

        public override int numParams()
        {
            return 4;
        }
    }
}
